module Engram.LevelUpCounter (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = LevelUp_Counter_AE


instance ToEngram Engram where
  toCode LevelUp_Counter_AE = "EngramEntry_LevelUp_Counter_AE_C"
