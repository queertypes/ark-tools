module Engram.ImmersiveTaming (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = BaitBalloon
  | HumanScent
  | BaitCooler
  | BaitDispenser
  | BaitStation_Food
  | BaitStation_Kibble
  | BaitStation_Trophy
  | TamingJournal
  | VialStation


instance ToEngram Engram where
  toCode BaitBalloon = "EngramEntry_BaitBalloon_C"
  toCode HumanScent = "EngramEntry_HumanScent_C"
  toCode BaitCooler = "EngramEntry_BaitCooler_C"
  toCode BaitDispenser = "EngramEntry_BaitDispenser_C"
  toCode BaitStation_Food = "EngramEntry_BaitStation_Food_C"
  toCode BaitStation_Kibble = "EngramEntry_BaitStation_Kibble_C"
  toCode BaitStation_Trophy = "EngramEntry_BaitStation_Trophy_C"
  toCode TamingJournal = "EngramEntry_TamingJournal_C"
  toCode VialStation = "EngramEntry_VialStation_C"
