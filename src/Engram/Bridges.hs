module Engram.Bridges (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = JD_Bridge01
  | JD_Bridge02
  | JD_Bridge_Metal_01
  | JD_Bridge_Metal_02
  | JD_Bridge_Stone_01
  | JD_Bridge_Stone_02
  | JD_Bridge_Wood_03


instance ToEngram Engram where
  toCode JD_Bridge01 = "EngramEntry_jd_Bridge01_C"
  toCode JD_Bridge02 = "EngramEntry_jd_Bridge02_C"
  toCode JD_Bridge_Metal_01 = "EngramEntry_jd_Bridge_Metal_01_C"
  toCode JD_Bridge_Metal_02 = "EngramEntry_jd_Bridge_Metal_02_C"
  toCode JD_Bridge_Stone_01 = "EngramEntry_jd_Bridge_Stone_01_C"
  toCode JD_Bridge_Stone_02 = "EngramEntry_jd_Bridge_Stone_02_C"
  toCode JD_Bridge_Wood_03 = "EngramEntry_jd_Bridge_Wood_03_C"
