module Engram.UpgradeStation (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = AugmentStation
  | AugmentStation_Base
  | AugmentStation_Metal
  | BlueprintStation
  | BlueprintStation_Base
  | BlueprintStation_Metal
  | UpgradeStationManual
  | UpgradeStation
  | UpgradeStation_Base
  | UpgradeStation_Metal


instance ToEngram Engram where
  toCode AugmentStation = "EngramEntry_AugmentStation_C"
  toCode AugmentStation_Base = "EngramEntry_AugmentStation_Base_C"
  toCode AugmentStation_Metal = "EngramEntry_AugmentStation_Metal_C"
  toCode BlueprintStation = "EngramEntry_BlueprintStation_C"
  toCode BlueprintStation_Base = "EngramEntry_BlueprintStation_Base_C"
  toCode BlueprintStation_Metal = "EngramEntry_BlueprintStation_Metal_C"
  toCode UpgradeStationManual = "EngramEntry_UpgradeStationManual_C"
  toCode UpgradeStation = "EngramEntry_UpgradeStation_C"
  toCode UpgradeStation_Base = "EngramEntry_UpgradeStation_Base_C"
  toCode UpgradeStation_Metal = "EngramEntry_UpgradeStation_Metal_C"
