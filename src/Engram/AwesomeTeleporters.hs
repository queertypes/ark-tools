module Engram.AwesomeTeleporters (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = AwesomeTeleporters_Remote
  | AwesomeTeleporters_Teleporter
  | AwesomeTeleporters_DinoTracker


instance ToEngram Engram where
  toCode AwesomeTeleporters_Remote = "EngramEntry_AwesomeTeleporters_Remote_C"
  toCode AwesomeTeleporters_Teleporter = "EngramEntry_AwesomeTeleporters_Teleporter_C"
  toCode AwesomeTeleporters_DinoTracker = "EngramEntry_AwesomeTeleporters_DinoTracker_C"
