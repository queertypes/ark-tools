module Engram.Kibbler (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = Kibbler_Balanced
  | Kibbler_Easy

instance ToEngram Engram where
  toCode Kibbler_Balanced = "EngramEntry_Kibbler_Balanced_C"
  toCode Kibbler_Easy = "EngramEntry_Kibbler_Easy_C"
