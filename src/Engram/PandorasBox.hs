module Engram.PandorasBox (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = MagicBox
  | PandorasBox


instance ToEngram Engram where
  toCode MagicBox = "EngramEntry_MagicBox_C"
  toCode PandorasBox = "EngramEntry_PandorasBox_C"
