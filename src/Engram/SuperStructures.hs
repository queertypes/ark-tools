module Engram.SuperStructures (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = BeerBarrelPlus
  | CampfirePlus
  | ChemBenchPlus
  | CompostBinPlus
  | CookingPotPlus
  | FabricatorPlus
  | FireplacePlus
  | MortarPestlePlus
  | PreservingBinPlus
  | ForgePlus
  | ReplicatorPlus
  | SmithyPlus
  | SPlusCraftingStation
  | CropPlotPlus_Large
  | CropPlotPlus_Medium
  | CropPlotPlus_SeamlessSquare
  | CropPlotPlus_SeamlessTriangle
  | CropPlotPlus_Small
  | TekCropPlotSS
  | TekCropPlotTriangleSS
  | Door_Adobe
  | Door_Glass
  | Door_Greenhouse
  | Door_Metal
  | Door_Stone
  | Door_Tek
  | Door_Thatch
  | Door_Underwater
  | Door_Wood
  | DoubleDoor_AdobeSP
  | DoubleDoor_Glass
  | DoubleDoor_GreenhouseSP
  | DoubleDoor_MetalSP
  | DoubleDoor_StoneSP
  | DoubleDoor_TekSP
  | DoubleDoor_WoodSP
  | Gate_Dynamic
  | LargeGate_Adobe
  | LargeGate_Glass
  | LargeGate_Metal
  | LargeGate_Stone
  | LargeGate_Tek
  | Gate_Adobe
  | Gate_Glass
  | Gate_Metal
  | Gate_Stone
  | Gate_Tek
  | Gate_Wood
  | Trapdoor_Adobe
  | Trapdoor_Glass
  | Trapdoor_Metal
  | Trapdoor_Moonpool
  | Trapdoor_Stone
  | Trapdoor_Tek
  | Trapdoor_Wood
  | LargeTrapdoor_Adobe
  | LargeTrapdoor_Glass
  | LargeTrapdoor_Metal
  | LargeTrapdoor_Stone
  | LargeTrapdoor_Tek
  | XLTrapdoor_Adobe
  | XLTrapdoor_Glass
  | XLTrapdoor_Metal
  | XLTrapdoor_Stone
  | XLTrapdoor_Tek
  | Window_Adobe
  | Window_Glass
  | Window_Greenhouse
  | Window_Metal
  | Window_Stone
  | Window_Tek
  | Window_Wood
  | LoadoutDummy
  | InventoryAssistant
  | PersonalTeleporter
  | TransferTool
  | SprayPainterPlus
  | Ladder_Adobe
  | Ladder_Metal
  | Ladder_Rope
  | Ladder_Tek
  | Ladder_Wood
  | AirConditionerPlus
  | AmmoLoaderSS
  | AnimalTender
  | AutoCrafter
  | AutoTurret
  | AutoTurret_Ballista
  | AutoTurret_Cannon
  | AutoTurret_Flame
  | AutoTurret_Heavy
  | AutoTurret_Laser
  | AutoTurret_Minigun
  | AutoTurretPlant
  | AutoTurret_Rocket
  | AutoTurretTek
  | AutoTurret_Tranq
  | BedPlus
  | BunkBedPlus
  | TekBedPlus
  | BeeHivePlus
  | BlueprintMaker
  | ChargeInjector
  | ChargeStation
  | CloningChamberPlus
  | Converter
  | CryoFridge_SS
  | CrystalCracker
  | DedicatedStorageSP
  | DedicatedStorage_Intake
  | DinoUplink
  | DroneTerminal
  | DyeStation
  | ElementCatalyzer
  | ElevatorCallButton
  | ElevatorPlatform_Large
  | ElevatorPlatform_Medium
  | ElevatorPlatform_Small
  | ElevatorTrack_Metal
  | Farmer
  | FeedingTroughPlus
  | TekTroughPlus
  | TekForcefield
  | FridgePlus
  | GachaGavager
  | Gardener
  | GasCollectorTek
  | Generator
  | GeneratorTek
  | WindTurbinePlus
  | GenomicsChamber
  | Hatchery
  | HitchingPost
  | HitchingPost_Tek
  | HoverSkiffSS
  | Incinerator
  | EggIncubatorSS
  | IndustrialCooker
  | IndustrialForgePlus
  | IndustrialGrillPlus
  | GrinderPlus
  | ItemCollector
  | ItemTranslocator
  | LeashTek
  | SPlusTekLight
  | LoadoutDummySS
  | Mannequin
  | MultiLamp
  | Nanny
  | Noglin_Nullifier
  | ElectricalOutlet
  | PlanetShield
  | PressurePlateSS
  | RepairStation
  | SheepHerder
  | ShieldPost
  | SpikeWall_Metal
  | SpikeWall_Wood
  | BookshelfPlus
  | StorageLarge
  | StorageMetal
  | StorageSmall
  | TekStorage
  | TaxidermyPlus_Large
  | TaxidermyPlus_Medium
  | TaxidermyPlus_Small
  | TekBridgePlus
  | TekChemBench
  | TekCookingPot
  | TekForge
  | FridgeTek
  | JumpPadSS
  | TekSensor
  | TekStove
  | TeleporterPlus
  | StandingTorchPlus
  | WallTorchPlus
  | TransmitterPlus
  | TreeSapTapPlus
  | TrophyBaseSS
  | TrophyWallSS
  | VaultPlus
  | VesselPlus
  | Vivarium
  | WaterTank_Metal
  | WaterTank_Stone
  | PipeFlex_MetalSP
  | PipeFlex_StoneSP
  | PipeHorizontal_Metal
  | PipeHorizontal_Stone
  | PipeIncline_Metal
  | PipeIncline_Stone
  | PipeIntake_Metal
  | PipeIntake_Stone
  | InternalPipe_Pillar
  | InternalPipe_Square
  | InternalPipe_Triangle
  | InternalPipe_Wall
  | PipeIntersection_Metal
  | PipeIntersection_Stone
  | WaterTap_Metal
  | WaterTap_Stone
  | PipeVertical_Metal
  | PipeVertical_Stone
  | Catwalk_Glass
  | Catwalk_Metal
  | Catwalk_Tek
  | Catwalk_Wood
  | Ceiling_Adobe
  | Ceiling_Glass
  | Ceiling_Greenhouse
  | Ceiling_Metal
  | Ceiling_Stone
  | Ceiling_Tek
  | Ceiling_TekElevator
  | Ceiling_TekGlass
  | Ceiling_Thatch
  | Ceiling_Wood
  | TriCeiling_AdobeSP
  | TriCeiling_Glass
  | TriCeiling_GreenhouseSP
  | TriCeiling_MetalSP
  | TriCeiling_StoneSP
  | TriCeiling_TekSP
  | TriCeiling_TekElevator
  | TriCeiling_WoodSP
  | Doorframe_Adobe
  | Doorframe_Glass
  | Doorframe_Greenhouse
  | Doorframe_Metal
  | Doorframe_Stone
  | Doorframe_Tek
  | Doorframe_Thatch
  | Doorframe_Wood
  | DoubleDoorframe_AdobeSP
  | DoubleDoorframe_Glass
  | DoubleDoorframe_GreenhouseSP
  | DoubleDoorframe_MetalSP
  | DoubleDoorframe_StoneSP
  | DoubleDoorframe_TekSP
  | DoubleDoorframe_WoodSP
  | FenceFoundation_Adobe
  | FenceFoundation_Metal
  | FenceFoundation_Stone
  | FenceFoundation_Tek
  | FenceFoundation_Wood
  | FenceSupport_AdobeSP
  | FenceSupport_MetalSP
  | FenceSupport_StoneSP
  | FenceSupport_TekSP
  | FenceSupport_WoodSP
  | Foundation_Adobe
  | Foundation_Glass
  | Foundation_Metal
  | Foundation_Ocean_Metal
  | Foundation_Ocean_Wood
  | Foundation_Stone
  | Foundation_Tek
  | Foundation_Thatch
  | Foundation_Wood
  | TriFoundation_AdobeSP
  | TriFoundation_Glass
  | TriFoundation_MetalSP
  | TriFoundation_StoneSP
  | TriFoundation_TekSP
  | TriFoundation_WoodSP
  | Gateway_Dynamic
  | LargeGateway_Adobe
  | LargeGateway_Metal
  | LargeGateway_Stone
  | LargeGateway_Tek
  | Gateway_Adobe
  | Gateway_Metal
  | Gateway_Stone
  | Gateway_Tek
  | Gateway_Wood
  | Hatchframe_Adobe
  | Hatchframe_Glass
  | Hatchframe_Metal
  | Hatchframe_Stone
  | Hatchframe_Tek
  | Hatchframe_Wood
  | LargeHatchframe_Adobe
  | LargeHatchframe_Metal
  | LargeHatchframe_Stone
  | LargeHatchframe_Tek
  | LargeHatchframeSloped_Adobe
  | LargeHatchframeSloped_Metal
  | LargeHatchframeSloped_Stone
  | LargeHatchframeSloped_Tek
  | XLHatchframe_Adobe
  | XLHatchframe_Metal
  | XLHatchframe_Stone
  | XLHatchframe_Tek
  | XLHatchframeSloped_Adobe
  | XLHatchframeSloped_Metal
  | XLHatchframeSloped_Stone
  | XLHatchframeSloped_Tek
  | DynamicPillar_Adobe
  | DynamicPillar_Metal
  | DynamicPillar_Stone
  | DynamicPillar_Tek
  | DynamicPillar_Wood
  | LargePillar_Adobe
  | LargePillar_Metal
  | LargePillar_Stone
  | LargePillar_Tek
  | LargePillar_Wood
  | MediumPillar_Adobe
  | MediumPillar_Metal
  | MediumPillar_Stone
  | MediumPillar_Tek
  | MediumPillar_Wood
  | SmallPillar_Adobe
  | SmallPillar_Metal
  | SmallPillar_Stone
  | SmallPillar_Tek
  | SmallPillar_Wood
  | Railing_Adobe
  | Railing_Glass
  | Railing_Metal
  | Railing_Stone
  | Railing_Tek
  | Railing_Wood
  | Ramp_AdobeSP
  | Ramp_Glass
  | Ramp_MetalSP
  | Ramp_StoneSP
  | Ramp_TekSP
  | Ramp_WoodSP
  | Roof_Adobe
  | Roof_Glass
  | Roof_Greenhouse
  | Roof_Metal
  | Roof_Stone
  | Roof_Tek
  | Roof_TekGlass
  | Roof_Thatch
  | Roof_Wood
  | TriRoof_AdobeSP
  | TriRoof_Glass
  | TriRoof_GreenhouseSP
  | TriRoof_MetalSP
  | TriRoof_StoneSP
  | TriRoof_TekSP
  | TriRoof_WoodSP
  | SlopedWall_Adobe_Left
  | SlopedWall_Glass_Left
  | SlopedWall_Greenhouse_Left
  | SlopedWall_Metal_Left
  | SlopedWall_Stone_Left
  | SlopedWall_Tek_Left
  | SlopedWall_Thatch_Left
  | SlopedWall_Wood_Left
  | SlopedWall_Adobe_Right
  | SlopedWall_Glass_Right
  | SlopedWall_Greenhouse_Right
  | SlopedWall_Metal_Right
  | SlopedWall_Stone_Right
  | SlopedWall_Tek_Right
  | SlopedWall_Thatch_Right
  | SlopedWall_Wood_Right
  | Staircase_Adobe
  | Staircase_Glass
  | Staircase_Metal
  | Staircase_Stone
  | Staircase_Tek
  | Staircase_Wood
  | Underwater_Cube
  | Underwater_Cube_Sloped
  | Underwater_Moonpool
  | Wall_Adobe
  | Wall_Forcefield
  | Wall_Glass
  | Wall_Greenhouse
  | Wall_Metal
  | Wall_Stone
  | Wall_Tek
  | Wall_TekGlass
  | Wall_Thatch
  | Wall_Wood
  | LargeWall_AdobeSP
  | LargeWall_Forcefield
  | LargeWall_Glass
  | LargeWall_MetalSP
  | LargeWall_StoneSP
  | LargeWall_TekSP
  | LargeWall_WoodSP
  | XLWall_Adobe
  | XLWall_Forcefield
  | XLWall_Glass
  | XLWall_Metal
  | XLWall_Stone
  | XLWall_Tek
  | XLWall_Wood
  | WindowWall_Adobe
  | WindowWall_Metal
  | WindowWall_Stone
  | WindowWall_Tek
  | WindowWall_Wood
  | Wire_FlexSP
  | Wire_Horizontal
  | Wire_Incline
  | InternalWire_Pillar
  | InternalWire_Square
  | InternalWire_Triangle
  | InternalWire_Wall
  | Wire_Intersection
  | Wire_Vertical


instance ToEngram Engram where
  toCode BeerBarrelPlus = "EngramEntry_BeerBarrelPlus_C"
  toCode CampfirePlus = "EngramEntry_CampfirePlus_C"
  toCode ChemBenchPlus = "EngramEntry_ChemBenchPlus_C"
  toCode CompostBinPlus = "EngramEntry_CompostBinPlus_C"
  toCode CookingPotPlus = "EngramEntry_CookingPotPlus_C"
  toCode FabricatorPlus = "EngramEntry_FabricatorPlus_C"
  toCode FireplacePlus = "EngramEntry_FireplacePlus_C"
  toCode MortarPestlePlus = "EngramEntry_MortarPestlePlus_C"
  toCode PreservingBinPlus = "EngramEntry_PreservingBinPlus_C"
  toCode ForgePlus = "EngramEntry_ForgePlus_C"
  toCode ReplicatorPlus = "EngramEntry_ReplicatorPlus_C"
  toCode SmithyPlus = "EngramEntry_SmithyPlus_C"
  toCode SPlusCraftingStation = "EngramEntry_SPlusCraftingStation_C"
  toCode CropPlotPlus_Large = "EngramEntry_CropPlotPlus_Large_C"
  toCode CropPlotPlus_Medium = "EngramEntry_CropPlotPlus_Medium_C"
  toCode CropPlotPlus_SeamlessSquare = "EngramEntry_CropPlotPlus_SeamlessSquare_C"
  toCode CropPlotPlus_SeamlessTriangle = "EngramEntry_CropPlotPlus_SeamlessTriangle_C"
  toCode CropPlotPlus_Small = "EngramEntry_CropPlotPlus_Small_C"
  toCode TekCropPlotSS = "EngramEntry_TekCropPlotSS_C"
  toCode TekCropPlotTriangleSS = "EngramEntry_TekCropPlotTriangleSS_C"
  toCode Door_Adobe = "EngramEntry_Door_Adobe_C"
  toCode Door_Glass = "EngramEntry_Door_Glass_C"
  toCode Door_Greenhouse = "EngramEntry_Door_Greenhouse_C"
  toCode Door_Metal = "EngramEntry_Door_Metal_C"
  toCode Door_Stone = "EngramEntry_Door_Stone_C"
  toCode Door_Tek = "EngramEntry_Door_Tek_C"
  toCode Door_Thatch = "EngramEntry_Door_Thatch_C"
  toCode Door_Underwater = "EngramEntry_Door_Underwater_C"
  toCode Door_Wood = "EngramEntry_Door_Wood_C"
  toCode DoubleDoor_AdobeSP = "EngramEntry_DoubleDoor_AdobeSP_C"
  toCode DoubleDoor_Glass = "EngramEntry_DoubleDoor_Glass_C"
  toCode DoubleDoor_GreenhouseSP = "EngramEntry_DoubleDoor_GreenhouseSP_C"
  toCode DoubleDoor_MetalSP = "EngramEntry_DoubleDoor_MetalSP_C"
  toCode DoubleDoor_StoneSP = "EngramEntry_DoubleDoor_StoneSP_C"
  toCode DoubleDoor_TekSP = "EngramEntry_DoubleDoor_TekSP_C"
  toCode DoubleDoor_WoodSP = "EngramEntry_DoubleDoor_WoodSP_C"
  toCode Gate_Dynamic = "EngramEntry_Gate_Dynamic_C"
  toCode LargeGate_Adobe = "EngramEntry_LargeGate_Adobe_C"
  toCode LargeGate_Glass = "EngramEntry_LargeGate_Glass_C"
  toCode LargeGate_Metal = "EngramEntry_LargeGate_Metal_C"
  toCode LargeGate_Stone = "EngramEntry_LargeGate_Stone_C"
  toCode LargeGate_Tek = "EngramEntry_LargeGate_Tek_C"
  toCode Gate_Adobe = "EngramEntry_Gate_Adobe_C"
  toCode Gate_Glass = "EngramEntry_Gate_Glass_C"
  toCode Gate_Metal = "EngramEntry_Gate_Metal_C"
  toCode Gate_Stone = "EngramEntry_Gate_Stone_C"
  toCode Gate_Tek = "EngramEntry_Gate_Tek_C"
  toCode Gate_Wood = "EngramEntry_Gate_Wood_C"
  toCode Trapdoor_Adobe = "EngramEntry_Trapdoor_Adobe_C"
  toCode Trapdoor_Glass = "EngramEntry_Trapdoor_Glass_C"
  toCode Trapdoor_Metal = "EngramEntry_Trapdoor_Metal_C"
  toCode Trapdoor_Moonpool = "EngramEntry_Trapdoor_Moonpool_C"
  toCode Trapdoor_Stone = "EngramEntry_Trapdoor_Stone_C"
  toCode Trapdoor_Tek = "EngramEntry_Trapdoor_Tek_C"
  toCode Trapdoor_Wood = "EngramEntry_Trapdoor_Wood_C"
  toCode LargeTrapdoor_Adobe = "EngramEntry_LargeTrapdoor_Adobe_C"
  toCode LargeTrapdoor_Glass = "EngramEntry_LargeTrapdoor_Glass_C"
  toCode LargeTrapdoor_Metal = "EngramEntry_LargeTrapdoor_Metal_C"
  toCode LargeTrapdoor_Stone = "EngramEntry_LargeTrapdoor_Stone_C"
  toCode LargeTrapdoor_Tek = "EngramEntry_LargeTrapdoor_Tek_C"
  toCode XLTrapdoor_Adobe = "EngramEntry_XLTrapdoor_Adobe_C"
  toCode XLTrapdoor_Glass = "EngramEntry_XLTrapdoor_Glass_C"
  toCode XLTrapdoor_Metal = "EngramEntry_XLTrapdoor_Metal_C"
  toCode XLTrapdoor_Stone = "EngramEntry_XLTrapdoor_Stone_C"
  toCode XLTrapdoor_Tek = "EngramEntry_XLTrapdoor_Tek_C"
  toCode Window_Adobe = "EngramEntry_Window_Adobe_C"
  toCode Window_Glass = "EngramEntry_Window_Glass_C"
  toCode Window_Greenhouse = "EngramEntry_Window_Greenhouse_C"
  toCode Window_Metal = "EngramEntry_Window_Metal_C"
  toCode Window_Stone = "EngramEntry_Window_Stone_C"
  toCode Window_Tek = "EngramEntry_Window_Tek_C"
  toCode Window_Wood = "EngramEntry_Window_Wood_C"
  toCode LoadoutDummy = "EngramEntry_LoadoutDummy_C"
  toCode InventoryAssistant = "EngramEntry_InventoryAssistant_C"
  toCode PersonalTeleporter = "EngramEntry_PersonalTeleporter_C"
  toCode TransferTool = "EngramEntry_TransferTool_C"
  toCode SprayPainterPlus = "EngramEntry_SprayPainterPlus_C"
  toCode Ladder_Adobe = "EngramEntry_Ladder_Adobe_C"
  toCode Ladder_Metal = "EngramEntry_Ladder_Metal_C"
  toCode Ladder_Rope = "EngramEntry_Ladder_Rope_C"
  toCode Ladder_Tek = "EngramEntry_Ladder_Tek_C"
  toCode Ladder_Wood = "EngramEntry_Ladder_Wood_C"
  toCode AirConditionerPlus = "EngramEntry_AirConditionerPlus_C"
  toCode AmmoLoaderSS = "EngramEntry_AmmoLoaderSS_C"
  toCode AnimalTender = "EngramEntry_AnimalTender_C"
  toCode AutoCrafter = "EngramEntry_AutoCrafter_C"
  toCode AutoTurret = "EngramEntry_AutoTurret_C"
  toCode AutoTurret_Ballista = "EngramEntry_AutoTurret_Ballista_C"
  toCode AutoTurret_Cannon = "EngramEntry_AutoTurret_Cannon_C"
  toCode AutoTurret_Flame = "EngramEntry_AutoTurret_Flame_C"
  toCode AutoTurret_Heavy = "EngramEntry_AutoTurret_Heavy_C"
  toCode AutoTurret_Laser = "EngramEntry_AutoTurret_Laser_C"
  toCode AutoTurret_Minigun = "EngramEntry_AutoTurret_Minigun_C"
  toCode AutoTurretPlant = "EngramEntry_AutoTurretPlant_C"
  toCode AutoTurret_Rocket = "EngramEntry_AutoTurret_Rocket_C"
  toCode AutoTurretTek = "EngramEntry_AutoTurretTek_C"
  toCode AutoTurret_Tranq = "EngramEntry_AutoTurret_Tranq_C"
  toCode BedPlus = "EngramEntry_BedPlus_C"
  toCode BunkBedPlus = "EngramEntry_BunkBedPlus_C"
  toCode TekBedPlus = "EngramEntry_TekBedPlus_C"
  toCode BeeHivePlus = "EngramEntry_BeeHivePlus_C"
  toCode BlueprintMaker = "EngramEntry_BlueprintMaker_C"
  toCode ChargeInjector = "EngramEntry_ChargeInjector_C"
  toCode ChargeStation = "EngramEntry_ChargeStation_C"
  toCode CloningChamberPlus = "EngramEntry_CloningChamberPlus_C"
  toCode Converter = "EngramEntry_Converter_C"
  toCode CryoFridge_SS = "EngramEntry_CryoFridge_SS_C"
  toCode CrystalCracker = "EngramEntry_CrystalCracker_C"
  toCode DedicatedStorageSP = "EngramEntry_DedicatedStorageSP_C"
  toCode DedicatedStorage_Intake = "EngramEntry_DedicatedStorage_Intake_C"
  toCode DinoUplink = "EngramEntry_DinoUplink_C"
  toCode DroneTerminal = "EngramEntry_DroneTerminal_C"
  toCode DyeStation = "EngramEntry_DyeStation_C"
  toCode ElementCatalyzer = "EngramEntry_ElementCatalyzer_C"
  toCode ElevatorCallButton = "EngramEntry_ElevatorCallButton_C"
  toCode ElevatorPlatform_Large = "EngramEntry_ElevatorPlatform_Large_C"
  toCode ElevatorPlatform_Medium = "EngramEntry_ElevatorPlatform_Medium_C"
  toCode ElevatorPlatform_Small = "EngramEntry_ElevatorPlatform_Small_C"
  toCode ElevatorTrack_Metal = "EngramEntry_ElevatorTrack_Metal_C"
  toCode Farmer = "EngramEntry_Farmer_C"
  toCode FeedingTroughPlus = "EngramEntry_FeedingTroughPlus_C"
  toCode TekTroughPlus = "EngramEntry_TekTroughPlus_C"
  toCode TekForcefield = "EngramEntry_TekForcefield_C"
  toCode FridgePlus = "EngramEntry_FridgePlus_C"
  toCode GachaGavager = "EngramEntry_GachaGavager_C"
  toCode Gardener = "EngramEntry_Gardener_C"
  toCode GasCollectorTek = "EngramEntry_GasCollectorTek_C"
  toCode Generator = "EngramEntry_Generator_C"
  toCode GeneratorTek = "EngramEntry_GeneratorTek_C"
  toCode WindTurbinePlus = "EngramEntry_WindTurbinePlus_C"
  toCode GenomicsChamber = "EngramEntry_GenomicsChamber_C"
  toCode Hatchery = "EngramEntry_Hatchery_C"
  toCode HitchingPost = "EngramEntry_HitchingPost_C"
  toCode HitchingPost_Tek = "EngramEntry_HitchingPost_Tek_C"
  toCode HoverSkiffSS = "EngramEntry_HoverSkiffSS_C"
  toCode Incinerator = "EngramEntry_Incinerator_C"
  toCode EggIncubatorSS = "EngramEntry_EggIncubatorSS_C"
  toCode IndustrialCooker = "EngramEntry_IndustrialCooker_C"
  toCode IndustrialForgePlus = "EngramEntry_IndustrialForgePlus_C"
  toCode IndustrialGrillPlus = "EngramEntry_IndustrialGrillPlus_C"
  toCode GrinderPlus = "EngramEntry_GrinderPlus_C"
  toCode ItemCollector = "EngramEntry_ItemCollector_C"
  toCode ItemTranslocator = "EngramEntry_ItemTranslocator_C"
  toCode LeashTek = "EngramEntry_LeashTek_C"
  toCode SPlusTekLight = "EngramEntry_SPlusTekLight_C"
  toCode LoadoutDummySS = "EngramEntry_LoadoutDummySS_C"
  toCode Mannequin = "EngramEntry_Mannequin_C"
  toCode MultiLamp = "EngramEntry_MultiLamp_C"
  toCode Nanny = "EngramEntry_Nanny_C"
  toCode Noglin_Nullifier = "EngramEntry_Noglin_Nullifier_C"
  toCode ElectricalOutlet = "EngramEntry_ElectricalOutlet_C"
  toCode PlanetShield = "EngramEntry_PlanetShield_C"
  toCode PressurePlateSS = "EngramEntry_PressurePlateSS_C"
  toCode RepairStation = "EngramEntry_RepairStation_C"
  toCode SheepHerder = "EngramEntry_SheepHerder_C"
  toCode ShieldPost = "EngramEntry_ShieldPost_C"
  toCode SpikeWall_Metal = "EngramEntry_SpikeWall_Metal_C"
  toCode SpikeWall_Wood = "EngramEntry_SpikeWall_Wood_C"
  toCode BookshelfPlus = "EngramEntry_BookshelfPlus_C"
  toCode StorageLarge = "EngramEntry_StorageLarge_C"
  toCode StorageMetal = "EngramEntry_StorageMetal_C"
  toCode StorageSmall = "EngramEntry_StorageSmall_C"
  toCode TekStorage = "EngramEntry_TekStorage_C"
  toCode TaxidermyPlus_Large = "EngramEntry_TaxidermyPlus_Large_C"
  toCode TaxidermyPlus_Medium = "EngramEntry_TaxidermyPlus_Medium_C"
  toCode TaxidermyPlus_Small = "EngramEntry_TaxidermyPlus_Small_C"
  toCode TekBridgePlus = "EngramEntry_TekBridgePlus_C"
  toCode TekChemBench = "EngramEntry_TekChemBench_C"
  toCode TekCookingPot = "EngramEntry_TekCookingPot_C"
  toCode TekForge = "EngramEntry_TekForge_C"
  toCode FridgeTek = "EngramEntry_FridgeTek_C"
  toCode JumpPadSS = "EngramEntry_JumpPadSS_C"
  toCode TekSensor = "EngramEntry_TekSensor_C"
  toCode TekStove = "EngramEntry_TekStove_C"
  toCode TeleporterPlus = "EngramEntry_TeleporterPlus_C"
  toCode StandingTorchPlus = "EngramEntry_StandingTorchPlus_C"
  toCode WallTorchPlus = "EngramEntry_WallTorchPlus_C"
  toCode TransmitterPlus = "EngramEntry_TransmitterPlus_C"
  toCode TreeSapTapPlus = "EngramEntry_TreeSapTapPlus_C"
  toCode TrophyBaseSS = "EngramEntry_TrophyBaseSS_C"
  toCode TrophyWallSS = "EngramEntry_TrophyWallSS_C"
  toCode VaultPlus = "EngramEntry_VaultPlus_C"
  toCode VesselPlus = "EngramEntry_VesselPlus_C"
  toCode Vivarium = "EngramEntry_Vivarium_C"
  toCode WaterTank_Metal = "EngramEntry_WaterTank_Metal_C"
  toCode WaterTank_Stone = "EngramEntry_WaterTank_Stone_C"
  toCode PipeFlex_MetalSP = "EngramEntry_PipeFlex_MetalSP_C"
  toCode PipeFlex_StoneSP = "EngramEntry_PipeFlex_StoneSP_C"
  toCode PipeHorizontal_Metal = "EngramEntry_PipeHorizontal_Metal_C"
  toCode PipeHorizontal_Stone = "EngramEntry_PipeHorizontal_Stone_C"
  toCode PipeIncline_Metal = "EngramEntry_PipeIncline_Metal_C"
  toCode PipeIncline_Stone = "EngramEntry_PipeIncline_Stone_C"
  toCode PipeIntake_Metal = "EngramEntry_PipeIntake_Metal_C"
  toCode PipeIntake_Stone = "EngramEntry_PipeIntake_Stone_C"
  toCode InternalPipe_Pillar = "EngramEntry_InternalPipe_Pillar_C"
  toCode InternalPipe_Square = "EngramEntry_InternalPipe_Square_C"
  toCode InternalPipe_Triangle = "EngramEntry_InternalPipe_Triangle_C"
  toCode InternalPipe_Wall = "EngramEntry_InternalPipe_Wall_C"
  toCode PipeIntersection_Metal = "EngramEntry_PipeIntersection_Metal_C"
  toCode PipeIntersection_Stone = "EngramEntry_PipeIntersection_Stone_C"
  toCode WaterTap_Metal = "EngramEntry_WaterTap_Metal_C"
  toCode WaterTap_Stone = "EngramEntry_WaterTap_Stone_C"
  toCode PipeVertical_Metal = "EngramEntry_PipeVertical_Metal_C"
  toCode PipeVertical_Stone = "EngramEntry_PipeVertical_Stone_C"
  toCode Catwalk_Glass = "EngramEntry_Catwalk_Glass_C"
  toCode Catwalk_Metal = "EngramEntry_Catwalk_Metal_C"
  toCode Catwalk_Tek = "EngramEntry_Catwalk_Tek_C"
  toCode Catwalk_Wood = "EngramEntry_Catwalk_Wood_C"
  toCode Ceiling_Adobe = "EngramEntry_Ceiling_Adobe_C"
  toCode Ceiling_Glass = "EngramEntry_Ceiling_Glass_C"
  toCode Ceiling_Greenhouse = "EngramEntry_Ceiling_Greenhouse_C"
  toCode Ceiling_Metal = "EngramEntry_Ceiling_Metal_C"
  toCode Ceiling_Stone = "EngramEntry_Ceiling_Stone_C"
  toCode Ceiling_Tek = "EngramEntry_Ceiling_Tek_C"
  toCode Ceiling_TekElevator = "EngramEntry_Ceiling_TekElevator_C"
  toCode Ceiling_TekGlass = "EngramEntry_Ceiling_TekGlass_C"
  toCode Ceiling_Thatch = "EngramEntry_Ceiling_Thatch_C"
  toCode Ceiling_Wood = "EngramEntry_Ceiling_Wood_C"
  toCode TriCeiling_AdobeSP = "EngramEntry_TriCeiling_AdobeSP_C"
  toCode TriCeiling_Glass = "EngramEntry_TriCeiling_Glass_C"
  toCode TriCeiling_GreenhouseSP = "EngramEntry_TriCeiling_GreenhouseSP_C"
  toCode TriCeiling_MetalSP = "EngramEntry_TriCeiling_MetalSP_C"
  toCode TriCeiling_StoneSP = "EngramEntry_TriCeiling_StoneSP_C"
  toCode TriCeiling_TekSP = "EngramEntry_TriCeiling_TekSP_C"
  toCode TriCeiling_TekElevator = "EngramEntry_TriCeiling_TekElevator_C"
  toCode TriCeiling_WoodSP = "EngramEntry_TriCeiling_WoodSP_C"
  toCode Doorframe_Adobe = "EngramEntry_Doorframe_Adobe_C"
  toCode Doorframe_Glass = "EngramEntry_Doorframe_Glass_C"
  toCode Doorframe_Greenhouse = "EngramEntry_Doorframe_Greenhouse_C"
  toCode Doorframe_Metal = "EngramEntry_Doorframe_Metal_C"
  toCode Doorframe_Stone = "EngramEntry_Doorframe_Stone_C"
  toCode Doorframe_Tek = "EngramEntry_Doorframe_Tek_C"
  toCode Doorframe_Thatch = "EngramEntry_Doorframe_Thatch_C"
  toCode Doorframe_Wood = "EngramEntry_Doorframe_Wood_C"
  toCode DoubleDoorframe_AdobeSP = "EngramEntry_DoubleDoorframe_AdobeSP_C"
  toCode DoubleDoorframe_Glass = "EngramEntry_DoubleDoorframe_Glass_C"
  toCode DoubleDoorframe_GreenhouseSP = "EngramEntry_DoubleDoorframe_GreenhouseSP_C"
  toCode DoubleDoorframe_MetalSP = "EngramEntry_DoubleDoorframe_MetalSP_C"
  toCode DoubleDoorframe_StoneSP = "EngramEntry_DoubleDoorframe_StoneSP_C"
  toCode DoubleDoorframe_TekSP = "EngramEntry_DoubleDoorframe_TekSP_C"
  toCode DoubleDoorframe_WoodSP = "EngramEntry_DoubleDoorframe_WoodSP_C"
  toCode FenceFoundation_Adobe = "EngramEntry_FenceFoundation_Adobe_C"
  toCode FenceFoundation_Metal = "EngramEntry_FenceFoundation_Metal_C"
  toCode FenceFoundation_Stone = "EngramEntry_FenceFoundation_Stone_C"
  toCode FenceFoundation_Tek = "EngramEntry_FenceFoundation_Tek_C"
  toCode FenceFoundation_Wood = "EngramEntry_FenceFoundation_Wood_C"
  toCode FenceSupport_AdobeSP = "EngramEntry_FenceSupport_AdobeSP_C"
  toCode FenceSupport_MetalSP = "EngramEntry_FenceSupport_MetalSP_C"
  toCode FenceSupport_StoneSP = "EngramEntry_FenceSupport_StoneSP_C"
  toCode FenceSupport_TekSP = "EngramEntry_FenceSupport_TekSP_C"
  toCode FenceSupport_WoodSP = "EngramEntry_FenceSupport_WoodSP_C"
  toCode Foundation_Adobe = "EngramEntry_Foundation_Adobe_C"
  toCode Foundation_Glass = "EngramEntry_Foundation_Glass_C"
  toCode Foundation_Metal = "EngramEntry_Foundation_Metal_C"
  toCode Foundation_Ocean_Metal = "EngramEntry_Foundation_Ocean_Metal_C"
  toCode Foundation_Ocean_Wood = "EngramEntry_Foundation_Ocean_Wood_C"
  toCode Foundation_Stone = "EngramEntry_Foundation_Stone_C"
  toCode Foundation_Tek = "EngramEntry_Foundation_Tek_C"
  toCode Foundation_Thatch = "EngramEntry_Foundation_Thatch_C"
  toCode Foundation_Wood = "EngramEntry_Foundation_Wood_C"
  toCode TriFoundation_AdobeSP = "EngramEntry_TriFoundation_AdobeSP_C"
  toCode TriFoundation_Glass = "EngramEntry_TriFoundation_Glass_C"
  toCode TriFoundation_MetalSP = "EngramEntry_TriFoundation_MetalSP_C"
  toCode TriFoundation_StoneSP = "EngramEntry_TriFoundation_StoneSP_C"
  toCode TriFoundation_TekSP = "EngramEntry_TriFoundation_TekSP_C"
  toCode TriFoundation_WoodSP = "EngramEntry_TriFoundation_WoodSP_C"
  toCode Gateway_Dynamic = "EngramEntry_Gateway_Dynamic_C"
  toCode LargeGateway_Adobe = "EngramEntry_LargeGateway_Adobe_C"
  toCode LargeGateway_Metal = "EngramEntry_LargeGateway_Metal_C"
  toCode LargeGateway_Stone = "EngramEntry_LargeGateway_Stone_C"
  toCode LargeGateway_Tek = "EngramEntry_LargeGateway_Tek_C"
  toCode Gateway_Adobe = "EngramEntry_Gateway_Adobe_C"
  toCode Gateway_Metal = "EngramEntry_Gateway_Metal_C"
  toCode Gateway_Stone = "EngramEntry_Gateway_Stone_C"
  toCode Gateway_Tek = "EngramEntry_Gateway_Tek_C"
  toCode Gateway_Wood = "EngramEntry_Gateway_Wood_C"
  toCode Hatchframe_Adobe = "EngramEntry_Hatchframe_Adobe_C"
  toCode Hatchframe_Glass = "EngramEntry_Hatchframe_Glass_C"
  toCode Hatchframe_Metal = "EngramEntry_Hatchframe_Metal_C"
  toCode Hatchframe_Stone = "EngramEntry_Hatchframe_Stone_C"
  toCode Hatchframe_Tek = "EngramEntry_Hatchframe_Tek_C"
  toCode Hatchframe_Wood = "EngramEntry_Hatchframe_Wood_C"
  toCode LargeHatchframe_Adobe = "EngramEntry_LargeHatchframe_Adobe_C"
  toCode LargeHatchframe_Metal = "EngramEntry_LargeHatchframe_Metal_C"
  toCode LargeHatchframe_Stone = "EngramEntry_LargeHatchframe_Stone_C"
  toCode LargeHatchframe_Tek = "EngramEntry_LargeHatchframe_Tek_C"
  toCode LargeHatchframeSloped_Adobe = "EngramEntry_LargeHatchframeSloped_Adobe_C"
  toCode LargeHatchframeSloped_Metal = "EngramEntry_LargeHatchframeSloped_Metal_C"
  toCode LargeHatchframeSloped_Stone = "EngramEntry_LargeHatchframeSloped_Stone_C"
  toCode LargeHatchframeSloped_Tek = "EngramEntry_LargeHatchframeSloped_Tek_C"
  toCode XLHatchframe_Adobe = "EngramEntry_XLHatchframe_Adobe_C"
  toCode XLHatchframe_Metal = "EngramEntry_XLHatchframe_Metal_C"
  toCode XLHatchframe_Stone = "EngramEntry_XLHatchframe_Stone_C"
  toCode XLHatchframe_Tek = "EngramEntry_XLHatchframe_Tek_C"
  toCode XLHatchframeSloped_Adobe = "EngramEntry_XLHatchframeSloped_Adobe_C"
  toCode XLHatchframeSloped_Metal = "EngramEntry_XLHatchframeSloped_Metal_C"
  toCode XLHatchframeSloped_Stone = "EngramEntry_XLHatchframeSloped_Stone_C"
  toCode XLHatchframeSloped_Tek = "EngramEntry_XLHatchframeSloped_Tek_C"
  toCode DynamicPillar_Adobe = "EngramEntry_DynamicPillar_Adobe_C"
  toCode DynamicPillar_Metal = "EngramEntry_DynamicPillar_Metal_C"
  toCode DynamicPillar_Stone = "EngramEntry_DynamicPillar_Stone_C"
  toCode DynamicPillar_Tek = "EngramEntry_DynamicPillar_Tek_C"
  toCode DynamicPillar_Wood = "EngramEntry_DynamicPillar_Wood_C"
  toCode LargePillar_Adobe = "EngramEntry_LargePillar_Adobe_C"
  toCode LargePillar_Metal = "EngramEntry_LargePillar_Metal_C"
  toCode LargePillar_Stone = "EngramEntry_LargePillar_Stone_C"
  toCode LargePillar_Tek = "EngramEntry_LargePillar_Tek_C"
  toCode LargePillar_Wood = "EngramEntry_LargePillar_Wood_C"
  toCode MediumPillar_Adobe = "EngramEntry_MediumPillar_Adobe_C"
  toCode MediumPillar_Metal = "EngramEntry_MediumPillar_Metal_C"
  toCode MediumPillar_Stone = "EngramEntry_MediumPillar_Stone_C"
  toCode MediumPillar_Tek = "EngramEntry_MediumPillar_Tek_C"
  toCode MediumPillar_Wood = "EngramEntry_MediumPillar_Wood_C"
  toCode SmallPillar_Adobe = "EngramEntry_SmallPillar_Adobe_C"
  toCode SmallPillar_Metal = "EngramEntry_SmallPillar_Metal_C"
  toCode SmallPillar_Stone = "EngramEntry_SmallPillar_Stone_C"
  toCode SmallPillar_Tek = "EngramEntry_SmallPillar_Tek_C"
  toCode SmallPillar_Wood = "EngramEntry_SmallPillar_Wood_C"
  toCode Railing_Adobe = "EngramEntry_Railing_Adobe_C"
  toCode Railing_Glass = "EngramEntry_Railing_Glass_C"
  toCode Railing_Metal = "EngramEntry_Railing_Metal_C"
  toCode Railing_Stone = "EngramEntry_Railing_Stone_C"
  toCode Railing_Tek = "EngramEntry_Railing_Tek_C"
  toCode Railing_Wood = "EngramEntry_Railing_Wood_C"
  toCode Ramp_AdobeSP = "EngramEntry_Ramp_AdobeSP_C"
  toCode Ramp_Glass = "EngramEntry_Ramp_Glass_C"
  toCode Ramp_MetalSP = "EngramEntry_Ramp_MetalSP_C"
  toCode Ramp_StoneSP = "EngramEntry_Ramp_StoneSP_C"
  toCode Ramp_TekSP = "EngramEntry_Ramp_TekSP_C"
  toCode Ramp_WoodSP = "EngramEntry_Ramp_WoodSP_C"
  toCode Roof_Adobe = "EngramEntry_Roof_Adobe_C"
  toCode Roof_Glass = "EngramEntry_Roof_Glass_C"
  toCode Roof_Greenhouse = "EngramEntry_Roof_Greenhouse_C"
  toCode Roof_Metal = "EngramEntry_Roof_Metal_C"
  toCode Roof_Stone = "EngramEntry_Roof_Stone_C"
  toCode Roof_Tek = "EngramEntry_Roof_Tek_C"
  toCode Roof_TekGlass = "EngramEntry_Roof_TekGlass_C"
  toCode Roof_Thatch = "EngramEntry_Roof_Thatch_C"
  toCode Roof_Wood = "EngramEntry_Roof_Wood_C"
  toCode TriRoof_AdobeSP = "EngramEntry_TriRoof_AdobeSP_C"
  toCode TriRoof_Glass = "EngramEntry_TriRoof_Glass_C"
  toCode TriRoof_GreenhouseSP = "EngramEntry_TriRoof_GreenhouseSP_C"
  toCode TriRoof_MetalSP = "EngramEntry_TriRoof_MetalSP_C"
  toCode TriRoof_StoneSP = "EngramEntry_TriRoof_StoneSP_C"
  toCode TriRoof_TekSP = "EngramEntry_TriRoof_TekSP_C"
  toCode TriRoof_WoodSP = "EngramEntry_TriRoof_WoodSP_C"
  toCode SlopedWall_Adobe_Left = "EngramEntry_SlopedWall_Adobe_Left_C"
  toCode SlopedWall_Glass_Left = "EngramEntry_SlopedWall_Glass_Left_C"
  toCode SlopedWall_Greenhouse_Left = "EngramEntry_SlopedWall_Greenhouse_Left_C"
  toCode SlopedWall_Metal_Left = "EngramEntry_SlopedWall_Metal_Left_C"
  toCode SlopedWall_Stone_Left = "EngramEntry_SlopedWall_Stone_Left_C"
  toCode SlopedWall_Tek_Left = "EngramEntry_SlopedWall_Tek_Left_C"
  toCode SlopedWall_Thatch_Left = "EngramEntry_SlopedWall_Thatch_Left_C"
  toCode SlopedWall_Wood_Left = "EngramEntry_SlopedWall_Wood_Left_C"
  toCode SlopedWall_Adobe_Right = "EngramEntry_SlopedWall_Adobe_Right_C"
  toCode SlopedWall_Glass_Right = "EngramEntry_SlopedWall_Glass_Right_C"
  toCode SlopedWall_Greenhouse_Right = "EngramEntry_SlopedWall_Greenhouse_Right_C"
  toCode SlopedWall_Metal_Right = "EngramEntry_SlopedWall_Metal_Right_C"
  toCode SlopedWall_Stone_Right = "EngramEntry_SlopedWall_Stone_Right_C"
  toCode SlopedWall_Tek_Right = "EngramEntry_SlopedWall_Tek_Right_C"
  toCode SlopedWall_Thatch_Right = "EngramEntry_SlopedWall_Thatch_Right_C"
  toCode SlopedWall_Wood_Right = "EngramEntry_SlopedWall_Wood_Right_C"
  toCode Staircase_Adobe = "EngramEntry_Staircase_Adobe_C"
  toCode Staircase_Glass = "EngramEntry_Staircase_Glass_C"
  toCode Staircase_Metal = "EngramEntry_Staircase_Metal_C"
  toCode Staircase_Stone = "EngramEntry_Staircase_Stone_C"
  toCode Staircase_Tek = "EngramEntry_Staircase_Tek_C"
  toCode Staircase_Wood = "EngramEntry_Staircase_Wood_C"
  toCode Underwater_Cube = "EngramEntry_Underwater_Cube_C"
  toCode Underwater_Cube_Sloped = "EngramEntry_Underwater_Cube_Sloped_C"
  toCode Underwater_Moonpool = "EngramEntry_Underwater_Moonpool_C"
  toCode Wall_Adobe = "EngramEntry_Wall_Adobe_C"
  toCode Wall_Forcefield = "EngramEntry_Wall_Forcefield_C"
  toCode Wall_Glass = "EngramEntry_Wall_Glass_C"
  toCode Wall_Greenhouse = "EngramEntry_Wall_Greenhouse_C"
  toCode Wall_Metal = "EngramEntry_Wall_Metal_C"
  toCode Wall_Stone = "EngramEntry_Wall_Stone_C"
  toCode Wall_Tek = "EngramEntry_Wall_Tek_C"
  toCode Wall_TekGlass = "EngramEntry_Wall_TekGlass_C"
  toCode Wall_Thatch = "EngramEntry_Wall_Thatch_C"
  toCode Wall_Wood = "EngramEntry_Wall_Wood_C"
  toCode LargeWall_AdobeSP = "EngramEntry_LargeWall_AdobeSP_C"
  toCode LargeWall_Forcefield = "EngramEntry_LargeWall_Forcefield_C"
  toCode LargeWall_Glass = "EngramEntry_LargeWall_Glass_C"
  toCode LargeWall_MetalSP = "EngramEntry_LargeWall_MetalSP_C"
  toCode LargeWall_StoneSP = "EngramEntry_LargeWall_StoneSP_C"
  toCode LargeWall_TekSP = "EngramEntry_LargeWall_TekSP_C"
  toCode LargeWall_WoodSP = "EngramEntry_LargeWall_WoodSP_C"
  toCode XLWall_Adobe = "EngramEntry_XLWall_Adobe_C"
  toCode XLWall_Forcefield = "EngramEntry_XLWall_Forcefield_C"
  toCode XLWall_Glass = "EngramEntry_XLWall_Glass_C"
  toCode XLWall_Metal = "EngramEntry_XLWall_Metal_C"
  toCode XLWall_Stone = "EngramEntry_XLWall_Stone_C"
  toCode XLWall_Tek = "EngramEntry_XLWall_Tek_C"
  toCode XLWall_Wood = "EngramEntry_XLWall_Wood_C"
  toCode WindowWall_Adobe = "EngramEntry_WindowWall_Adobe_C"
  toCode WindowWall_Metal = "EngramEntry_WindowWall_Metal_C"
  toCode WindowWall_Stone = "EngramEntry_WindowWall_Stone_C"
  toCode WindowWall_Tek = "EngramEntry_WindowWall_Tek_C"
  toCode WindowWall_Wood = "EngramEntry_WindowWall_Wood_C"
  toCode Wire_FlexSP = "EngramEntry_Wire_FlexSP_C"
  toCode Wire_Horizontal = "EngramEntry_Wire_Horizontal_C"
  toCode Wire_Incline = "EngramEntry_Wire_Incline_C"
  toCode InternalWire_Pillar = "EngramEntry_InternalWire_Pillar_C"
  toCode InternalWire_Square = "EngramEntry_InternalWire_Square_C"
  toCode InternalWire_Triangle = "EngramEntry_InternalWire_Triangle_C"
  toCode InternalWire_Wall = "EngramEntry_InternalWire_Wall_C"
  toCode Wire_Intersection = "EngramEntry_Wire_Intersection_C"
  toCode Wire_Vertical = "EngramEntry_Wire_Vertical_C"
