module Engram.AccessoriesPlus (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = Mochila_Avanzado
  | Mochila_Avanzado_Glider_Lite
  | Mochila_Avanzado_Glider_Modern_Lite
  | Portable_BugRepellant
  | Portable_ChitinPaste
  | Portable_Clay
  | Portable_Gunpowder
  | Portable_LAntidote
  | Portable_Narcotic
  | Portable_PreservingSalt
  | Portable_Propellant
  | Portable_Sparkpowder
  | Portable_Stimulant
  | Mochila_Avanzado_Modern
  | Control_TEK_Backpack
  | Mochila_Basica
  | Mochila_Intermedio
  | Advanced_Quiver
  | Basic_Quiver
  | Estandarte


instance ToEngram Engram where
  toCode Mochila_Avanzado = "EngramEntry_Mochila_Avanzado_C"
  toCode Mochila_Avanzado_Glider_Lite = "EngramEntry_Mochila_Avanzado_Glider_Lite_C"
  toCode Mochila_Avanzado_Glider_Modern_Lite = "EngramEntry_Mochila_Avanzado_Glider_Modern_Lite_C"
  toCode Portable_BugRepellant = "EngramEntry_Portable_BugRepellant_C"
  toCode Portable_ChitinPaste = "EngramEntry_Portable_ChitinPaste_C"
  toCode Portable_Clay = "EngramEntry_Portable_Clay_C"
  toCode Portable_Gunpowder = "EngramEntry_Portable_Gunpowder_C"
  toCode Portable_LAntidote = "EngramEntry_Portable_LAntidote_C"
  toCode Portable_Narcotic = "EngramEntry_Portable_Narcotic_C"
  toCode Portable_PreservingSalt = "EngramEntry_Portable_PreservingSalt_C"
  toCode Portable_Propellant = "EngramEntry_Portable_Propellant_C"
  toCode Portable_Sparkpowder = "EngramEntry_Portable_Sparkpowder_C"
  toCode Portable_Stimulant = "EngramEntry_Portable_Stimulant_C"
  toCode Mochila_Avanzado_Modern = "EngramEntry_Mochila_Avanzado_Modern_C"
  toCode Control_TEK_Backpack = "EngramEntry_Control_TEK_Backpack_C"
  toCode Mochila_Basica = "EngramEntry_Mochila_Basica_C"
  toCode Mochila_Intermedio = "EngramEntry_Mochila_Intermedio_C"
  toCode Advanced_Quiver = "EngramEntry_Advanced_Quiver_C"
  toCode Basic_Quiver = "EngramEntry_Basic_Quiver_C"
  toCode Estandarte = "EngramEntry_Estandarte_C"
