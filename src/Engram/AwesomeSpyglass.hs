module Engram.AwesomeSpyglass (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = AwesomeSpyglass


instance ToEngram Engram where
  toCode AwesomeSpyglass = "EngramEntry_AwesomeSpyglass_C"
