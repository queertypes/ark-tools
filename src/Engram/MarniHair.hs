module Engram.MarniHair (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = MMHairstylesTable


instance ToEngram Engram where
  toCode MMHairstylesTable = "EngramEntry_MMHairstylesTable_C"
