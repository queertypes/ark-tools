module Engram.DinoStorage (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = SoulGun_DS
  | SoulFinder_DS
  | SoulTraps_DS
  | SoulTerminal_DS


instance ToEngram Engram where
  toCode SoulGun_DS = "EngramEntry_SoulGun_DS_C"
  toCode SoulFinder_DS = "EngramEntry_SoulFinder_DS_C"
  toCode SoulTraps_DS = "EngramEntry_SoulTraps_DS_C"
  toCode SoulTerminal_DS = "EngramEntry_SoulTerminal_DS_C"
