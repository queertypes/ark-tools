module Engram.ArkGenomes (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = ACModule_01_Enhanced
  | ACModule_02_Elite
  | ACModule_03_Wraith
  | ACModule_04_01_FireStorm
  | ACModule_04_02_SpiritPlague
  | ACModule_04_03_Glacier
  | ACModule_05_ShadowFall
  | ACModule_06_Ethereal
  | ACModule_07_NightVoid
  | ACModule_08_Transcendent
  | AVA
  | FCG
  | CRS
  | CCS
  | DinoGTE
  | DinoGSS
  | DinoGES
  | DC
  | DinoGEMS
  | SH
  | SerumExtractor
  | DinoTSC
  | GKM
  | CorruptNSC
  | DinoGSC
  | DSC
  | DinoESP
  | CorruptCryopod
  | ArrowTranq_Goji
  | HealthDart
  | OxygenDart
  | SlowDart
  | LingenDart
  | TranqDart_Goji
  | GenomeExtractor


instance ToEngram Engram where
  toCode ACModule_01_Enhanced = "EngramEntry_ACModule_01_Enhanced_C"
  toCode ACModule_02_Elite = "EngramEntry_ACModule_02_Elite_C"
  toCode ACModule_03_Wraith = "EngramEntry_ACModule_03_Wraith_C"
  toCode ACModule_04_01_FireStorm = "EngramEntry_ACModule_04_01_FireStorm_C"
  toCode ACModule_04_02_SpiritPlague = "EngramEntry_ACModule_04_02_SpiritPlague_C"
  toCode ACModule_04_03_Glacier = "EngramEntry_ACModule_04_03_Glacier_C"
  toCode ACModule_05_ShadowFall = "EngramEntry_ACModule_05_ShadowFall_C"
  toCode ACModule_06_Ethereal = "EngramEntry_ACModule_06_Ethereal_C"
  toCode ACModule_07_NightVoid = "EngramEntry_ACModule_07_NightVoid_C"
  toCode ACModule_08_Transcendent = "EngramEntry_ACModule_08_Transcendent_C"
  toCode AVA = "EngramEntry_AVA_C"
  toCode FCG = "EngramEntry_FCG_C"
  toCode CRS = "EngramEntry_CRS_C"
  toCode CCS = "EngramEntry_CCS_C"
  toCode DinoGTE = "EngramEntry_DinoGTE_C"
  toCode DinoGSS = "EngramEntry_DinoGSS_C"
  toCode DinoGES = "EngramEntry_DinoGES_C"
  toCode DC = "EngramEntry_DC_C"
  toCode DinoGEMS = "EngramEntry_DinoGEMS_C"
  toCode SH = "EngramEntry_SH_C"
  toCode SerumExtractor = "EngramEntry_SerumExtractor_C"
  toCode DinoTSC = "EngramEntry_DinoTSC_C"
  toCode GKM = "EngramEntry_GKM_C"
  toCode CorruptNSC = "EngramEntry_CorruptNSC_C"
  toCode DinoGSC = "EngramEntry_DinoGSC_C"
  toCode DSC = "EngramEntry_DSC_C"
  toCode DinoESP = "EngramEntry_DinoESP_C"
  toCode CorruptCryopod = "EngramEntry_CorruptCryopod_C"
  toCode ArrowTranq_Goji = "EngramEntry_ArrowTranq_Goji_C"
  toCode HealthDart = "EngramEntry_HealthDart_C"
  toCode OxygenDart = "EngramEntry_OxygenDart_C"
  toCode SlowDart = "EngramEntry_SlowDart_C"
  toCode LingenDart = "EngramEntry_LingenDart_C"
  toCode TranqDart_Goji = "EngramEntry_TranqDart_Goji_C"
  toCode GenomeExtractor = "EngramEntry_GenomeExtractor_C"
