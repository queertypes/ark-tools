module Engram.IndustrialForges (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = IndustrialForge
  | IndustrialForge65
  | IndustrialForge85


instance ToEngram Engram where
  toCode IndustrialForge = "EngramEntry_IndustrialForge_C"
  toCode IndustrialForge65 = "EngramEntry_IndustrialForge65_C"
  toCode IndustrialForge85 = "EngramEntry_IndustrialForge85_C"
