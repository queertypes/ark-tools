module Engram.ArkOmega (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = SeagullNecklace_ArkOmega
  | PegoNecklace_ArkOmega
  | Thief_ArkOmega
  | ReflectShield
  | PygmyArmor
  | Decoy
  | EggCracker
  | GodEssence
  | OmegaGlider
  | InfinityStone
  | MekSummoner
  | AntiGravityPotion
  | CollectivePotion
  | CraftingPotion
  | EleResPotion
  | EndlessFortitudePotion
  | FortitudePotion
  | GravityPotion
  | AlphaPotion
  | BasicPotion
  | BetaPotion
  | GodPotion
  | OmegaPotion
  | PrimePotion
  | UltimatePotion
  | MagicPotion
  | NightvisionPotion
  | RampagePotion
  | SirenPotion
  | EndlessSpiritualPotion
  | SpiritualPotion
  | AlphaStamina
  | BasicStamina
  | BetaStamina
  | GodStamina
  | OmegaStamina
  | PrimeStamina
  | UltimateStamina
  | StoneskinPotion
  | SuicidePotion
  | TamingPotion
  | WakePotion
  | OmegaRaft
  | AlphaSedatives_ArkOmega
  | AncientSedatives_ArkOmega
  | BetaSedatives_ArkOmega
  | GodlikeSedatives_ArkOmega
  | OmegaSedatives_ArkOmega
  | PrimeSedatives_ArkOmega
  | Sedatives_ArkOmega
  | UltimateSedatives_ArkOmega
  | UnstableSteak
  | UnstableWater
  | BossBeacon
  | ChromaticCannon
  | Craptivator
  | CrossMating
  | CursedStatue
  | DimensionalDumpster
  | DimensionalGrinder
  | DimensionalBox
  | RetrieveTerminal
  | DodoCage
  | DPSDummy
  | EggCollector
  | GodEgg
  | GoldenChest
  | HadesStandingTorch
  | ItemFrag
  | OmegaAltar
  | OmegaBeacon
  | OmegaWorkBench
  | QuantumCompactor
  | RedDwarf
  | RespawnPoint
  | ScrollStorage
  | FruitSeeder
  | SetMannequin
  | SoulCompressor
  | SoulFurnace
  | SoulGrinder
  | SoulMagnet
  | UniqueTotem
  | OmegaUpgradeBench
  | KibbleMachine
  | VoidVacuum
  | Boomstick
  | ClonerStaff
  | CosmicGrenade
  | DischargeBoomerang
  | EtherealSpear
  | Evis
  | Flashbang
  | Fortune
  | GatheringTool
  | GetawayOrb
  | Gravitron
  | HadesTorch
  | AlphaHealingDart
  | BetaHealingDart
  | HealingDart
  | OmegaHealingDart
  | PrimeHealingDart
  | UltimateHealingDart
  | Heartsong
  | Icicle
  | Inversion
  | KnockoutClub
  | Laser
  | Lurkers
  | WeaponMalice
  | CompoundAlphaKnockoutArrow
  | CompoundAncientKnockoutArrow
  | CompoundBetaKnockoutArrow
  | CompoundGodlikeKnockoutArrow
  | CompoundKnockoutArrow
  | CompoundOmegaKnockoutArrow
  | CompoundPrimeKnockoutArrow
  | CompoundUltimateKnockoutArrow
  | WebGun
  | MultiTool
  | Outburst
  | Phantom
  | Plaguefork
  | Ripper
  | Risk
  | Shatter
  | VersatileSlingshot
  | StoneSickle
  | Stormbringer
  | StormGrenade
  | Thunderbolt
  | AlphaArrowKnockout
  | AncientArrowKnockout
  | ArrowKnockout
  | BetaArrowKnockout
  | GodlikeArrowKnockout
  | OmegaArrowKnockout
  | PrimeArrowKnockout
  | UltimateArrowKnockout
  | Translocator
  | UndeadSword
  | Eggsecutioner
  | VacuumGrenade
  | VoidGrenade
  | Wabbajack
  | WakeDart
  | WarpBow
  | WyvernBreath



instance ToEngram Engram where
  toCode SeagullNecklace_ArkOmega = "EngramEntry_SeagullNecklace_ArkOmega_C"
  toCode PegoNecklace_ArkOmega = "EngramEntry_PegoNecklace_ArkOmega_C"
  toCode Thief_ArkOmega = "EngramEntry_Thief_ArkOmega_C"
  toCode ReflectShield = "EngramEntry_ReflectShield_C"
  toCode PygmyArmor = "EngramEntry_PygmyArmor_C"
  toCode Decoy = "EngramEntry_Decoy_C"
  toCode EggCracker = "EngramEntry_EggCracker_C"
  toCode GodEssence = "EngramEntry_GodEssence_C"
  toCode OmegaGlider = "EngramEntry_OmegaGlider_C"
  toCode InfinityStone = "EngramEntry_InfinityStone_C"
  toCode MekSummoner = "EngramEntry_MekSummoner_C"
  toCode AntiGravityPotion = "EngramEntry_AntiGravityPotion_C"
  toCode CollectivePotion = "EngramEntry_CollectivePotion_C"
  toCode CraftingPotion = "EngramEntry_CraftingPotion_C"
  toCode EleResPotion = "EngramEntry_EleResPotion_C"
  toCode EndlessFortitudePotion = "EngramEntry_EndlessFortitudePotion_C"
  toCode FortitudePotion = "EngramEntry_FortitudePotion_C"
  toCode GravityPotion = "EngramEntry_GravityPotion_C"
  toCode AlphaPotion = "EngramEntry_AlphaPotion_C"
  toCode BasicPotion = "EngramEntry_BasicPotion_C"
  toCode BetaPotion = "EngramEntry_BetaPotion_C"
  toCode GodPotion = "EngramEntry_GodPotion_C"
  toCode OmegaPotion = "EngramEntry_OmegaPotion_C"
  toCode PrimePotion = "EngramEntry_PrimePotion_C"
  toCode UltimatePotion = "EngramEntry_UltimatePotion_C"
  toCode MagicPotion = "EngramEntry_MagicPotion_C"
  toCode NightvisionPotion = "EngramEntry_NightvisionPotion_C"
  toCode RampagePotion = "EngramEntry_RampagePotion_C"
  toCode SirenPotion = "EngramEntry_SirenPotion_C"
  toCode EndlessSpiritualPotion = "EngramEntry_EndlessSpiritualPotion_C"
  toCode SpiritualPotion = "EngramEntry_SpiritualPotion_C"
  toCode AlphaStamina = "EngramEntry_AlphaStamina_C"
  toCode BasicStamina = "EngramEntry_BasicStamina_C"
  toCode BetaStamina = "EngramEntry_BetaStamina_C"
  toCode GodStamina = "EngramEntry_GodStamina_C"
  toCode OmegaStamina = "EngramEntry_OmegaStamina_C"
  toCode PrimeStamina = "EngramEntry_PrimeStamina_C"
  toCode UltimateStamina = "EngramEntry_UltimateStamina_C"
  toCode StoneskinPotion = "EngramEntry_StoneskinPotion_C"
  toCode SuicidePotion = "EngramEntry_SuicidePotion_C"
  toCode TamingPotion = "EngramEntry_TamingPotion_C"
  toCode WakePotion = "EngramEntry_WakePotion_C"
  toCode OmegaRaft = "EngramEntry_OmegaRaft_C"
  toCode AlphaSedatives_ArkOmega = "EngramEntry_AlphaSedatives_ArkOmega_C"
  toCode AncientSedatives_ArkOmega = "EngramEntry_AncientSedatives_ArkOmega_C"
  toCode BetaSedatives_ArkOmega = "EngramEntry_BetaSedatives_ArkOmega_C"
  toCode GodlikeSedatives_ArkOmega = "EngramEntry_GodlikeSedatives_ArkOmega_C"
  toCode OmegaSedatives_ArkOmega = "EngramEntry_OmegaSedatives_ArkOmega_C"
  toCode PrimeSedatives_ArkOmega = "EngramEntry_PrimeSedatives_ArkOmega_C"
  toCode Sedatives_ArkOmega = "EngramEntry_Sedatives_ArkOmega_C"
  toCode UltimateSedatives_ArkOmega = "EngramEntry_UltimateSedatives_ArkOmega_C"
  toCode UnstableSteak = "EngramEntry_UnstableSteak_C"
  toCode UnstableWater = "EngramEntry_UnstableWater_C"
  toCode BossBeacon = "EngramEntry_BossBeacon_C"
  toCode ChromaticCannon = "EngramEntry_ChromaticCannon_C"
  toCode Craptivator = "EngramEntry_Craptivator_C"
  toCode CrossMating = "EngramEntry_CrossMating_C"
  toCode CursedStatue = "EngramEntry_CursedStatue_C"
  toCode DimensionalDumpster = "EngramEntry_DimensionalDumpster_C"
  toCode DimensionalGrinder = "EngramEntry_DimensionalGrinder_C"
  toCode DimensionalBox = "EngramEntry_DimensionalBox_C"
  toCode RetrieveTerminal = "EngramEntry_RetrieveTerminal_C"
  toCode DodoCage = "EngramEntry_DodoCage_C"
  toCode DPSDummy = "EngramEntry_DPSDummy_C"
  toCode EggCollector = "EngramEntry_EggCollector_C"
  toCode GodEgg = "EngramEntry_GodEgg_C"
  toCode GoldenChest = "EngramEntry_GoldenChest_C"
  toCode HadesStandingTorch = "EngramEntry_HadesStandingTorch_C"
  toCode ItemFrag = "EngramEntry_ItemFrag_C"
  toCode OmegaAltar = "EngramEntry_OmegaAltar_C"
  toCode OmegaBeacon = "EngramEntry_OmegaBeacon_C"
  toCode OmegaWorkBench = "EngramEntry_OmegaWorkBench_C"
  toCode QuantumCompactor = "EngramEntry_QuantumCompactor_C"
  toCode RedDwarf = "EngramEntry_RedDwarf_C"
  toCode RespawnPoint = "EngramEntry_RespawnPoint_C"
  toCode ScrollStorage = "EngramEntry_ScrollStorage_C"
  toCode FruitSeeder = "EngramEntry_FruitSeeder_C"
  toCode SetMannequin = "EngramEntry_SetMannequin_C"
  toCode SoulCompressor = "EngramEntry_SoulCompressor_C"
  toCode SoulFurnace = "EngramEntry_SoulFurnace_C"
  toCode SoulGrinder = "EngramEntry_SoulGrinder_C"
  toCode SoulMagnet = "EngramEntry_SoulMagnet_C"
  toCode UniqueTotem = "EngramEntry_UniqueTotem_C"
  toCode OmegaUpgradeBench = "EngramEntry_OmegaUpgradeBench_C"
  toCode KibbleMachine = "EngramEntry_KibbleMachine_C"
  toCode VoidVacuum = "EngramEntry_VoidVacuum_C"
  toCode Boomstick = "EngramEntry_Boomstick_C"
  toCode ClonerStaff = "EngramEntry_ClonerStaff_C"
  toCode CosmicGrenade = "EngramEntry_CosmicGrenade_C"
  toCode DischargeBoomerang = "EngramEntry_DischargeBoomerang_C"
  toCode EtherealSpear = "EngramEntry_EtherealSpear_C"
  toCode Evis = "EngramEntry_Evis_C"
  toCode Flashbang = "EngramEntry_Flashbang_C"
  toCode Fortune = "EngramEntry_Fortune_C"
  toCode GatheringTool = "EngramEntry_GatheringTool_C"
  toCode GetawayOrb = "EngramEntry_GetawayOrb_C"
  toCode Gravitron = "EngramEntry_Gravitron_C"
  toCode HadesTorch = "EngramEntry_HadesTorch_C"
  toCode AlphaHealingDart = "EngramEntry_AlphaHealingDart_C"
  toCode BetaHealingDart = "EngramEntry_BetaHealingDart_C"
  toCode HealingDart = "EngramEntry_HealingDart_C"
  toCode OmegaHealingDart = "EngramEntry_OmegaHealingDart_C"
  toCode PrimeHealingDart = "EngramEntry_PrimeHealingDart_C"
  toCode UltimateHealingDart = "EngramEntry_UltimateHealingDart_C"
  toCode Heartsong = "EngramEntry_Heartsong_C"
  toCode Icicle = "EngramEntry_Icicle_C"
  toCode Inversion = "EngramEntry_Inversion_C"
  toCode KnockoutClub = "EngramEntry_KnockoutClub_C"
  toCode Laser = "EngramEntry_Laser_C"
  toCode Lurkers = "EngramEntry_Lurkers_C"
  toCode WeaponMalice = "EngramEntry_WeaponMalice_C"
  toCode CompoundAlphaKnockoutArrow = "EngramEntry_CompoundAlphaKnockoutArrow_C"
  toCode CompoundAncientKnockoutArrow = "EngramEntry_CompoundAncientKnockoutArrow_C"
  toCode CompoundBetaKnockoutArrow = "EngramEntry_CompoundBetaKnockoutArrow_C"
  toCode CompoundGodlikeKnockoutArrow = "EngramEntry_CompoundGodlikeKnockoutArrow_C"
  toCode CompoundKnockoutArrow = "EngramEntry_CompoundKnockoutArrow_C"
  toCode CompoundOmegaKnockoutArrow = "EngramEntry_CompoundOmegaKnockoutArrow_C"
  toCode CompoundPrimeKnockoutArrow = "EngramEntry_CompoundPrimeKnockoutArrow_C"
  toCode CompoundUltimateKnockoutArrow = "EngramEntry_CompoundUltimateKnockoutArrow_C"
  toCode WebGun = "EngramEntry_WebGun_C"
  toCode MultiTool = "EngramEntry_MultiTool_C"
  toCode Outburst = "EngramEntry_Outburst_C"
  toCode Phantom = "EngramEntry_Phantom_C"
  toCode Plaguefork = "EngramEntry_Plaguefork_C"
  toCode Ripper = "EngramEntry_Ripper_C"
  toCode Risk = "EngramEntry_Risk_C"
  toCode Shatter = "EngramEntry_Shatter_C"
  toCode VersatileSlingshot = "EngramEntry_VersatileSlingshot_C"
  toCode StoneSickle = "EngramEntry_StoneSickle_C"
  toCode Stormbringer = "EngramEntry_Stormbringer_C"
  toCode StormGrenade = "EngramEntry_StormGrenade_C"
  toCode Thunderbolt = "EngramEntry_Thunderbolt_C"
  toCode AlphaArrowKnockout = "EngramEntry_AlphaArrowKnockout_C"
  toCode AncientArrowKnockout = "EngramEntry_AncientArrowKnockout_C"
  toCode ArrowKnockout = "EngramEntry_ArrowKnockout_C"
  toCode BetaArrowKnockout = "EngramEntry_BetaArrowKnockout_C"
  toCode GodlikeArrowKnockout = "EngramEntry_GodlikeArrowKnockout_C"
  toCode OmegaArrowKnockout = "EngramEntry_OmegaArrowKnockout_C"
  toCode PrimeArrowKnockout = "EngramEntry_PrimeArrowKnockout_C"
  toCode UltimateArrowKnockout = "EngramEntry_ultimateArrowKnockout_C"
  toCode Translocator = "EngramEntry_Translocator_C"
  toCode UndeadSword = "EngramEntry_UndeadSword_C"
  toCode Eggsecutioner = "EngramEntry_Eggsecutioner_C"
  toCode VacuumGrenade = "EngramEntry_VacuumGrenade_C"
  toCode VoidGrenade = "EngramEntry_VoidGrenade_C"
  toCode Wabbajack = "EngramEntry_Wabbajack_C"
  toCode WakeDart = "EngramEntry_WakeDart_C"
  toCode WarpBow = "EngramEntry_WarpBow_C"
  toCode WyvernBreath = "EngramEntry_WyvernBreath_C"
