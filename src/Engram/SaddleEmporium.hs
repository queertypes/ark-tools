module Engram.SaddleEmporium (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = SaddleSkins_Deer_NivSadEmp
  | Saddle_Deer_NivSadEmp
  | HeavyArmorsSkins_Equus_NivSadEmp
  | HeavyArmors_Equus_NivSadEmp
  | LightArmorskins_Equus_NivSadEmp
  | LightArmors_Equus_NivSadEmp
  | RopeSkins_Equus_NivSadEmp
  | Rope_Equus_NivSadEmp
  | SaddleSkins_Equus_NivSadEmp
  | Saddle_Equus_NivSadEmp
  | SaddleSkins_Paracer_NivSadEmp
  | Saddle_ParacerPlat_NivSadEmp
  | Saddle_ParacerPlat_Wood_NivSadEmp
  | Saddle_Paracer_NivSadEmp
  | SaddleSkins_Para_NivSadEmp
  | Saddle_Para_NivSadEmp
  | SaddleSkins_Ptero_NivSadEmp
  | Saddle_Ptero_NivSadEmp
  | SaddleSkins_Raptor_NivSadEmp
  | Saddle_Raptor_NivSadEmp
  | SaddleSkins_Spino_NivSadEmp
  | Saddle_Spino_NivSadEmp
  | SaddleSkins_Trike_NivSadEmp
  | Saddle_Trike_NivSadEmp
  | Workbench_NivSadEmp


instance ToEngram Engram where
  toCode SaddleSkins_Deer_NivSadEmp = "EngramEntry_SaddleSkins_Deer_NivSadEmp_C"
  toCode Saddle_Deer_NivSadEmp = "EngramEntry_Saddle_Deer_NivSadEmp_C"
  toCode HeavyArmorsSkins_Equus_NivSadEmp = "EngramEntry_HeavyArmorsSkins_Equus_NivSadEmp_C"
  toCode HeavyArmors_Equus_NivSadEmp = "EngramEntry_HeavyArmors_Equus_NivSadEmp_C"
  toCode LightArmorskins_Equus_NivSadEmp = "EngramEntry_LightArmorskins_Equus_NivSadEmp_C"
  toCode LightArmors_Equus_NivSadEmp = "EngramEntry_LightArmors_Equus_NivSadEmp_C"
  toCode RopeSkins_Equus_NivSadEmp = "EngramEntry_RopeSkins_Equus_NivSadEmp_C"
  toCode Rope_Equus_NivSadEmp = "EngramEntry_Rope_Equus_NivSadEmp_C"
  toCode SaddleSkins_Equus_NivSadEmp = "EngramEntry_SaddleSkins_Equus_NivSadEmp_C"
  toCode Saddle_Equus_NivSadEmp = "EngramEntry_Saddle_Equus_NivSadEmp_C"
  toCode SaddleSkins_Paracer_NivSadEmp = "EngramEntry_SaddleSkins_Paracer_NivSadEmp_C"
  toCode Saddle_ParacerPlat_NivSadEmp = "EngramEntry_Saddle_ParacerPlat_NivSadEmp_C"
  toCode Saddle_ParacerPlat_Wood_NivSadEmp = "EngramEntry_Saddle_ParacerPlat_Wood_NivSadEmp_C"
  toCode Saddle_Paracer_NivSadEmp = "EngramEntry_Saddle_Paracer_NivSadEmp_C"
  toCode SaddleSkins_Para_NivSadEmp = "EngramEntry_SaddleSkins_Para_NivSadEmp_C"
  toCode Saddle_Para_NivSadEmp = "EngramEntry_Saddle_Para_NivSadEmp_C"
  toCode SaddleSkins_Ptero_NivSadEmp = "EngramEntry_SaddleSkins_Ptero_NivSadEmp_C"
  toCode Saddle_Ptero_NivSadEmp = "EngramEntry_Saddle_Ptero_NivSadEmp_C"
  toCode SaddleSkins_Raptor_NivSadEmp = "EngramEntry_SaddleSkins_Raptor_NivSadEmp_C"
  toCode Saddle_Raptor_NivSadEmp = "EngramEntry_Saddle_Raptor_NivSadEmp_C"
  toCode SaddleSkins_Spino_NivSadEmp = "EngramEntry_SaddleSkins_Spino_NivSadEmp_C"
  toCode Saddle_Spino_NivSadEmp = "EngramEntry_Saddle_Spino_NivSadEmp_C"
  toCode SaddleSkins_Trike_NivSadEmp = "EngramEntry_SaddleSkins_Trike_NivSadEmp_C"
  toCode Saddle_Trike_NivSadEmp = "EngramEntry_Saddle_Trike_NivSadEmp_C"
  toCode Workbench_NivSadEmp = "EngramEntry_Workbench_NivSadEmp_C"
