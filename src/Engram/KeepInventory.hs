module Engram.KeepInventory (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = FastTravelBed_DIK


instance ToEngram Engram where
  toCode FastTravelBed_DIK = "EngramEntry_FastTravelBed_DIK_C"
