module Engram.Vanilla (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = BasiliskSaddle
  | ChargeBattery
  | ChargeLantern
  | ClimbingPick
  | Fishbasket
  | GasCollector
  | Glider
  | Glowstick
  | HazardSuit_Boots
  | HazardSuit_Chest
  | HazardSuit_Gloves
  | HazardSuit_Helmet
  | HazardSuit_Pants
  | KarkinoSaddle
  | MetalCliffPlatform
  | Pliers
  | PortableRopeLadder
  | RavagerSaddle
  | RollratSaddle
  | ShadowDrakeSaddle
  | ShagRug
  | StoneCliffPlatform
  | TekRockDrakeSaddle
  | TekSniper
  | WoodElevatorPlatform_Large
  | WoodElevatorPlatform_Medium
  | WoodElevatorPlatform_Small
  | WoodElevator_Switch
  | WoodElevator_Track
  | ZiplineAmmo
  | ZiplineMotor
  | GlowStick
  | CryoFridge
  | DinoLeash
  | EmptyCryopod
  | GachaSaddle
  | GasBagsSaddle
  | IceJumperSaddle
  | MekBackpack_Cannon
  | MekBackpack_Missiles
  | MekBackpack_Shield
  | MekCannonShell
  | MekRocketPod
  | MekSpawner
  | OwlSaddle
  | RefinedElement
  | RefinedShards
  | SpindlesSaddle
  | StorageBox_Balloon
  | TaxidermyBase_Large
  | TaxidermyBase_Medium
  | TaxidermyBase_Small
  | TaxidermyTool
  | TekBridge
  | TekGravityGrenade
  | Saddle_Andrewsarchus
  | Saddle_Desmodus
  | CherufeSaddle
  | CruiseMissile
  | FishingNet
  | GiantTurtleSaddle
  | HoverSkiff
  | JumpPad
  | MiningDrill
  | OceanPlatformMetal
  | OceanPlatformWood
  | PressurePlate
  | ShoulderCannon
  | TekAlarm
  | TekClaws
  | TekGrenadeLauncher
  | TekSpaceWhaleSaddle
  | AmmoBox
  | Boulder_Fire
  | Canoe
  | EggIncubator
  | LoadoutMannequin
  | Milkglider_Saddle
  | Minigun
  | NetGun_Ammo
  | SpaceDolphin_Saddle
  | TekBow
  | TekCanteen
  | TekCropPlot
  | TekExosuit
  | TekHoverSail
  | TekPistol
  | TekSecuirtyConsole
  | Saddle_Amarga
  | AdvancedBullet
  | AdvancedRifleBullet
  | AdvancedSniperBullet
  | AggroTranqDart
  | AirConditioner
  | AlarmTrap
  | AnvilBench
  | ArrowStone
  | ArrowTranq
  | BallistaArrow
  | BallistaTurret
  | BearTrap
  | BearTrap_Large
  | BeerBarrel
  | BloodExtractor
  | Bola
  | Bookshelf
  | Bow
  | BugRepel
  | C4Ammo
  | Camera
  | Campfire
  | Cannon
  | CannonBall
  | Canteen
  | CatapultTurret
  | ChainBola
  | ChemBench
  | ChitinBoots
  | ChitinGloves
  | ChitinHelmet
  | ChitinPants
  | ChitinPaste
  | ChitinShirt
  | ClothBoots
  | ClothGloves
  | ClothHelmet
  | ClothPants
  | ClothShirt
  | Compass
  | CompostBin
  | CompoundArrow
  | CompoundBow
  | CookingPot
  | CropPlot_Large
  | CropPlot_Medium
  | CropPlot_Small
  | Crossbow
  | CureLow
  | ElectricProd
  | Electronics
  | ElevatorPlatformLarge
  | ElevatorPlatformMedium
  | ElevatorPlatformSmall
  | ElevatorTrack
  | Fabricator
  | FeedingTrough
  | Fireplace
  | FishingRod
  | Flag
  | FlagSingle
  | FlareLauncher
  | Flashlight
  | Forge
  | FurBoots
  | FurGloves
  | FurHelmet
  | FurPants
  | FurShirt
  | Furniture_WoodTable
  | GPS
  | GasGrenade
  | GasMask
  | GhillieBoots
  | GhillieGloves
  | GhillieHelmet
  | GhilliePants
  | GhillieShirt
  | GrapplingHook
  | Gravestone
  | GreenhouseCeiling
  | GreenhouseDoor
  | GreenhouseRoof
  | GreenhouseSlopedWall_Left
  | GreenhouseSlopedWall_Right
  | GreenhouseWall
  | GreenhouseWallWithDoor
  | GreenhouseWindow
  | Grenade
  | Grill
  | Grinder
  | Gunpowder
  | Handcuffs
  | HarpoonGun
  | HeavyTurret
  | HideBoots
  | HideGloves
  | HideHelmet
  | HidePants
  | HideShirt
  | HideSleepingBag
  | HoloScope
  | IceBox
  | IndustrialCookingPot
  | IndustrialForge
  | Keypad
  | Lamppost
  | LamppostOmni
  | Lance
  | Laser
  | MachinedPistol
  | MachinedRifle
  | MachinedShotgun
  | MachinedSniper
  | MagnifyingGlass
  | MetalBoots
  | MetalCatwalk
  | MetalCeiling
  | MetalCeilingWithTrapdoor
  | MetalCeilingWithTrapdoorGiant
  | MetalDoor
  | MetalFenceFoundation
  | MetalFloor
  | MetalGate
  | MetalGate_Large
  | MetalGateway
  | MetalGateway_Large
  | MetalGloves
  | MetalHatchet
  | MetalHelmet
  | MetalLadder
  | MetalPants
  | MetalPick
  | MetalPillar
  | MetalPipeIncline
  | MetalPipeIntake
  | MetalPipeIntersection
  | MetalPipeStraight
  | MetalPipeTap
  | MetalPipeVertical
  | MetalRailing
  | MetalRamp
  | MetalRoof
  | MetalShield
  | MetalShirt
  | MetalSickle
  | MetalSign
  | MetalSign_Large
  | MetalSign_Wall
  | MetalSlopedWall_Left
  | MetalSlopedWall_Right
  | MetalSpikeWall
  | MetalStairs
  | MetalTrapdoor
  | MetalTrapdoorGiant
  | MetalWall
  | MetalWallWithDoor
  | MetalWallWithWindow
  | MetalWindow
  | MinersHelmet
  | MinigunTurret
  | MiracleGro
  | ModernBed
  | MortarAndPestle
  | Motorboat
  | Narcotic
  | NightVisionGoggles
  | NotePaper
  | Paintbrush
  | PaintingCanvas
  | Parachute
  | Pike
  | Pistol
  | PoisonGrenade
  | PoisonTrap
  | Polymer
  | PowerCableIncline
  | PowerCableIntersection
  | PowerCableStraight
  | PowerCableVertical
  | PowerGenerator
  | PowerOutlet
  | PreservingBin
  | Radio
  | Raft
  | RefinedTranqDart
  | RiotBoots
  | RiotGloves
  | RiotHelmet
  | RiotPants
  | RiotShield
  | RiotShirt
  | RocketAmmo
  | RocketLauncher
  | RocketTurret
  | RopeLadder
  | Saddle_Allo
  | Saddle_Ankylo
  | Saddle_Argentavis
  | Saddle_Arthro
  | Saddle_Baryonyx
  | Saddle_Basilosaurus
  | Saddle_Beaver
  | Saddle_Carcha
  | Saddle_Carno
  | Saddle_Chalico
  | Saddle_Daeodon
  | Saddle_Deinonychus
  | Saddle_Diplodocus
  | Saddle_Direbear
  | Saddle_Doed
  | Saddle_Dolphin
  | Saddle_Dunkle
  | Saddle_Equus
  | Saddle_Galli
  | Saddle_Gigant
  | Saddle_Hyaenodon
  | Saddle_Iguanodon
  | Saddle_Kaprosuchus
  | Saddle_Mammoth
  | Saddle_Manta
  | Saddle_Megalania
  | Saddle_Megalodon
  | Saddle_Megalosaurus
  | Saddle_Megatherium
  | Saddle_Mosa
  | Saddle_Mosa_Platform
  | Saddle_Pachy
  | Saddle_PachyRhino
  | Saddle_Para
  | Saddle_Paracer
  | Saddle_Paracer_Platform
  | Saddle_Pela
  | Saddle_Phiomia
  | Saddle_Plesia
  | Saddle_Plesio_Platform
  | Saddle_Procop
  | Saddle_Ptero
  | Saddle_Quetz
  | Saddle_Quetz_Platform
  | Saddle_Raptor
  | Saddle_Rex
  | Saddle_Rhino
  | Saddle_Saber
  | Saddle_Sarco
  | Saddle_Sauro
  | Saddle_Sauro_Platform
  | Saddle_Scorpion
  | Saddle_Spider
  | Saddle_Spino
  | Saddle_Stag
  | Saddle_Stego
  | Saddle_Tapejara
  | Saddle_TerrorBird
  | Saddle_Therizino
  | Saddle_Thylaco
  | Saddle_Titano_Platform
  | Saddle_Toad
  | Saddle_Trike
  | Saddle_Tropeognathus
  | Saddle_Turtle
  | Saddle_Tuso
  | Saddle_Yuty
  | Scissors
  | Scope
  | ScubaBoots_Flippers
  | ScubaHelmet_Goggles
  | ScubaPants
  | ScubaShirt_SuitWithTank
  | SeaMine
  | Silencer
  | SimpleBed
  | SimpleBullet
  | SimpleRifle
  | SimpleRifleBullet
  | SimpleShotgun
  | SimpleShotgunBullet
  | Slingshot
  | Sparkpowder
  | Spear
  | SprayPainter
  | Spyglass
  | StandingTorch
  | Stimulant
  | StoneCeiling
  | StoneCeilingWithTrapdoor
  | StoneCeilingWithTrapdoorGiant
  | StoneClub
  | StoneDoor
  | StoneFenceFoundation
  | StoneFloor
  | StoneGate
  | StoneGateLarge
  | StoneGateway
  | StoneGateway_Large
  | StoneHatchet
  | StonePick
  | StonePillar
  | StonePipeInclined
  | StonePipeIntake
  | StonePipeIntersection
  | StonePipeStraight
  | StonePipeTap
  | StonePipeVertical
  | StoneRailing
  | StoneRoof
  | StoneSlopedWall_Left
  | StoneSlopedWall_Right
  | StoneStairs
  | StoneTrapdoor
  | StoneTrapdoorGiant
  | StoneWall
  | StoneWallWithDoor
  | StoneWallWithWindow
  | StoneWindow
  | StorageBox_Huge
  | StorageBox_Large
  | StorageBox_Small
  | SubstrateAbsorbent
  | Sword
  | TekATV
  | TekBoots
  | TekGenerator
  | TekGloves
  | TekGrenade
  | TekHelmet
  | TekLight
  | TekMegalodonSaddle
  | TekMosaSaddle
  | TekPants
  | TekReplicator
  | TekRexSaddle
  | TekRifle
  | TekShield
  | TekShieldArmor
  | TekShirt
  | TekSword
  | TekTapejaraSaddle
  | TekTeleporter
  | TekTransmitter
  | TekTrough
  | ThatchCeiling
  | ThatchDoor
  | ThatchFloor
  | ThatchRoof
  | ThatchSlopedWall_Right
  | ThatchSlopedWall_left
  | ThatchWall
  | ThatchWallWithDoor
  | Toilet
  | Torch
  | TrainingDummy
  | TranqDart
  | TranqSpearBolt
  | TransGPS
  | TransGPSAmmo
  | TreePlatformMetal
  | TreePlatformWood
  | TreeSapTap
  | TripwireC4
  | TrophyBase
  | TrophyWall
  | Turret
  | TurretTek
  | WallTorch
  | WarMap
  | Wardrums
  | WaterJar
  | WaterTank
  | WaterTankMetal
  | Waterskin
  | WeaponC4
  | WoodBench
  | WoodCage
  | WoodCatwalk
  | WoodCeiling
  | WoodCeilingWithTrapdoor
  | WoodChair
  | WoodDoor
  | WoodFenceFoundation
  | WoodFloor
  | WoodGate
  | WoodGateway
  | WoodLadder
  | WoodPillar
  | WoodRailing
  | WoodRamp
  | WoodRoof
  | WoodShield
  | WoodSign
  | WoodSign_Large
  | WoodSign_Wall
  | WoodSlopedWall_Left
  | WoodSlopedWall_Right
  | WoodSpikeWall
  | WoodStairs
  | WoodTrapdoor
  | WoodWall
  | WoodWallWithDoor
  | WoodWallWithWindow
  | WoodWindow
  | TekBed
  | TekCatwalk
  | TekCeiling
  | TekCeilingWithTrapdoor
  | TekCloningChamber
  | TekDoor
  | TekFenceFoundation
  | TekFloor
  | TekLadder
  | TekPillar
  | TekRailing
  | TekRamp
  | TekRoof
  | TekSlopedWall_Left
  | TekSlopedWall_Right
  | TekStairs
  | TekTrapdoor
  | TekUnderwaterBase
  | TekUnderwaterBase_BottomEntry
  | TekWall
  | TekWallWithDoor
  | TekWallWithWindow
  | TekWindow
  | Tek_Gate
  | Tek_Gate_Large
  | Tek_Gategrame
  | Tek_Gategrame_Large
  | DoubleDoor_Adobe
  | DoubleDoor_Greenhouse
  | DoubleDoor_Metal
  | DoubleDoor_Stone
  | DoubleDoor_Tek
  | DoubleDoor_Wood
  | DedicatedStorage
  | PipeFlex_Metal
  | PipeFlex_Stone
  | TriCeiling_Adobe
  | TriCeiling_Greenhouse
  | TriCeiling_Metal
  | TriCeiling_Stone
  | TriCeiling_Tek
  | TriCeiling_Wood
  | DoubleDoorframe_Adobe
  | DoubleDoorframe_Greenhouse
  | DoubleDoorframe_Metal
  | DoubleDoorframe_Stone
  | DoubleDoorframe_Tek
  | DoubleDoorframe_Wood
  | FenceSupport_Adobe
  | FenceSupport_Metal
  | FenceSupport_Stone
  | FenceSupport_Tek
  | FenceSupport_Wood
  | TriFoundation_Adobe
  | TriFoundation_Metal
  | TriFoundation_Stone
  | TriFoundation_Tek
  | TriFoundation_Wood
  | Ramp_Adobe
  | Ramp_Metal
  | Ramp_Stone
  | Ramp_Tek
  | Ramp_Wood
  | TriRoof_Adobe
  | TriRoof_Greenhouse
  | TriRoof_Metal
  | TriRoof_Stone
  | TriRoof_Tek
  | TriRoof_Wood
  | LargeWall_Adobe
  | LargeWall_Metal
  | LargeWall_Stone
  | LargeWall_Tek
  | LargeWall_Wood
  | Wire_Flex
  | Clay
  | PreservingSalt
  | Saddle_Camelsaurus
  | Saddle_Mantis
  | Saddle_Moth
  | Saddle_RockGolem
  | Saddle_SpineyLizard
  | DesertClothBoots
  | DesertClothGloves
  | DesertClothGooglesHelmet
  | DesertClothPants
  | DesertClothShirt
  | AdobeCeiling
  | AdobeCeilingDoorGiant
  | AdobeCeilingWithDoorWay_Giant
  | AdobeCeilingWithTrapdoor
  | AdobeDoor
  | AdobeFenceFoundation
  | AdobeFloor
  | AdobeGate
  | AdobeGate_Large
  | AdobeGateway
  | AdobeGateway_Large
  | AdobeLadder
  | AdobePillar
  | AdobeRailing
  | AdobeRamp
  | AdobeRoof
  | AdobeSlopedWall_Left
  | AdobeSlopedWall_Right
  | AdobeStairs
  | AdobeTrapdoor
  | AdobeWall
  | AdobeWallWithDoor
  | AdobeWallWithWindow
  | AdobeWindow
  | Mirror
  | Vessel
  | OilPump
  | Tent
  | WaterWell
  | WindTurbine
  | Boomerang
  | FlameArrow
  | OilJar
  | ChainSaw
  | ClusterGrenade
  | Flamethrower
  | FlamethrowerAmmo
  | Propellant
  | RocketHommingAmmo
  | WeaponWhip


instance ToEngram Engram where
  toCode BasiliskSaddle =  "EngramEntry_BasiliskSaddle_C"
  toCode ChargeBattery =  "EngramEntry_ChargeBattery_C"
  toCode ChargeLantern =  "EngramEntry_ChargeLantern_C"
  toCode ClimbingPick =  "EngramEntry_ClimbingPick_C"
  toCode Fishbasket =  "EngramEntry_Fishbasket_C"
  toCode GasCollector =  "EngramEntry_GasCollector_C"
  toCode Glider =  "EngramEntry_Glider_C"
  toCode Glowstick =  "EngramEntry_Glowstick_C"
  toCode HazardSuit_Boots = "EngramEntry_HazardSuit_Boots_C"
  toCode HazardSuit_Chest = "EngramEntry_HazardSuit_Chest_C"
  toCode HazardSuit_Gloves = "EngramEntry_HazardSuit_Gloves_C"
  toCode HazardSuit_Helmet = "EngramEntry_HazardSuit_Helmet_C"
  toCode HazardSuit_Pants = "EngramEntry_HazardSuit_Pants_C"
  toCode KarkinoSaddle = "EngramEntry_KarkinoSaddle_C"
  toCode MetalCliffPlatform = "EngramEntry_MetalCliffPlatform_C"
  toCode Pliers = "EngramEntry_Pliers_C"
  toCode PortableRopeLadder = "EngramEntry_PortableRopeLadder_C"
  toCode RavagerSaddle = "EngramEntry_RavagerSaddle_C"
  toCode RollratSaddle = "EngramEntry_RollratSaddle_C"
  toCode ShadowDrakeSaddle = "EngramEntry_ShadowDrakeSaddle_C"
  toCode ShagRug = "EngramEntry_ShagRug_C"
  toCode StoneCliffPlatform = "EngramEntry_StoneCliffPlatform_C"
  toCode TekRockDrakeSaddle = "EngramEntry_TekRockDrakeSaddle_C"
  toCode TekSniper = "EngramEntry_TekSniper_C"
  toCode WoodElevatorPlatform_Large = "EngramEntry_WoodElevatorPlatform_Large_C"
  toCode WoodElevatorPlatform_Medium = "EngramEntry_WoodElevatorPlatform_Medium_C"
  toCode WoodElevatorPlatform_Small = "EngramEntry_WoodElevatorPlatform_Small_C"
  toCode WoodElevator_Switch = "EngramEntry_WoodElevator_Switch_C"
  toCode WoodElevator_Track = "EngramEntry_WoodElevator_Track_C"
  toCode ZiplineAmmo = "EngramEntry_ZiplineAmmo_C"
  toCode ZiplineMotor = "EngramEntry_ZiplineMotor_C"
  toCode GlowStick = "EngramEntry_GlowStick_C"
  toCode CryoFridge = "EngramEntry_CryoFridge_C"
  toCode DinoLeash = "EngramEntry_DinoLeash_C"
  toCode EmptyCryopod = "EngramEntry_EmptyCryopod_C"
  toCode GachaSaddle = "EngramEntry_GachaSaddle_C"
  toCode GasBagsSaddle = "EngramEntry_GasBagsSaddle_C"
  toCode IceJumperSaddle = "EngramEntry_IceJumperSaddle_C"
  toCode MekBackpack_Cannon = "EngramEntry_MekBackpack_Cannon_C"
  toCode MekBackpack_Missiles = "EngramEntry_MekBackpack_Missiles_C"
  toCode MekBackpack_Shield = "EngramEntry_MekBackpack_Shield_C"
  toCode MekCannonShell = "EngramEntry_MekCannonShell_C"
  toCode MekRocketPod = "EngramEntry_MekRocketPod_C"
  toCode MekSpawner = "EngramEntry_MekSpawner_C"
  toCode OwlSaddle = "EngramEntry_OwlSaddle_C"
  toCode RefinedElement = "EngramEntry_RefinedElement_C"
  toCode RefinedShards = "EngramEntry_RefinedShards_C"
  toCode SpindlesSaddle = "EngramEntry_SpindlesSaddle_C"
  toCode StorageBox_Balloon = "EngramEntry_StorageBox_Balloon_C"
  toCode TaxidermyBase_Large = "EngramEntry_TaxidermyBase_Large_C"
  toCode TaxidermyBase_Medium = "EngramEntry_TaxidermyBase_Medium_C"
  toCode TaxidermyBase_Small = "EngramEntry_TaxidermyBase_Small_C"
  toCode TaxidermyTool = "EngramEntry_TaxidermyTool_C"
  toCode TekBridge = "EngramEntry_TekBridge_C"
  toCode TekGravityGrenade = "EngramEntry_TekGravityGrenade_C"
  toCode Saddle_Andrewsarchus = "EngramEntry_Saddle_Andrewsarchus_C"
  toCode Saddle_Desmodus = "EngramEntry_Saddle_Desmodus_C"
  toCode CherufeSaddle = "EngramEntry_CherufeSaddle_C"
  toCode CruiseMissile = "EngramEntry_CruiseMissile_C"
  toCode FishingNet = "EngramEntry_FishingNet_C"
  toCode GiantTurtleSaddle = "EngramEntry_GiantTurtleSaddle_C"
  toCode HoverSkiff = "EngramEntry_HoverSkiff_C"
  toCode JumpPad = "EngramEntry_JumpPad_C"
  toCode MiningDrill = "EngramEntry_MiningDrill_C"
  toCode OceanPlatformMetal = "EngramEntry_OceanPlatformMetal_C"
  toCode OceanPlatformWood = "EngramEntry_OceanPlatformWood_C"
  toCode PressurePlate = "EngramEntry_PressurePlate_C"
  toCode ShoulderCannon = "EngramEntry_ShoulderCannon_C"
  toCode TekAlarm = "EngramEntry_TekAlarm_C"
  toCode TekClaws = "EngramEntry_TekClaws_C"
  toCode TekGrenadeLauncher = "EngramEntry_TekGrenadeLauncher_C"
  toCode TekSpaceWhaleSaddle = "EngramEntry_TekSpaceWhaleSaddle_C"
  toCode AmmoBox = "EngramEntry_AmmoBox_C"
  toCode Boulder_Fire = "EngramEntry_Boulder_Fire_C"
  toCode Canoe = "EngramEntry_Canoe_C"
  toCode EggIncubator = "EngramEntry_EggIncubator_C"
  toCode LoadoutMannequin = "EngramEntry_LoadoutMannequin_C"
  toCode Milkglider_Saddle = "EngramEntry_Milkglider_Saddle_C"
  toCode Minigun = "EngramEntry_Minigun_C"
  toCode NetGun_Ammo = "EngramEntry_NetGun_Ammo_C"
  toCode SpaceDolphin_Saddle = "EngramEntry_SpaceDolphin_Saddle_C"
  toCode TekBow = "EngramEntry_TekBow_C"
  toCode TekCanteen = "EngramEntry_TekCanteen_C"
  toCode TekCropPlot = "EngramEntry_TekCropPlot_C"
  toCode TekExosuit = "EngramEntry_TekExosuit_C"
  toCode TekHoverSail = "EngramEntry_TekHoverSail_C"
  toCode TekPistol = "EngramEntry_TekPistol_C"
  toCode TekSecuirtyConsole = "EngramEntry_TekSecuirtyConsole_C"
  toCode Saddle_Amarga = "EngramEntry_Saddle_Amarga_C"
  toCode AdvancedBullet = "EngramEntry_AdvancedBullet_C"
  toCode AdvancedRifleBullet = "EngramEntry_AdvancedRifleBullet_C"
  toCode AdvancedSniperBullet = "EngramEntry_AdvancedSniperBullet_C"
  toCode AggroTranqDart = "EngramEntry_AggroTranqDart_C"
  toCode AirConditioner = "EngramEntry_AirConditioner_C"
  toCode AlarmTrap = "EngramEntry_AlarmTrap_C"
  toCode AnvilBench = "EngramEntry_AnvilBench_C"
  toCode ArrowStone = "EngramEntry_ArrowStone_C"
  toCode ArrowTranq = "EngramEntry_ArrowTranq_C"
  toCode BallistaArrow = "EngramEntry_BallistaArrow_C"
  toCode BallistaTurret = "EngramEntry_BallistaTurret_C"
  toCode BearTrap = "EngramEntry_BearTrap_C"
  toCode BearTrap_Large = "EngramEntry_BearTrap_Large_C"
  toCode BeerBarrel = "EngramEntry_BeerBarrel_C"
  toCode BloodExtractor = "EngramEntry_BloodExtractor_C"
  toCode Bola = "EngramEntry_Bola_C"
  toCode Bookshelf = "EngramEntry_Bookshelf_C"
  toCode Bow = "EngramEntry_Bow_C"
  toCode BugRepel = "EngramEntry_BugRepel_C"
  toCode C4Ammo = "EngramEntry_C4Ammo_C"
  toCode Camera = "EngramEntry_Camera_C"
  toCode Campfire = "EngramEntry_Campfire_C"
  toCode Cannon = "EngramEntry_Cannon_C"
  toCode CannonBall = "EngramEntry_CannonBall_C"
  toCode Canteen = "EngramEntry_Canteen_C"
  toCode CatapultTurret = "EngramEntry_CatapultTurret_C"
  toCode ChainBola = "EngramEntry_ChainBola_C"
  toCode ChemBench = "EngramEntry_ChemBench_C"
  toCode ChitinBoots = "EngramEntry_ChitinBoots_C"
  toCode ChitinGloves = "EngramEntry_ChitinGloves_C"
  toCode ChitinHelmet = "EngramEntry_ChitinHelmet_C"
  toCode ChitinPants = "EngramEntry_ChitinPants_C"
  toCode ChitinPaste = "EngramEntry_ChitinPaste_C"
  toCode ChitinShirt = "EngramEntry_ChitinShirt_C"
  toCode ClothBoots = "EngramEntry_ClothBoots_C"
  toCode ClothGloves = "EngramEntry_ClothGloves_C"
  toCode ClothHelmet = "EngramEntry_ClothHelmet_C"
  toCode ClothPants = "EngramEntry_ClothPants_C"
  toCode ClothShirt = "EngramEntry_ClothShirt_C"
  toCode Compass = "EngramEntry_Compass_C"
  toCode CompostBin = "EngramEntry_CompostBin_C"
  toCode CompoundArrow = "EngramEntry_CompoundArrow_C"
  toCode CompoundBow = "EngramEntry_CompoundBow_C"
  toCode CookingPot = "EngramEntry_CookingPot_C"
  toCode CropPlot_Large = "EngramEntry_CropPlot_Large_C"
  toCode CropPlot_Medium = "EngramEntry_CropPlot_Medium_C"
  toCode CropPlot_Small = "EngramEntry_CropPlot_Small_C"
  toCode Crossbow = "EngramEntry_Crossbow_C"
  toCode CureLow = "EngramEntry_CureLow_C"
  toCode ElectricProd = "EngramEntry_ElectricProd_C"
  toCode Electronics = "EngramEntry_Electronics_C"
  toCode ElevatorPlatformLarge = "EngramEntry_ElevatorPlatformLarge_C"
  toCode ElevatorPlatformMedium = "EngramEntry_ElevatorPlatformMedium_C"
  toCode ElevatorPlatformSmall = "EngramEntry_ElevatorPlatformSmall_C"
  toCode ElevatorTrack = "EngramEntry_ElevatorTrack_C"
  toCode Fabricator = "EngramEntry_Fabricator_C"
  toCode FeedingTrough = "EngramEntry_FeedingTrough_C"
  toCode Fireplace = "EngramEntry_Fireplace_C"
  toCode FishingRod = "EngramEntry_FishingRod_C"
  toCode Flag = "EngramEntry_Flag_C"
  toCode FlagSingle = "EngramEntry_FlagSingle_C"
  toCode FlareLauncher = "EngramEntry_FlareLauncher_C"
  toCode Flashlight = "EngramEntry_Flashlight_C"
  toCode Forge = "EngramEntry_Forge_C"
  toCode FurBoots = "EngramEntry_FurBoots_C"
  toCode FurGloves = "EngramEntry_FurGloves_C"
  toCode FurHelmet = "EngramEntry_FurHelmet_C"
  toCode FurPants = "EngramEntry_FurPants_C"
  toCode FurShirt = "EngramEntry_FurShirt_C"
  toCode Furniture_WoodTable = "EngramEntry_Furniture_WoodTable_C"
  toCode GPS = "EngramEntry_GPS_C"
  toCode GasGrenade = "EngramEntry_GasGrenade_C"
  toCode GasMask = "EngramEntry_GasMask_C"
  toCode GhillieBoots = "EngramEntry_GhillieBoots_C"
  toCode GhillieGloves = "EngramEntry_GhillieGloves_C"
  toCode GhillieHelmet = "EngramEntry_GhillieHelmet_C"
  toCode GhilliePants = "EngramEntry_GhilliePants_C"
  toCode GhillieShirt = "EngramEntry_GhillieShirt_C"
  toCode GrapplingHook = "EngramEntry_GrapplingHook_C"
  toCode Gravestone = "EngramEntry_Gravestone_C"
  toCode GreenhouseCeiling = "EngramEntry_GreenhouseCeiling_C"
  toCode GreenhouseDoor = "EngramEntry_GreenhouseDoor_C"
  toCode GreenhouseRoof = "EngramEntry_GreenhouseRoof_C"
  toCode GreenhouseSlopedWall_Left = "EngramEntry_GreenhouseSlopedWall_Left_C"
  toCode GreenhouseSlopedWall_Right = "EngramEntry_GreenhouseSlopedWall_Right_C"
  toCode GreenhouseWall = "EngramEntry_GreenhouseWall_C"
  toCode GreenhouseWallWithDoor = "EngramEntry_GreenhouseWallWithDoor_C"
  toCode GreenhouseWindow = "EngramEntry_GreenhouseWindow_C"
  toCode Grenade = "EngramEntry_Grenade_C"
  toCode Grill = "EngramEntry_Grill_C"
  toCode Grinder = "EngramEntry_Grinder_C"
  toCode Gunpowder = "EngramEntry_Gunpowder_C"
  toCode Handcuffs = "EngramEntry_Handcuffs_C"
  toCode HarpoonGun = "EngramEntry_HarpoonGun_C"
  toCode HeavyTurret = "EngramEntry_HeavyTurret_C"
  toCode HideBoots = "EngramEntry_HideBoots_C"
  toCode HideGloves = "EngramEntry_HideGloves_C"
  toCode HideHelmet = "EngramEntry_HideHelmet_C"
  toCode HidePants = "EngramEntry_HidePants_C"
  toCode HideShirt = "EngramEntry_HideShirt_C"
  toCode HideSleepingBag = "EngramEntry_HideSleepingBag_C"
  toCode HoloScope = "EngramEntry_HoloScope_C"
  toCode IceBox = "EngramEntry_IceBox_C"
  toCode IndustrialCookingPot = "EngramEntry_IndustrialCookingPot_C"
  toCode IndustrialForge = "EngramEntry_IndustrialForge_C"
  toCode Keypad = "EngramEntry_Keypad_C"
  toCode Lamppost = "EngramEntry_Lamppost_C"
  toCode LamppostOmni = "EngramEntry_LamppostOmni_C"
  toCode Lance = "EngramEntry_Lance_C"
  toCode Laser = "EngramEntry_Laser_C"
  toCode MachinedPistol = "EngramEntry_MachinedPistol_C"
  toCode MachinedRifle = "EngramEntry_MachinedRifle_C"
  toCode MachinedShotgun = "EngramEntry_MachinedShotgun_C"
  toCode MachinedSniper = "EngramEntry_MachinedSniper_C"
  toCode MagnifyingGlass = "EngramEntry_MagnifyingGlass_C"
  toCode MetalBoots = "EngramEntry_MetalBoots_C"
  toCode MetalCatwalk = "EngramEntry_MetalCatwalk_C"
  toCode MetalCeiling = "EngramEntry_MetalCeiling_C"
  toCode MetalCeilingWithTrapdoor = "EngramEntry_MetalCeilingWithTrapdoor_C"
  toCode MetalCeilingWithTrapdoorGiant = "EngramEntry_MetalCeilingWithTrapdoorGiant_C"
  toCode MetalDoor = "EngramEntry_MetalDoor_C"
  toCode MetalFenceFoundation = "EngramEntry_MetalFenceFoundation_C"
  toCode MetalFloor = "EngramEntry_MetalFloor_C"
  toCode MetalGate = "EngramEntry_MetalGate_C"
  toCode MetalGate_Large = "EngramEntry_MetalGate_Large_C"
  toCode MetalGateway = "EngramEntry_MetalGateway_C"
  toCode MetalGateway_Large = "EngramEntry_MetalGateway_Large_C"
  toCode MetalGloves = "EngramEntry_MetalGloves_C"
  toCode MetalHatchet = "EngramEntry_MetalHatchet_C"
  toCode MetalHelmet = "EngramEntry_MetalHelmet_C"
  toCode MetalLadder = "EngramEntry_MetalLadder_C"
  toCode MetalPants = "EngramEntry_MetalPants_C"
  toCode MetalPick = "EngramEntry_MetalPick_C"
  toCode MetalPillar = "EngramEntry_MetalPillar_C"
  toCode MetalPipeIncline = "EngramEntry_MetalPipeIncline_C"
  toCode MetalPipeIntake = "EngramEntry_MetalPipeIntake_C"
  toCode MetalPipeIntersection = "EngramEntry_MetalPipeIntersection_C"
  toCode MetalPipeStraight = "EngramEntry_MetalPipeStraight_C"
  toCode MetalPipeTap = "EngramEntry_MetalPipeTap_C"
  toCode MetalPipeVertical = "EngramEntry_MetalPipeVertical_C"
  toCode MetalRailing = "EngramEntry_MetalRailing_C"
  toCode MetalRamp = "EngramEntry_MetalRamp_C"
  toCode MetalRoof = "EngramEntry_MetalRoof_C"
  toCode MetalShield = "EngramEntry_MetalShield_C"
  toCode MetalShirt = "EngramEntry_MetalShirt_C"
  toCode MetalSickle = "EngramEntry_MetalSickle_C"
  toCode MetalSign = "EngramEntry_MetalSign_C"
  toCode MetalSign_Large = "EngramEntry_MetalSign_Large_C"
  toCode MetalSign_Wall = "EngramEntry_MetalSign_Wall_C"
  toCode MetalSlopedWall_Left = "EngramEntry_MetalSlopedWall_Left_C"
  toCode MetalSlopedWall_Right = "EngramEntry_MetalSlopedWall_Right_C"
  toCode MetalSpikeWall = "EngramEntry_MetalSpikeWall_C"
  toCode MetalStairs = "EngramEntry_MetalStairs_C"
  toCode MetalTrapdoor = "EngramEntry_MetalTrapdoor_C"
  toCode MetalTrapdoorGiant = "EngramEntry_MetalTrapdoorGiant_C"
  toCode MetalWall = "EngramEntry_MetalWall_C"
  toCode MetalWallWithDoor = "EngramEntry_MetalWallWithDoor_C"
  toCode MetalWallWithWindow = "EngramEntry_MetalWallWithWindow_C"
  toCode MetalWindow = "EngramEntry_MetalWindow_C"
  toCode MinersHelmet = "EngramEntry_MinersHelmet_C"
  toCode MinigunTurret = "EngramEntry_MinigunTurret_C"
  toCode MiracleGro = "EngramEntry_MiracleGro_C"
  toCode ModernBed = "EngramEntry_ModernBed_C"
  toCode MortarAndPestle = "EngramEntry_MortarAndPestle_C"
  toCode Motorboat = "EngramEntry_Motorboat_C"
  toCode Narcotic = "EngramEntry_Narcotic_C"
  toCode NightVisionGoggles = "EngramEntry_NightVisionGoggles_C"
  toCode NotePaper = "EngramEntry_NotePaper_C"
  toCode Paintbrush = "EngramEntry_Paintbrush_C"
  toCode PaintingCanvas = "EngramEntry_PaintingCanvas_C"
  toCode Parachute = "EngramEntry_Parachute_C"
  toCode Pike = "EngramEntry_Pike_C"
  toCode Pistol = "EngramEntry_Pistol_C"
  toCode PoisonGrenade = "EngramEntry_PoisonGrenade_C"
  toCode PoisonTrap = "EngramEntry_PoisonTrap_C"
  toCode Polymer = "EngramEntry_Polymer_C"
  toCode PowerCableIncline = "EngramEntry_PowerCableIncline_C"
  toCode PowerCableIntersection = "EngramEntry_PowerCableIntersection_C"
  toCode PowerCableStraight = "EngramEntry_PowerCableStraight_C"
  toCode PowerCableVertical = "EngramEntry_PowerCableVertical_C"
  toCode PowerGenerator = "EngramEntry_PowerGenerator_C"
  toCode PowerOutlet = "EngramEntry_PowerOutlet_C"
  toCode PreservingBin = "EngramEntry_PreservingBin_C"
  toCode Radio = "EngramEntry_Radio_C"
  toCode Raft = "EngramEntry_Raft_C"
  toCode RefinedTranqDart = "EngramEntry_RefinedTranqDart_C"
  toCode RiotBoots = "EngramEntry_RiotBoots_C"
  toCode RiotGloves = "EngramEntry_RiotGloves_C"
  toCode RiotHelmet = "EngramEntry_RiotHelmet_C"
  toCode RiotPants = "EngramEntry_RiotPants_C"
  toCode RiotShield = "EngramEntry_RiotShield_C"
  toCode RiotShirt = "EngramEntry_RiotShirt_C"
  toCode RocketAmmo = "EngramEntry_RocketAmmo_C"
  toCode RocketLauncher = "EngramEntry_RocketLauncher_C"
  toCode RocketTurret = "EngramEntry_RocketTurret_C"
  toCode RopeLadder = "EngramEntry_RopeLadder_C"
  toCode Saddle_Allo = "EngramEntry_Saddle_Allo_C"
  toCode Saddle_Ankylo = "EngramEntry_Saddle_Ankylo_C"
  toCode Saddle_Argentavis = "EngramEntry_Saddle_Argentavis_C"
  toCode Saddle_Arthro = "EngramEntry_Saddle_Arthro_C"
  toCode Saddle_Baryonyx = "EngramEntry_Saddle_Baryonyx_C"
  toCode Saddle_Basilosaurus = "EngramEntry_Saddle_Basilosaurus_C"
  toCode Saddle_Beaver = "EngramEntry_Saddle_Beaver_C"
  toCode Saddle_Carcha = "EngramEntry_Saddle_Carcha_C"
  toCode Saddle_Carno = "EngramEntry_Saddle_Carno_C"
  toCode Saddle_Chalico = "EngramEntry_Saddle_Chalico_C"
  toCode Saddle_Daeodon = "EngramEntry_Saddle_Daeodon_C"
  toCode Saddle_Deinonychus = "EngramEntry_Saddle_Deinonychus_C"
  toCode Saddle_Diplodocus = "EngramEntry_Saddle_Diplodocus_C"
  toCode Saddle_Direbear = "EngramEntry_Saddle_Direbear_C"
  toCode Saddle_Doed = "EngramEntry_Saddle_Doed_C"
  toCode Saddle_Dolphin = "EngramEntry_Saddle_Dolphin_C"
  toCode Saddle_Dunkle = "EngramEntry_Saddle_Dunkle_C"
  toCode Saddle_Equus = "EngramEntry_Saddle_Equus_C"
  toCode Saddle_Galli = "EngramEntry_Saddle_Galli_C"
  toCode Saddle_Gigant = "EngramEntry_Saddle_Gigant_C"
  toCode Saddle_Hyaenodon = "EngramEntry_Saddle_Hyaenodon_C"
  toCode Saddle_Iguanodon = "EngramEntry_Saddle_Iguanodon_C"
  toCode Saddle_Kaprosuchus = "EngramEntry_Saddle_Kaprosuchus_C"
  toCode Saddle_Mammoth = "EngramEntry_Saddle_Mammoth_C"
  toCode Saddle_Manta = "EngramEntry_Saddle_Manta_C"
  toCode Saddle_Megalania = "EngramEntry_Saddle_Megalania_C"
  toCode Saddle_Megalodon = "EngramEntry_Saddle_Megalodon_C"
  toCode Saddle_Megalosaurus = "EngramEntry_Saddle_Megalosaurus_C"
  toCode Saddle_Megatherium = "EngramEntry_Saddle_Megatherium_C"
  toCode Saddle_Mosa = "EngramEntry_Saddle_Mosa_C"
  toCode Saddle_Mosa_Platform = "EngramEntry_Saddle_Mosa_Platform_C"
  toCode Saddle_Pachy = "EngramEntry_Saddle_Pachy_C"
  toCode Saddle_PachyRhino = "EngramEntry_Saddle_PachyRhino_C"
  toCode Saddle_Para = "EngramEntry_Saddle_Para_C"
  toCode Saddle_Paracer = "EngramEntry_Saddle_Paracer_C"
  toCode Saddle_Paracer_Platform = "EngramEntry_Saddle_Paracer_Platform_C"
  toCode Saddle_Pela = "EngramEntry_Saddle_Pela_C"
  toCode Saddle_Phiomia = "EngramEntry_Saddle_Phiomia_C"
  toCode Saddle_Plesia = "EngramEntry_Saddle_Plesia_C"
  toCode Saddle_Plesio_Platform = "EngramEntry_Saddle_Plesio_Platform_C"
  toCode Saddle_Procop = "EngramEntry_Saddle_Procop_C"
  toCode Saddle_Ptero = "EngramEntry_Saddle_Ptero_C"
  toCode Saddle_Quetz = "EngramEntry_Saddle_Quetz_C"
  toCode Saddle_Quetz_Platform = "EngramEntry_Saddle_Quetz_Platform_C"
  toCode Saddle_Raptor = "EngramEntry_Saddle_Raptor_C"
  toCode Saddle_Rex = "EngramEntry_Saddle_Rex_C"
  toCode Saddle_Rhino = "EngramEntry_Saddle_Rhino_C"
  toCode Saddle_Saber = "EngramEntry_Saddle_Saber_C"
  toCode Saddle_Sarco = "EngramEntry_Saddle_Sarco_C"
  toCode Saddle_Sauro = "EngramEntry_Saddle_Sauro_C"
  toCode Saddle_Sauro_Platform = "EngramEntry_Saddle_Sauro_Platform_C"
  toCode Saddle_Scorpion = "EngramEntry_Saddle_Scorpion_C"
  toCode Saddle_Spider = "EngramEntry_Saddle_Spider_C"
  toCode Saddle_Spino = "EngramEntry_Saddle_Spino_C"
  toCode Saddle_Stag = "EngramEntry_Saddle_Stag_C"
  toCode Saddle_Stego = "EngramEntry_Saddle_Stego_C"
  toCode Saddle_Tapejara = "EngramEntry_Saddle_Tapejara_C"
  toCode Saddle_TerrorBird = "EngramEntry_Saddle_TerrorBird_C"
  toCode Saddle_Therizino = "EngramEntry_Saddle_Therizino_C"
  toCode Saddle_Thylaco = "EngramEntry_Saddle_Thylaco_C"
  toCode Saddle_Titano_Platform = "EngramEntry_Saddle_Titano_Platform_C"
  toCode Saddle_Toad = "EngramEntry_Saddle_Toad_C"
  toCode Saddle_Trike = "EngramEntry_Saddle_Trike_C"
  toCode Saddle_Tropeognathus = "EngramEntry_Saddle_Tropeognathus_C"
  toCode Saddle_Turtle = "EngramEntry_Saddle_Turtle_C"
  toCode Saddle_Tuso = "EngramEntry_Saddle_Tuso_C"
  toCode Saddle_Yuty = "EngramEntry_Saddle_Yuty_C"
  toCode Scissors = "EngramEntry_Scissors_C"
  toCode Scope = "EngramEntry_Scope_C"
  toCode ScubaBoots_Flippers = "EngramEntry_ScubaBoots_Flippers_C"
  toCode ScubaHelmet_Goggles = "EngramEntry_ScubaHelmet_Goggles_C"
  toCode ScubaPants = "EngramEntry_ScubaPants_C"
  toCode ScubaShirt_SuitWithTank = "EngramEntry_ScubaShirt_SuitWithTank_C"
  toCode SeaMine = "EngramEntry_SeaMine_C"
  toCode Silencer = "EngramEntry_Silencer_C"
  toCode SimpleBed = "EngramEntry_SimpleBed_C"
  toCode SimpleBullet = "EngramEntry_SimpleBullet_C"
  toCode SimpleRifle = "EngramEntry_SimpleRifle_C"
  toCode SimpleRifleBullet = "EngramEntry_SimpleRifleBullet_C"
  toCode SimpleShotgun = "EngramEntry_SimpleShotgun_C"
  toCode SimpleShotgunBullet = "EngramEntry_SimpleShotgunBullet_C"
  toCode Slingshot = "EngramEntry_Slingshot_C"
  toCode Sparkpowder = "EngramEntry_Sparkpowder_C"
  toCode Spear = "EngramEntry_Spear_C"
  toCode SprayPainter = "EngramEntry_SprayPainter_C"
  toCode Spyglass = "EngramEntry_Spyglass_C"
  toCode StandingTorch = "EngramEntry_StandingTorch_C"
  toCode Stimulant = "EngramEntry_Stimulant_C"
  toCode StoneCeiling = "EngramEntry_StoneCeiling_C"
  toCode StoneCeilingWithTrapdoor = "EngramEntry_StoneCeilingWithTrapdoor_C"
  toCode StoneCeilingWithTrapdoorGiant = "EngramEntry_StoneCeilingWithTrapdoorGiant_C"
  toCode StoneClub = "EngramEntry_StoneClub_C"
  toCode StoneDoor = "EngramEntry_StoneDoor_C"
  toCode StoneFenceFoundation = "EngramEntry_StoneFenceFoundation_C"
  toCode StoneFloor = "EngramEntry_StoneFloor_C"
  toCode StoneGate = "EngramEntry_StoneGate_C"
  toCode StoneGateLarge = "EngramEntry_StoneGateLarge_C"
  toCode StoneGateway = "EngramEntry_StoneGateway_C"
  toCode StoneGateway_Large = "EngramEntry_StoneGateway_Large_C"
  toCode StoneHatchet = "EngramEntry_StoneHatchet_C"
  toCode StonePick = "EngramEntry_StonePick_C"
  toCode StonePillar = "EngramEntry_StonePillar_C"
  toCode StonePipeInclined = "EngramEntry_StonePipeInclined_C"
  toCode StonePipeIntake = "EngramEntry_StonePipeIntake_C"
  toCode StonePipeIntersection = "EngramEntry_StonePipeIntersection_C"
  toCode StonePipeStraight = "EngramEntry_StonePipeStraight_C"
  toCode StonePipeTap = "EngramEntry_StonePipeTap_C"
  toCode StonePipeVertical = "EngramEntry_StonePipeVertical_C"
  toCode StoneRailing = "EngramEntry_StoneRailing_C"
  toCode StoneRoof = "EngramEntry_StoneRoof_C"
  toCode StoneSlopedWall_Left = "EngramEntry_StoneSlopedWall_Left_C"
  toCode StoneSlopedWall_Right = "EngramEntry_StoneSlopedWall_Right_C"
  toCode StoneStairs = "EngramEntry_StoneStairs_C"
  toCode StoneTrapdoor = "EngramEntry_StoneTrapdoor_C"
  toCode StoneTrapdoorGiant = "EngramEntry_StoneTrapdoorGiant_C"
  toCode StoneWall = "EngramEntry_StoneWall_C"
  toCode StoneWallWithDoor = "EngramEntry_StoneWallWithDoor_C"
  toCode StoneWallWithWindow = "EngramEntry_StoneWallWithWindow_C"
  toCode StoneWindow = "EngramEntry_StoneWindow_C"
  toCode StorageBox_Huge = "EngramEntry_StorageBox_Huge_C"
  toCode StorageBox_Large = "EngramEntry_StorageBox_Large_C"
  toCode StorageBox_Small = "EngramEntry_StorageBox_Small_C"
  toCode SubstrateAbsorbent = "EngramEntry_SubstrateAbsorbent_C"
  toCode Sword = "EngramEntry_Sword_C"
  toCode TekATV = "EngramEntry_TekATV_C"
  toCode TekBoots = "EngramEntry_TekBoots_C"
  toCode TekGenerator = "EngramEntry_TekGenerator_C"
  toCode TekGloves = "EngramEntry_TekGloves_C"
  toCode TekGrenade = "EngramEntry_TekGrenade_C"
  toCode TekHelmet = "EngramEntry_TekHelmet_C"
  toCode TekLight = "EngramEntry_TekLight_C"
  toCode TekMegalodonSaddle = "EngramEntry_TekMegalodonSaddle_C"
  toCode TekMosaSaddle = "EngramEntry_TekMosaSaddle_C"
  toCode TekPants = "EngramEntry_TekPants_C"
  toCode TekReplicator = "EngramEntry_TekReplicator_C"
  toCode TekRexSaddle = "EngramEntry_TekRexSaddle_C"
  toCode TekRifle = "EngramEntry_TekRifle_C"
  toCode TekShield = "EngramEntry_TekShield_C"
  toCode TekShieldArmor = "EngramEntry_TekShieldArmor_C"
  toCode TekShirt = "EngramEntry_TekShirt_C"
  toCode TekSword = "EngramEntry_TekSword_C"
  toCode TekTapejaraSaddle = "EngramEntry_TekTapejaraSaddle_C"
  toCode TekTeleporter = "EngramEntry_TekTeleporter_C"
  toCode TekTransmitter = "EngramEntry_TekTransmitter_C"
  toCode TekTrough = "EngramEntry_TekTrough_C"
  toCode ThatchCeiling = "EngramEntry_ThatchCeiling_C"
  toCode ThatchDoor = "EngramEntry_ThatchDoor_C"
  toCode ThatchFloor = "EngramEntry_ThatchFloor_C"
  toCode ThatchRoof = "EngramEntry_ThatchRoof_C"
  toCode ThatchSlopedWall_Right = "EngramEntry_ThatchSlopedWall_Right_C"
  toCode ThatchSlopedWall_left = "EngramEntry_ThatchSlopedWall_left_C"
  toCode ThatchWall = "EngramEntry_ThatchWall_C"
  toCode ThatchWallWithDoor = "EngramEntry_ThatchWallWithDoor_C"
  toCode Toilet = "EngramEntry_Toilet_C"
  toCode Torch = "EngramEntry_Torch_C"
  toCode TrainingDummy = "EngramEntry_TrainingDummy_C"
  toCode TranqDart = "EngramEntry_TranqDart_C"
  toCode TranqSpearBolt = "EngramEntry_TranqSpearBolt_C"
  toCode TransGPS = "EngramEntry_TransGPS_C"
  toCode TransGPSAmmo = "EngramEntry_TransGPSAmmo_C"
  toCode TreePlatformMetal = "EngramEntry_TreePlatformMetal_C"
  toCode TreePlatformWood = "EngramEntry_TreePlatformWood_C"
  toCode TreeSapTap = "EngramEntry_TreeSapTap_C"
  toCode TripwireC4 = "EngramEntry_TripwireC4_C"
  toCode TrophyBase = "EngramEntry_TrophyBase_C"
  toCode TrophyWall = "EngramEntry_TrophyWall_C"
  toCode Turret = "EngramEntry_Turret_C"
  toCode TurretTek = "EngramEntry_TurretTek_C"
  toCode WallTorch = "EngramEntry_WallTorch_C"
  toCode WarMap = "EngramEntry_WarMap_C"
  toCode Wardrums = "EngramEntry_Wardrums_C"
  toCode WaterJar = "EngramEntry_WaterJar_C"
  toCode WaterTank = "EngramEntry_WaterTank_C"
  toCode WaterTankMetal = "EngramEntry_WaterTankMetal_C"
  toCode Waterskin = "EngramEntry_Waterskin_C"
  toCode WeaponC4 = "EngramEntry_WeaponC4_C"
  toCode WoodBench = "EngramEntry_WoodBench_C"
  toCode WoodCage = "EngramEntry_WoodCage_C"
  toCode WoodCatwalk = "EngramEntry_WoodCatwalk_C"
  toCode WoodCeiling = "EngramEntry_WoodCeiling_C"
  toCode WoodCeilingWithTrapdoor = "EngramEntry_WoodCeilingWithTrapdoor_C"
  toCode WoodChair = "EngramEntry_WoodChair_C"
  toCode WoodDoor = "EngramEntry_WoodDoor_C"
  toCode WoodFenceFoundation = "EngramEntry_WoodFenceFoundation_C"
  toCode WoodFloor = "EngramEntry_WoodFloor_C"
  toCode WoodGate = "EngramEntry_WoodGate_C"
  toCode WoodGateway = "EngramEntry_WoodGateway_C"
  toCode WoodLadder = "EngramEntry_WoodLadder_C"
  toCode WoodPillar = "EngramEntry_WoodPillar_C"
  toCode WoodRailing = "EngramEntry_WoodRailing_C"
  toCode WoodRamp = "EngramEntry_WoodRamp_C"
  toCode WoodRoof = "EngramEntry_WoodRoof_C"
  toCode WoodShield = "EngramEntry_WoodShield_C"
  toCode WoodSign = "EngramEntry_WoodSign_C"
  toCode WoodSign_Large = "EngramEntry_WoodSign_Large_C"
  toCode WoodSign_Wall = "EngramEntry_WoodSign_Wall_C"
  toCode WoodSlopedWall_Left = "EngramEntry_WoodSlopedWall_Left_C"
  toCode WoodSlopedWall_Right = "EngramEntry_WoodSlopedWall_Right_C"
  toCode WoodSpikeWall = "EngramEntry_WoodSpikeWall_C"
  toCode WoodStairs = "EngramEntry_WoodStairs_C"
  toCode WoodTrapdoor = "EngramEntry_WoodTrapdoor_C"
  toCode WoodWall = "EngramEntry_WoodWall_C"
  toCode WoodWallWithDoor = "EngramEntry_WoodWallWithDoor_C"
  toCode WoodWallWithWindow = "EngramEntry_WoodWallWithWindow_C"
  toCode WoodWindow = "EngramEntry_WoodWindow_C"
  toCode TekBed = "EngramEntry_TekBed_C"
  toCode TekCatwalk = "EngramEntry_TekCatwalk_C"
  toCode TekCeiling = "EngramEntry_TekCeiling_C"
  toCode TekCeilingWithTrapdoor = "EngramEntry_TekCeilingWithTrapdoor_C"
  toCode TekCloningChamber = "EngramEntry_TekCloningChamber_C"
  toCode TekDoor = "EngramEntry_TekDoor_C"
  toCode TekFenceFoundation = "EngramEntry_TekFenceFoundation_C"
  toCode TekFloor = "EngramEntry_TekFloor_C"
  toCode TekLadder = "EngramEntry_TekLadder_C"
  toCode TekPillar = "EngramEntry_TekPillar_C"
  toCode TekRailing = "EngramEntry_TekRailing_C"
  toCode TekRamp = "EngramEntry_TekRamp_C"
  toCode TekRoof = "EngramEntry_TekRoof_C"
  toCode TekSlopedWall_Left = "EngramEntry_TekSlopedWall_Left_C"
  toCode TekSlopedWall_Right = "EngramEntry_TekSlopedWall_Right_C"
  toCode TekStairs = "EngramEntry_TekStairs_C"
  toCode TekTrapdoor = "EngramEntry_TekTrapdoor_C"
  toCode TekUnderwaterBase = "EngramEntry_TekUnderwaterBase_C"
  toCode TekUnderwaterBase_BottomEntry = "EngramEntry_TekUnderwaterBase_BottomEntry_C"
  toCode TekWall = "EngramEntry_TekWall_C"
  toCode TekWallWithDoor = "EngramEntry_TekWallWithDoor_C"
  toCode TekWallWithWindow = "EngramEntry_TekWallWithWindow_C"
  toCode TekWindow = "EngramEntry_TekWindow_C"
  toCode Tek_Gate = "EngramEntry_Tek_Gate_C"
  toCode Tek_Gate_Large = "EngramEntry_Tek_Gate_Large_C"
  toCode Tek_Gategrame = "EngramEntry_Tek_Gategrame_C"
  toCode Tek_Gategrame_Large = "EngramEntry_Tek_Gategrame_Large_C"
  toCode DoubleDoor_Adobe = "EngramEntry_DoubleDoor_Adobe_C"
  toCode DoubleDoor_Greenhouse = "EngramEntry_DoubleDoor_Greenhouse_C"
  toCode DoubleDoor_Metal = "EngramEntry_DoubleDoor_Metal_C"
  toCode DoubleDoor_Stone = "EngramEntry_DoubleDoor_Stone_C"
  toCode DoubleDoor_Tek = "EngramEntry_DoubleDoor_Tek_C"
  toCode DoubleDoor_Wood = "EngramEntry_DoubleDoor_Wood_C"
  toCode DedicatedStorage = "EngramEntry_DedicatedStorage_C"
  toCode PipeFlex_Metal = "EngramEntry_PipeFlex_Metal_C"
  toCode PipeFlex_Stone = "EngramEntry_PipeFlex_Stone_C"
  toCode TriCeiling_Adobe = "EngramEntry_TriCeiling_Adobe_C"
  toCode TriCeiling_Greenhouse = "EngramEntry_TriCeiling_Greenhouse_C"
  toCode TriCeiling_Metal = "EngramEntry_TriCeiling_Metal_C"
  toCode TriCeiling_Stone = "EngramEntry_TriCeiling_Stone_C"
  toCode TriCeiling_Tek = "EngramEntry_TriCeiling_Tek_C"
  toCode TriCeiling_Wood = "EngramEntry_TriCeiling_Wood_C"
  toCode DoubleDoorframe_Adobe = "EngramEntry_DoubleDoorframe_Adobe_C"
  toCode DoubleDoorframe_Greenhouse = "EngramEntry_DoubleDoorframe_Greenhouse_C"
  toCode DoubleDoorframe_Metal = "EngramEntry_DoubleDoorframe_Metal_C"
  toCode DoubleDoorframe_Stone = "EngramEntry_DoubleDoorframe_Stone_C"
  toCode DoubleDoorframe_Tek = "EngramEntry_DoubleDoorframe_Tek_C"
  toCode DoubleDoorframe_Wood = "EngramEntry_DoubleDoorframe_Wood_C"
  toCode FenceSupport_Adobe = "EngramEntry_FenceSupport_Adobe_C"
  toCode FenceSupport_Metal = "EngramEntry_FenceSupport_Metal_C"
  toCode FenceSupport_Stone = "EngramEntry_FenceSupport_Stone_C"
  toCode FenceSupport_Tek = "EngramEntry_FenceSupport_Tek_C"
  toCode FenceSupport_Wood = "EngramEntry_FenceSupport_Wood_C"
  toCode TriFoundation_Adobe = "EngramEntry_TriFoundation_Adobe_C"
  toCode TriFoundation_Metal = "EngramEntry_TriFoundation_Metal_C"
  toCode TriFoundation_Stone = "EngramEntry_TriFoundation_Stone_C"
  toCode TriFoundation_Tek = "EngramEntry_TriFoundation_Tek_C"
  toCode TriFoundation_Wood = "EngramEntry_TriFoundation_Wood_C"
  toCode Ramp_Adobe = "EngramEntry_Ramp_Adobe_C"
  toCode Ramp_Metal = "EngramEntry_Ramp_Metal_C"
  toCode Ramp_Stone = "EngramEntry_Ramp_Stone_C"
  toCode Ramp_Tek = "EngramEntry_Ramp_Tek_C"
  toCode Ramp_Wood = "EngramEntry_Ramp_Wood_C"
  toCode TriRoof_Adobe = "EngramEntry_TriRoof_Adobe_C"
  toCode TriRoof_Greenhouse = "EngramEntry_TriRoof_Greenhouse_C"
  toCode TriRoof_Metal = "EngramEntry_TriRoof_Metal_C"
  toCode TriRoof_Stone = "EngramEntry_TriRoof_Stone_C"
  toCode TriRoof_Tek = "EngramEntry_TriRoof_Tek_C"
  toCode TriRoof_Wood = "EngramEntry_TriRoof_Wood_C"
  toCode LargeWall_Adobe = "EngramEntry_LargeWall_Adobe_C"
  toCode LargeWall_Metal = "EngramEntry_LargeWall_Metal_C"
  toCode LargeWall_Stone = "EngramEntry_LargeWall_Stone_C"
  toCode LargeWall_Tek = "EngramEntry_LargeWall_Tek_C"
  toCode LargeWall_Wood = "EngramEntry_LargeWall_Wood_C"
  toCode Wire_Flex = "EngramEntry_Wire_Flex_C"
  toCode Clay = "EngramEntry_Clay_C"
  toCode PreservingSalt = "EngramEntry_PreservingSalt_C"
  toCode Saddle_Camelsaurus = "EngramEntry_Saddle_Camelsaurus_C"
  toCode Saddle_Mantis = "EngramEntry_Saddle_Mantis_C"
  toCode Saddle_Moth = "EngramEntry_Saddle_Moth_C"
  toCode Saddle_RockGolem = "EngramEntry_Saddle_RockGolem_C"
  toCode Saddle_SpineyLizard = "EngramEntry_Saddle_SpineyLizard_C"
  toCode DesertClothBoots = "EngramEntry_DesertClothBoots_C"
  toCode DesertClothGloves = "EngramEntry_DesertClothGloves_C"
  toCode DesertClothGooglesHelmet = "EngramEntry_DesertClothGooglesHelmet_C"
  toCode DesertClothPants = "EngramEntry_DesertClothPants_C"
  toCode DesertClothShirt = "EngramEntry_DesertClothShirt_C"
  toCode AdobeCeiling = "EngramEntry_AdobeCeiling_C"
  toCode AdobeCeilingDoorGiant = "EngramEntry_AdobeCeilingDoorGiant_C"
  toCode AdobeCeilingWithDoorWay_Giant = "EngramEntry_AdobeCeilingWithDoorWay_Giant_C"
  toCode AdobeCeilingWithTrapdoor = "EngramEntry_AdobeCeilingWithTrapdoor_C"
  toCode AdobeDoor = "EngramEntry_AdobeDoor_C"
  toCode AdobeFenceFoundation = "EngramEntry_AdobeFenceFoundation_C"
  toCode AdobeFloor = "EngramEntry_AdobeFloor_C"
  toCode AdobeGate = "EngramEntry_AdobeGate_C"
  toCode AdobeGate_Large = "EngramEntry_AdobeGate_Large_C"
  toCode AdobeGateway = "EngramEntry_AdobeGateway_C"
  toCode AdobeGateway_Large = "EngramEntry_AdobeGateway_Large_C"
  toCode AdobeLadder = "EngramEntry_AdobeLadder_C"
  toCode AdobePillar = "EngramEntry_AdobePillar_C"
  toCode AdobeRailing = "EngramEntry_AdobeRailing_C"
  toCode AdobeRamp = "EngramEntry_AdobeRamp_C"
  toCode AdobeRoof = "EngramEntry_AdobeRoof_C"
  toCode AdobeSlopedWall_Left = "EngramEntry_AdobeSlopedWall_Left_C"
  toCode AdobeSlopedWall_Right = "EngramEntry_AdobeSlopedWall_Right_C"
  toCode AdobeStairs = "EngramEntry_AdobeStairs_C"
  toCode AdobeTrapdoor = "EngramEntry_AdobeTrapdoor_C"
  toCode AdobeWall = "EngramEntry_AdobeWall_C"
  toCode AdobeWallWithDoor = "EngramEntry_AdobeWallWithDoor_C"
  toCode AdobeWallWithWindow = "EngramEntry_AdobeWallWithWindow_C"
  toCode AdobeWindow = "EngramEntry_AdobeWindow_C"
  toCode Mirror = "EngramEntry_Mirror_C"
  toCode Vessel = "EngramEntry_Vessel_C"
  toCode OilPump = "EngramEntry_OilPump_C"
  toCode Tent = "EngramEntry_Tent_C"
  toCode WaterWell = "EngramEntry_WaterWell_C"
  toCode WindTurbine = "EngramEntry_WindTurbine_C"
  toCode Boomerang = "EngramEntry_Boomerang_C"
  toCode FlameArrow = "EngramEntry_FlameArrow_C"
  toCode OilJar = "EngramEntry_OilJar_C"
  toCode ChainSaw = "EngramEntry_ChainSaw_C"
  toCode ClusterGrenade = "EngramEntry_ClusterGrenade_C"
  toCode Flamethrower = "EngramEntry_Flamethrower_C"
  toCode FlamethrowerAmmo = "EngramEntry_FlamethrowerAmmo_C"
  toCode Propellant = "EngramEntry_Propellant_C"
  toCode RocketHommingAmmo = "EngramEntry_RocketHommingAmmo_C"
  toCode WeaponWhip = "EngramEntry_WeaponWhip_C"
