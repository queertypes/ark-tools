module Engram.AdditionalLights (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = AL_CandleLights
  | AL_ColoredLanterns
  | AL_CraftingStation
  | AL_DiscoBall
  | AL_MetallicBulbLights
  | AL_WoodenBulbLights
  | AL_FireLights
  | AL_RingOfFire
  | AL_FloodLight
  | AL_HandheldCampingLamp
  | AL_HandheldCandelabra
  | AL_HandheldHeartLight
  | AL_HandheldLantern
  | AL_HandheldLantern02
  | AL_HandheldMoonLight
  | AL_HandheldSearchLight
  | AL_HandheldSkullCandleStick
  | AL_SkyLantern
  | AL_HandheldStarLight
  | AL_LightStick_White
  | AL_HandheldTorch
  | AL_Steps
  | AL_GlowBugs
  | AL_ColorChanger
  | AL_PositionController
  | AL_RemoteController
  | AL_SwitchConnector
  | AL_MetalLightSwitch
  | AL_WoodenLightSwitch
  | AL_LightSequencer
  | AL_EmergencyHelmetsClassic
  | AL_Spotlight
  | AL_StringLights


instance ToEngram Engram where
  toCode AL_CandleLights = "EngramEntry_AL_CandleLights_C"
  toCode AL_ColoredLanterns = "EngramEntry_AL_ColoredLanterns_C"
  toCode AL_CraftingStation = "EngramEntry_AL_CraftingStation_C"
  toCode AL_DiscoBall = "EngramEntry_AL_DiscoBall_C"
  toCode AL_MetallicBulbLights = "EngramEntry_AL_MetallicBulbLights_C"
  toCode AL_WoodenBulbLights = "EngramEntry_AL_WoodenBulbLights_C"
  toCode AL_FireLights = "EngramEntry_AL_FireLights_C"
  toCode AL_RingOfFire = "EngramEntry_AL_RingOfFire_C"
  toCode AL_FloodLight = "EngramEntry_AL_FloodLight_C"
  toCode AL_HandheldCampingLamp = "EngramEntry_AL_HandheldCampingLamp_C"
  toCode AL_HandheldCandelabra = "EngramEntry_AL_HandheldCandelabra_C"
  toCode AL_HandheldHeartLight = "EngramEntry_AL_HandheldHeartLight_C"
  toCode AL_HandheldLantern = "EngramEntry_AL_HandheldLantern_C"
  toCode AL_HandheldLantern02 = "EngramEntry_AL_HandheldLantern02_C"
  toCode AL_HandheldMoonLight = "EngramEntry_AL_HandheldMoonLight_C"
  toCode AL_HandheldSearchLight = "EngramEntry_AL_HandheldSearchLight_C"
  toCode AL_HandheldSkullCandleStick = "EngramEntry_AL_HandheldSkullCandleStick_C"
  toCode AL_SkyLantern = "EngramEntry_AL_SkyLantern_C"
  toCode AL_HandheldStarLight = "EngramEntry_AL_HandheldStarLight_C"
  toCode AL_LightStick_White = "EngramEntry_AL_LightStick_White_C"
  toCode AL_HandheldTorch = "EngramEntry_AL_HandheldTorch_C"
  toCode AL_Steps = "EngramEntry_AL_Steps_C"
  toCode AL_GlowBugs = "EngramEntry_AL_GlowBugs_C"
  toCode AL_ColorChanger = "EngramEntry_AL_ColorChanger_C"
  toCode AL_PositionController = "EngramEntry_AL_PositionController_C"
  toCode AL_RemoteController = "EngramEntry_AL_RemoteController_C"
  toCode AL_SwitchConnector = "EngramEntry_AL_SwitchConnector_C"
  toCode AL_MetalLightSwitch = "EngramEntry_AL_MetalLightSwitch_C"
  toCode AL_WoodenLightSwitch = "EngramEntry_AL_WoodenLightSwitch_C"
  toCode AL_LightSequencer = "EngramEntry_AL_LightSequencer_C"
  toCode AL_EmergencyHelmetsClassic = "EngramEntry_AL_EmergencyHelmetsClassic_C"
  toCode AL_Spotlight = "EngramEntry_AL_Spotlight_C"
  toCode AL_StringLights = "EngramEntry_AL_StringLights_C"
