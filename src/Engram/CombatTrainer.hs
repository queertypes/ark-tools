module Engram.CombatTrainer (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = RankingPistol


instance ToEngram Engram where
  toCode RankingPistol = "EngramEntry_RankingPistol_C"
