module Engram.ShinyDinos (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = ShinyProd
  | ShinyTracker


instance ToEngram Engram where
  toCode ShinyProd = "EngramEntry_ShinyProd_C"
  toCode ShinyTracker = "EngramEntry_ShinyTracker_C"
