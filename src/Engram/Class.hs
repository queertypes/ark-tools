module Engram.Class (ToEngram, toCode) where

import Data.Text (Text)

class ToEngram a where
  toCode :: a -> Text
