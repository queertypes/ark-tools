module Engram.SeedFarm (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = SF_BioTekGiga
  | SF_TheBioLankBoss
  | PotionPoopMaker
  | SF_Seed_Elemental
  | BP_ApprenticeCropPlot
  | BP_AscendantCropPlot
  | BP_JourneymanCropPlot
  | BP_MastercraftCropPlot
  | BP_TekCropPlot
  | UltimateTekPlot
  | MagicCompostBin
  | MagicReverseCrafter
  | SF_Ceiling_Greenhouse
  | SF_Ceiling_Stone
  | SF_Foundation
  | SF_SpoilerBin
  | SF_SuperSmallForge
  | SeedFarm_Table


instance ToEngram Engram where
  toCode SF_BioTekGiga = "EngramEntry_SF_BioTekGiga_C"
  toCode SF_TheBioLankBoss = "EngramEntry_SF_TheBioLankBoss_C"
  toCode PotionPoopMaker = "EngramEntry_PotionPoopMaker_C"
  toCode SF_Seed_Elemental = "EngramEntry_SF_Seed_Elemental_C"
  toCode BP_ApprenticeCropPlot = "EngramEntry_BP_ApprenticeCropPlot_C"
  toCode BP_AscendantCropPlot = "EngramEntry_BP_AscendantCropPlot_C"
  toCode BP_JourneymanCropPlot = "EngramEntry_BP_JourneymanCropPlot_C"
  toCode BP_MastercraftCropPlot = "EngramEntry_BP_MastercraftCropPlot_C"
  toCode BP_TekCropPlot = "EngramEntry_BP_TekCropPlot_C"
  toCode UltimateTekPlot = "EngramEntry_UltimateTekPlot_C"
  toCode MagicCompostBin = "EngramEntry_MagicCompostBin_C"
  toCode MagicReverseCrafter = "EngramEntry_MagicReverseCrafter_C"
  toCode SF_Ceiling_Greenhouse = "EngramEntry_SF_Ceiling_Greenhouse_C"
  toCode SF_Ceiling_Stone = "EngramEntry_SF_Ceiling_Stone_C"
  toCode SF_Foundation = "EngramEntry_SF_Foundation_C"
  toCode SF_SpoilerBin = "EngramEntry_SF_SpoilerBin_C"
  toCode SF_SuperSmallForge = "EngramEntry_SF_SuperSmallForge_C"
  toCode SeedFarm_Table = "EngramEntry_SeedFarm_Table_C"
