module Engram.Chetus (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = ChetuBola
  | BonusHatchet_TheChetu
  | BonusPick_TheChetu
  | ChetuDinoPickUp
  | BonusSickle
  | ChetuTranqBola
  | Ascender_Chetu
  | SpoilandCrafter_TheChetu


instance ToEngram Engram where
  toCode ChetuBola = "EngramEntry_ChetuBola_C"
  toCode BonusHatchet_TheChetu = "EngramEntry_BonusHatchet_TheChetu_C"
  toCode BonusPick_TheChetu = "EngramEntry_BonusPick_TheChetu_C"
  toCode ChetuDinoPickUp = "EngramEntry_ChetuDinoPickUp_C"
  toCode BonusSickle = "EngramEntry_BonusSickle_C"
  toCode ChetuTranqBola = "EngramEntry_ChetuTranqBola_C"
  toCode Ascender_Chetu = "EngramEntry_Ascender_Chetu_C"
  toCode SpoilandCrafter_TheChetu = "EngramEntry_SpoilandCrafter_TheChetu_C"
