module Engram.SolarPanels (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = RedDwarf_SolarPanel


instance ToEngram Engram where
  toCode RedDwarf_SolarPanel = "EngramEntry_RedDwarf_SolarPanel_C"
