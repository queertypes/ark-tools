module Engram.Generate (entry, entryL, entryS, unlock, unlockS) where

import Engram.Class (ToEngram(..))
import Data.Text (Text, pack)

-- | Generates an engram override meant to be available from the start
-- of the game.
entryS :: ToEngram a => a -> Text
entryS x = entry x 1 1 False True

-- | Generates a non-hidden engram override w/ no pre-reqs
entryL :: ToEngram a => a -> Int -> Int -> Text
entryL x lvl cost = entry x lvl cost False True

-- | Generates an engram override with the given parameters.
entry :: ToEngram a => a -> Int -> Int -> Bool -> Bool -> Text
entry x lvl cost hidden rmPrereqs =
  "OverrideNamedEngramEntries=(EngramClassName=\""
  <> toCode x
  <> "\",EngramLevelRequirement="
  <> ps lvl
  <> ",EngramPointsCost="
  <> ps cost
  <> ",EngramHidden="
  <> ps hidden
  <> ",RemoveEngramPreReq="
  <> ps rmPrereqs
  <> ")"
  where ps :: Show a => a -> Text
        ps = pack . show

-- | Generates auto-unlock entry for engram for LV 1
unlockS :: ToEngram a => a -> Text
unlockS x = unlock x 1

-- EngramEntryAutoUnlocks=(EngramClassName="EngramEntry_SubstrateAbsorbent_C",LevelToAutoUnlock=0)
-- | Generates auto-unlock entry for engram for LV lvl
unlock :: ToEngram a => a -> Int -> Text
unlock x lvl =
  "EngramEntryAutoUnlocks=(EngramClassName=\""
  <> toCode x
  <> "\",LevelToAutoUnlock="
  <> ps lvl
  <> ")"
  where ps :: Show a => a -> Text
        ps = pack . show
