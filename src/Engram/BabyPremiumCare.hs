module Engram.BabyPremiumCare (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = CareGiver
  | CareGiver_SE
  | CareGiver_Wyvern


instance ToEngram Engram where
  toCode CareGiver = "EngramEntry_CareGiver_C"
  toCode CareGiver_SE = "EngramEntry_CareGiver_SE_C"
  toCode CareGiver_Wyvern = "EngramEntry_CareGiver_Wyvern_C"
