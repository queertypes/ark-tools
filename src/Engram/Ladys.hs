module Engram.Ladys (Engram(..)) where

import Engram.Class (ToEngram(..))

data Engram
  = KitchenSupplies_LWCM
  | PottingShed_LWCM
  | CookingStove_LWCM
  | SmallCookingPot_LWCM


instance ToEngram Engram where
  toCode KitchenSupplies_LWCM = "EngramEntry_KitchenSupplies_LWCM_C"
  toCode PottingShed_LWCM = "EngramEntry_PottingShed_LWCM_C"
  toCode CookingStove_LWCM = "EngramEntry_CookingStove_LWCM_C"
  toCode SmallCookingPot_LWCM = "EngramEntry_SmallCookingPot_LWCM_C"
