module Recipe
  (Recipe(..), Item(..), Component(..), Tier(..),
   crystalForTier, recipeOverride, recipeOverrides)
where

import Data.Text (Text)
import Data.Text.Lazy.Builder.Int (decimal)
import Data.Text.Lazy.Builder (toLazyText)
import Data.Text.Lazy (toStrict)

data Recipe =
  Recipe Item [Component]
  deriving (Show,Eq)

data Component = Component Item Int deriving (Show,Eq)

data Tier
  = T1
  | T2
  | T3
  | T4
  | T5
  deriving (Show,Eq)

crystalForTier :: Tier -> Item
crystalForTier t = case t of
  T1 -> ElementalCrystal
  T2 -> ApprenticeCrystal
  T3 -> JourneyManCrystal
  T4 -> MastercraftCrystal
  T5 -> AscendantCrystal

data Item
  -- Vanilla ARK items
  = Metal
  | Crystal
  | Obsidian
  | RareFlower
  | RareMushroom
  | PlantXSeed
  | Mejoberry
  | MejoberrySeed
  | AmmoniteBile
  | AnglerGel
  | BioToxin
  | BlackPearl
  | BlueGem
  | BloodPack
  | CementingPaste
  | Charcoal
  | Chitin
  | Electronics
  | Element
  | Fiber
  | Flint
  | Gasoline
  | Gunpowder
  | Hide
  | Keratin
  | LeechBlood
  | MetalIngot
  | Mutagel
  | Mutagen
  | Narcotic
  | Oil
  | OrganicPolymer
  | Pelt
  | Polymer
  | RedGem
  | Salt
  | Sand
  | Sap
  | SilicaPearls
  | Silk
  | SparkPowder
  | Stimulant
  | Stone
  | Sulfur
  | Thatch
  | Wood
  | RawMeat
  | RawPrimeMeat
  | RawFishMeat
  | RawPrimeFishMeat
  | SpoiledMeat
  | BasicKibble
  | SimpleKibble
  | RegularKibble
  | SuperiorKibble
  | ExceptionalKibble
  | ExtraordinaryKibble
  | BlueColoring
  | GreenColoring
  | RedColoring
  | GiantBeeHoney
  | LargeCropPlot

  -- SeedFarm mod items
  --- Seeds and Crystals
  | ElementalCrystal
  | ElementalCrystalSeed
  | ApprenticeCrystal
  | ApprenticeCrystalSeed
  | JourneyManCrystal
  | JourneyManCrystalSeed
  | MastercraftCrystal
  | MastercraftCrystalSeed
  | AscendantCrystal
  | AscendantCrystalSeed
  | PlantXConsumable
  --- Bosses
  | BioTekGigaSpawner
  | BioLankBossSpawner
  | BioLankEssence
  --- Structures
  | SuperSmallForge
  | ApprenticeCropPlot
  | JourneyManCropPlot
  | MastercraftCropPlot
  | AscendantCropPlot
  | TekCropPlot
  | UltimateTekCropPlot
  --- Tiered Seeds
  ---- T1
  | RawMeatSeed
  | RawPrimeMeatSeed
  | RawFishMeatSeed
  | RawPrimeFishMeatSeed
  ---- T2
  | DyeSeed
  | FlintSeed
  | HideSeed
  | SpoiledMeatSeed
  | ThatchSeed
  ---- T3
  | BioToxinSeed
  | BloodPackSeed
  | ChitinSeed
  | FiberSeed
  | HoneySeed
  | LeechBloodSeed
  | OilSeed
  | PeltSeed
  | RareFlowerSeed
  | RareMushroomSeed
  | SandSeed
  | StoneSeed
  | WoodSeed
  ---- T4
  | AnglerGelSeed
  | CementingPasteSeed
  | CharcoalSeed
  | CrystalSeed
  | GasolineSeed
  | MetalSeed
  | NarcoticSeed
  | ObsidianSeed
  | SaltSeed
  | SapSeed
  | SilicaPearlsSeed
  | SilkSeed
  | SparkPowderSeed
  | StimulantSeed
  ---- T5
  | AmmoniteBileSeed
  | BlackPearlSeed
  | ElectronicsSeed
  | ElementSeed
  | GemSeed
  | GunpowderSeed
  | KibbleSeed
  | MetalIngotSeed
  | PolymerSeed
  deriving (Show,Eq)

recipeOverrides :: [Recipe] -> Text
recipeOverrides [] = ""
recipeOverrides (r:rs) =
  recipeOverride r
  <> "\n" <> recipeOverrides rs


recipeOverride :: Recipe -> Text
recipeOverride (Recipe i cs) =
  "ConfigOverrideItemCraftingCosts=(ItemClassString=\""
  <> itemId i
  <> "\",BaseCraftingResourceRequirements=("
  <> ue4ComponentResources cs
  <> "))"

ue4ComponentResources :: [Component] -> Text
ue4ComponentResources [] = ""
ue4ComponentResources [Component i amnt] =
  "(ResourceItemTypeString=\""
  <> itemId i
  <> "\",BaseResourceRequirement="
  <> dec amnt
  <> ",bCraftingRequireExactResourceType=false)"
  <> ue4ComponentResources []
ue4ComponentResources ((Component i amnt):cs) =
  "(ResourceItemTypeString=\""
  <> itemId i
  <> "\",BaseResourceRequirement="
  <> dec amnt
  <> ",bCraftingRequireExactResourceType=false),"
  <> ue4ComponentResources cs

dec :: Integral a => a -> Text
dec = toStrict . toLazyText . decimal

itemId :: Item -> Text
itemId i = case i of
  Metal -> "PrimalItemResource_Metal_C"
  Crystal -> "PrimalItemResource_Crystal_C"
  Obsidian -> "PrimalItemResource_Obsidian_C"
  RareFlower -> "PrimalItemResource_RareFlower_C"
  RareMushroom -> "PrimalItemResource_RareMushroom_C"
  PlantXSeed -> "PrimalItemConsumable_Seed_DefensePlant_C"
  Mejoberry -> "PrimalItemConsumable_Berry_Mejoberry_C"
  MejoberrySeed -> "PrimalItemConsumable_Seed_Mejoberry_C"
  AmmoniteBile -> "PrimalItemResource_AmmoniteBlood_C"
  AnglerGel -> "PrimalItemResource_AnglerGel_C"
  BioToxin -> "PrimalItemConsumable_JellyVenom_C"
  BlackPearl -> "PrimalItemResource_BlackPearl_C"
  BlueGem -> "PrimalItemResource_Gem_BioLum_C"
  BloodPack -> "PrimalItemConsumable_BloodPack_C"
  CementingPaste -> "PrimalItemResource_ChitinPaste_C"
  Charcoal -> "PrimalItemResource_Charcoal_C"
  Chitin -> "PrimalItemResource_Chitin_C"
  Electronics -> "PrimalItemResource_Electronics_C"
  Element -> "PrimalItemResource_Element_C"
  Fiber -> "PrimalItemResource_Fibers_C"
  Flint -> "PrimalItemResource_Flint_C"
  Gasoline -> "PrimalItemResource_Gasoline_C"
  Gunpowder -> "PrimalItemResource_Gunpowder_C"
  Hide -> "PrimalItemResource_Hide_C"
  Keratin -> "PrimalItemResource_Keratin_C"
  LeechBlood -> "PrimalItemResource_LeechBlood_C"
  MetalIngot -> "PrimalItemResource_MetalIngot_C"
  Mutagel -> "PrimalItemConsumable_Mutagel_C"
  Mutagen -> "PrimalItemConsumable_Mutagen_C"
  Narcotic -> "PrimalItemConsumable_Narcotic_C"
  Oil -> "PrimalItemResource_Oil_C"
  OrganicPolymer -> "PrimalItemResource_Polymer_Organic_C"
  Pelt -> "PrimalItemResource_Pelt_C"
  Polymer -> "PrimalItemResource_Polymer_C"
  RedGem -> "PrimalItemResource_Gem_Element_C"
  Salt -> "PrimalItemResource_RawSalt_C"
  Sand -> "PrimalItemResource_Sand_C"
  Sap -> "PrimalItemResource_Sap_C"
  SilicaPearls -> "PrimalItemResource_Silicon_C"
  Silk -> "PrimalItemResource_Silk_C"
  SparkPowder -> "PrimalItemResource_Sparkpowder_C"
  Stimulant -> "PrimalItemConsumable_Stimulant_C"
  Stone -> "PrimalItemResource_Stone_C"
  Sulfur -> "PrimalItemResource_Sulfur_C"
  Thatch -> "PrimalItemResource_Thatch_C"
  Wood -> "PrimalItemResource_Wood_C"
  RawMeat -> "PrimalItemConsumable_RawMeat_C"
  RawPrimeMeat -> "PrimalItemConsumable_RawPrimeMeat_C"
  RawFishMeat -> "PrimalItemConsumable_RawMeat_Fish_C"
  RawPrimeFishMeat -> "PrimalItemConsumable_RawPrimeMeat_Fish_C"
  BasicKibble -> "PrimalItemConsumable_Kibble_Base_XSmall_C"
  SimpleKibble -> "PrimalItemConsumable_Kibble_Base_Small_C"
  RegularKibble -> "PrimalItemConsumable_Kibble_Base_Medium_C"
  SuperiorKibble -> "PrimalItemConsumable_Kibble_Base_Large_C"
  ExceptionalKibble -> "PrimalItemConsumable_Kibble_Base_XLarge_C"
  ExtraordinaryKibble -> "PrimalItemConsumable_Kibble_Base_Special_C"
  BlueColoring -> "PrimalItemDye_Blue_C"
  GreenColoring -> "PrimalItemDye_Green_C"
  RedColoring -> "PrimalItemDye_Red_C"
  GiantBeeHoney -> "PrimalItemConsumable_Honey_C"
  LargeCropPlot -> "PrimalItemStructure_CropPlot_Large_C"


  ElementalCrystal -> "PrimalItemConsumable_Berry_SF_MagicElemental_C"
  ApprenticeCrystal -> "PrimalItemConsumable_Berry_SF_CrystalApprentice_C"
  JourneyManCrystal -> "PrimalItemConsumable_Berry_SF_CrystalJourneyman_C"
  MastercraftCrystal -> "PrimalItemConsumable_Berry_SF_CrystalMastercraft_C"
  AscendantCrystal -> "PrimalItemConsumable_Berry_SF_CrystalAscendant_C"
  ElementalCrystalSeed -> "PrimalItemConsumable_Seed_SF_Elemental_C"
  ApprenticeCrystalSeed -> "PrimalItemConsumable_Seed_SF_CrystalApprentice_C"
  JourneyManCrystalSeed -> "PrimalItemConsumable_Seed_SF_CrystalJourneyman_C"
  MastercraftCrystalSeed -> "PrimalItemConsumable_Seed_SF_CrystalMastercraft_C"
  AscendantCrystalSeed -> "PrimalItemConsumable_Seed_SF_CrystalAscendant_C"
  PlantXConsumable -> "PrimalItemConsumable_Berry_SF_PlantSpeciesX_C"
  BioLankEssence -> "PrimalItemResource_SF_BioLankEssence_C"
  BioTekGigaSpawner -> "PrimalItemConsumable_SF_BioTekGigaSpawner_C"
  BioLankBossSpawner -> "PrimalItemConsumable_SF_TheBioLankBossSpawner_C"
  SuperSmallForge -> "PrimalItemStructure_SF_SuperSmallForge_C"
  ApprenticeCropPlot -> "PrimalItemStructure_BP_ApprenticeCropPlot_C"
  JourneyManCropPlot -> "PrimalItemStructure_BP_JourneymanCropPlot_C"
  MastercraftCropPlot -> "PrimalItemStructure_BP_MastercraftCropPlot_C"
  AscendantCropPlot -> "PrimalItemStructure_BP_AscendantCropPlot_C"
  TekCropPlot -> "PrimalItemStructure_BP_TekCropPlot_C"
  UltimateTekCropPlot -> "PrimalItemStructure_BP_UltimateTekPlot_C"
  RawMeatSeed -> "PrimalItemConsumable_Seed_SF_RawMeat_C"
  RawPrimeMeatSeed -> "PrimalItemConsumable_Seed_SF_RawPrimeMeat_C"
  RawFishMeatSeed -> "PrimalItemConsumable_Seed_SF_RawFishMeat_C"
  RawPrimeFishMeatSeed -> "PrimalItemConsumable_Seed_SF_RawPrimeFishMeat_C"
  SpoiledMeat -> "PrimalItemConsumable_SpoiledMeat_C"
  DyeSeed -> "PrimalItemConsumable_Seed_SF_Dye_C"
  FlintSeed -> "PrimalItemConsumable_Seed_SF_Flint_C"
  HideSeed -> "PrimalItemConsumable_Seed_SF_Hide_C"
  SpoiledMeatSeed -> "PrimalItemConsumable_Seed_SF_SpoiledMeat_C"
  ThatchSeed -> "PrimalItemConsumable_Seed_SF_Thatch_C"
  BioToxinSeed -> "PrimalItemConsumable_Seed_SF_BioToxin_C"
  BloodPackSeed -> "PrimalItemConsumable_Seed_SF_BloodPack_C"
  ChitinSeed -> "PrimalItemConsumable_Seed_SF_Chitin_C"
  FiberSeed -> "PrimalItemConsumable_Seed_SF_Fiber_C"
  HoneySeed -> "PrimalItemConsumable_Seed_SF_Honey_C"
  LeechBloodSeed -> "PrimalItemConsumable_Seed_SF_LeechBlood_C"
  OilSeed -> "PrimalItemConsumable_Seed_SF_Oil_C"
  PeltSeed -> "PrimalItemConsumable_Seed_SF_Pelt_C"
  RareFlowerSeed -> "PrimalItemConsumable_Seed_SF_RareFlower_C"
  RareMushroomSeed -> "PrimalItemConsumable_Seed_SF_RareMushroom_C"
  SandSeed -> "PrimalItemConsumable_Seed_SF_Sand_C"
  StoneSeed -> "PrimalItemConsumable_Seed_SF_Stone_C"
  WoodSeed -> "PrimalItemConsumable_Seed_SF_Wood_C"
  AnglerGelSeed -> "PrimalItemConsumable_Seed_SF_AnglerGel_C"
  CementingPasteSeed -> "PrimalItemConsumable_Seed_SF_CementPaste_C"
  CharcoalSeed -> "PrimalItemConsumable_Seed_SF_Charcoal_C"
  CrystalSeed -> "PrimalItemConsumable_Seed_SF_Crystal_C"
  GasolineSeed -> "PrimalItemConsumable_Seed_SF_Gasoline_C"
  MetalSeed -> "PrimalItemConsumable_Seed_SF_Metal_C"
  NarcoticSeed -> "PrimalItemConsumable_Seed_SF_Narcotic_C"
  ObsidianSeed -> "PrimalItemConsumable_Seed_SF_Obsidian_C"
  SaltSeed -> "PrimalItemConsumable_Seed_SF_Salt_C"
  SapSeed -> "PrimalItemConsumable_Seed_SF_Sap_C"
  SilicaPearlsSeed -> "PrimalItemConsumable_Seed_SF_SilicaPearl_C"
  SilkSeed -> "PrimalItemConsumable_Seed_SF_Silk_C"
  SparkPowderSeed -> "PrimalItemConsumable_Seed_SF_SparkPowder_C"
  StimulantSeed -> "PrimalItemConsumable_Seed_SF_Stimulant_C"
  AmmoniteBileSeed -> "PrimalItemConsumable_Seed_SF_Ammonite_C"
  BlackPearlSeed -> "PrimalItemConsumable_Seed_SF_BlackPearl_C"
  ElectronicsSeed -> "PrimalItemConsumable_Seed_SF_Electronics_C"
  GemSeed -> "PrimalItemConsumable_Seed_SF_Gems_C"
  GunpowderSeed -> "PrimalItemConsumable_Seed_SF_Gunpowder_C"
  KibbleSeed -> "PrimalItemConsumable_Seed_SF_Kibbles_C"
  MetalIngotSeed -> "PrimalItemConsumable_Seed_SF_MetalIngot_C"
  PolymerSeed -> "PrimalItemConsumable_Seed_SF_Polymer_C"
  ElementSeed -> "PrimalItemConsumable_Seed_SF_Element_C"
