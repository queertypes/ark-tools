module Level (generateLevelingConfig, generateLevelingTable) where

import Data.List (foldl')
import Data.Text (Text)
import Data.Text.Lazy.Builder.Int (decimal)
import Data.Text.Lazy.Builder (toLazyText)
import Data.Text.Lazy (toStrict)
import qualified Data.Text as T

generateLevelingConfig :: Int -> Int -> Double -> Double -> Int -> Text
generateLevelingConfig n u a b s =
  let xpTable = generateLevelingTable n u a b s
  in generateLevelingEntries xpTable

xpForLevel :: Int -> Int -> Double -> Double -> Int -> Int
xpForLevel n u a b s =
  let (n' :: Double) = fromIntegral n
      (s' :: Double) = fromIntegral s
   in if n < u
      then floor (a * (n'**3.0) + b * (n'**2.0) + 2.0*n' + s')
      else 1000000

generateLevelingTable :: Int -> Int -> Double -> Double -> Int -> [Int]
generateLevelingTable numLevels u a b s =
  scanl (\acc x -> xpForLevel x u a b s + acc) s [1..numLevels-1]
  -- foldl' (\acc x -> xpForLevel x 1.025 25 : acc) [] [1..numLevels]

generateLevelingEntries :: [Int] -> Text
generateLevelingEntries xpTable =
  "LevelExperienceRampOverrides=("
  <> generateTableEntries xpTable
  <> ")"

generateTableEntries :: [Int] -> Text
generateTableEntries xpTable =
  T.intercalate "," . reverse $
  foldl' (\acc (n,x) -> entry n x : acc) [] (zip [1..(length xpTable)] xpTable)
  where entry n x =
          "ExperiencePointsForLevel[" <> dec n <> "]=" <> dec x

dec :: Integral a => a -> Text
dec = toStrict . toLazyText . decimal
