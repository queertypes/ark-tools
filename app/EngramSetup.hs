module EngramSetup (overrides) where

import Data.Text (Text)
import qualified Data.Text as T

import qualified Engram.AccessoriesPlus as AP
import qualified Engram.AdditionalLights as AL
import qualified Engram.ArkGenomes as AG
import qualified Engram.ArkOmega as AO
import qualified Engram.AwesomeSpyglass as AS
import qualified Engram.AwesomeTeleporters as AT
import qualified Engram.BabyPremiumCare as BPC
import qualified Engram.Bridges as B
import qualified Engram.Chetus as CH
import qualified Engram.CombatTrainer as CT
import qualified Engram.DinoStorage as DS
import Engram.Generate
import qualified Engram.ImmersiveTaming as IT
import qualified Engram.IndustrialForges as IF
import qualified Engram.KeepInventory as KI
import qualified Engram.Kibbler as K
import qualified Engram.Ladys as LF
import qualified Engram.LevelUpCounter as LVL
import qualified Engram.MarniHair as MH
import qualified Engram.PandorasBox as PB
import qualified Engram.SaddleEmporium as SE
import qualified Engram.SeedFarm as SF
import qualified Engram.ShinyDinos as SD
import qualified Engram.SolarPanels as SP
import qualified Engram.SuperStructures as SS
import qualified Engram.UpgradeStation as US
import qualified Engram.Vanilla as V

superStructures :: [SS.Engram]
superStructures =
  [ SS.BeerBarrelPlus
  , SS.AirConditionerPlus
  , SS.AmmoLoaderSS
  , SS.AnimalTender
  , SS.AutoCrafter
  , SS.AutoTurret
  , SS.AutoTurretPlant
  , SS.AutoTurretTek
  , SS.AutoTurret_Ballista
  , SS.AutoTurret_Cannon
  , SS.AutoTurret_Flame
  , SS.AutoTurret_Heavy
  , SS.AutoTurret_Laser
  , SS.AutoTurret_Minigun
  , SS.AutoTurret_Rocket
  , SS.AutoTurret_Tranq
  , SS.BedPlus
  , SS.BeeHivePlus
  , SS.BlueprintMaker
  , SS.BookshelfPlus
  , SS.BunkBedPlus
  , SS.CampfirePlus
  , SS.Catwalk_Glass
  , SS.Catwalk_Metal
  , SS.Catwalk_Tek
  , SS.Catwalk_Wood
  , SS.Ceiling_Adobe
  , SS.Ceiling_Glass
  , SS.Ceiling_Greenhouse
  , SS.Ceiling_Metal
  , SS.Ceiling_Stone
  , SS.Ceiling_Tek
  , SS.Ceiling_TekElevator
  , SS.Ceiling_TekGlass
  , SS.Ceiling_Thatch
  , SS.Ceiling_Wood
  , SS.ChargeInjector
  , SS.ChargeStation
  , SS.ChemBenchPlus
  , SS.CloningChamberPlus
  , SS.CompostBinPlus
  , SS.Converter
  , SS.CookingPotPlus
  , SS.CropPlotPlus_Large
  , SS.CropPlotPlus_Medium
  , SS.CropPlotPlus_SeamlessSquare
  , SS.CropPlotPlus_SeamlessTriangle
  , SS.CropPlotPlus_Small
  , SS.CryoFridge_SS
  , SS.CrystalCracker
  , SS.DedicatedStorageSP
  , SS.DedicatedStorage_Intake
  , SS.DinoUplink
  , SS.Door_Adobe
  , SS.Door_Glass
  , SS.Door_Greenhouse
  , SS.Door_Metal
  , SS.Door_Stone
  , SS.Door_Tek
  , SS.Door_Thatch
  , SS.Door_Underwater
  , SS.Door_Wood
  , SS.Doorframe_Adobe
  , SS.Doorframe_Glass
  , SS.Doorframe_Greenhouse
  , SS.Doorframe_Metal
  , SS.Doorframe_Stone
  , SS.Doorframe_Tek
  , SS.Doorframe_Thatch
  , SS.Doorframe_Wood
  , SS.DoubleDoor_AdobeSP
  , SS.DoubleDoor_Glass
  , SS.DoubleDoor_GreenhouseSP
  , SS.DoubleDoor_MetalSP
  , SS.DoubleDoor_StoneSP
  , SS.DoubleDoor_TekSP
  , SS.DoubleDoor_WoodSP
  , SS.DoubleDoorframe_AdobeSP
  , SS.DoubleDoorframe_Glass
  , SS.DoubleDoorframe_GreenhouseSP
  , SS.DoubleDoorframe_MetalSP
  , SS.DoubleDoorframe_StoneSP
  , SS.DoubleDoorframe_TekSP
  , SS.DoubleDoorframe_WoodSP
  , SS.DroneTerminal
  , SS.DyeStation
  , SS.DynamicPillar_Adobe
  , SS.DynamicPillar_Metal
  , SS.DynamicPillar_Stone
  , SS.DynamicPillar_Tek
  , SS.DynamicPillar_Wood
  , SS.EggIncubatorSS
  , SS.ElectricalOutlet
  , SS.ElementCatalyzer
  , SS.ElevatorCallButton
  , SS.ElevatorPlatform_Large
  , SS.ElevatorPlatform_Medium
  , SS.ElevatorPlatform_Small
  , SS.ElevatorTrack_Metal
  , SS.FabricatorPlus
  , SS.Farmer
  , SS.FeedingTroughPlus
  , SS.FenceFoundation_Adobe
  , SS.FenceFoundation_Metal
  , SS.FenceFoundation_Stone
  , SS.FenceFoundation_Tek
  , SS.FenceFoundation_Wood
  , SS.FenceSupport_AdobeSP
  , SS.FenceSupport_MetalSP
  , SS.FenceSupport_StoneSP
  , SS.FenceSupport_TekSP
  , SS.FenceSupport_WoodSP
  , SS.FireplacePlus
  , SS.ForgePlus
  , SS.Foundation_Adobe
  , SS.Foundation_Glass
  , SS.Foundation_Metal
  , SS.Foundation_Ocean_Metal
  , SS.Foundation_Ocean_Wood
  , SS.Foundation_Stone
  , SS.Foundation_Tek
  , SS.Foundation_Thatch
  , SS.Foundation_Wood
  , SS.FridgePlus
  , SS.FridgeTek
  , SS.GachaGavager
  , SS.Gardener
  , SS.GasCollectorTek
  , SS.Gate_Adobe
  , SS.Gate_Dynamic
  , SS.Gate_Glass
  , SS.Gate_Metal
  , SS.Gate_Stone
  , SS.Gate_Tek
  , SS.Gate_Wood
  , SS.Gateway_Adobe
  , SS.Gateway_Dynamic
  , SS.Gateway_Metal
  , SS.Gateway_Stone
  , SS.Gateway_Tek
  , SS.Gateway_Wood
  , SS.Generator
  , SS.GeneratorTek
  , SS.GenomicsChamber
  , SS.GrinderPlus
  , SS.Hatchery
  , SS.Hatchframe_Adobe
  , SS.Hatchframe_Glass
  , SS.Hatchframe_Metal
  , SS.Hatchframe_Stone
  , SS.Hatchframe_Tek
  , SS.Hatchframe_Wood
  , SS.HitchingPost
  , SS.HitchingPost_Tek
  , SS.HoverSkiffSS
  , SS.Incinerator
  , SS.IndustrialCooker
  , SS.IndustrialForgePlus
  , SS.IndustrialGrillPlus
  , SS.InternalPipe_Pillar
  , SS.InternalPipe_Square
  , SS.InternalPipe_Triangle
  , SS.InternalPipe_Wall
  , SS.InternalWire_Pillar
  , SS.InternalWire_Square
  , SS.InternalWire_Triangle
  , SS.InternalWire_Wall
  , SS.InventoryAssistant
  , SS.ItemCollector
  , SS.ItemTranslocator
  , SS.JumpPadSS
  , SS.Ladder_Adobe
  , SS.Ladder_Metal
  , SS.Ladder_Rope
  , SS.Ladder_Tek
  , SS.Ladder_Wood
  , SS.LargeGate_Adobe
  , SS.LargeGate_Glass
  , SS.LargeGate_Metal
  , SS.LargeGate_Stone
  , SS.LargeGate_Tek
  , SS.LargeGateway_Adobe
  , SS.LargeGateway_Metal
  , SS.LargeGateway_Stone
  , SS.LargeGateway_Tek
  , SS.LargeHatchframeSloped_Adobe
  , SS.LargeHatchframeSloped_Metal
  , SS.LargeHatchframeSloped_Stone
  , SS.LargeHatchframeSloped_Tek
  , SS.LargeHatchframe_Adobe
  , SS.LargeHatchframe_Metal
  , SS.LargeHatchframe_Stone
  , SS.LargeHatchframe_Tek
  , SS.LargePillar_Adobe
  , SS.LargePillar_Metal
  , SS.LargePillar_Stone
  , SS.LargePillar_Tek
  , SS.LargePillar_Wood
  , SS.LargeTrapdoor_Adobe
  , SS.LargeTrapdoor_Glass
  , SS.LargeTrapdoor_Metal
  , SS.LargeTrapdoor_Stone
  , SS.LargeTrapdoor_Tek
  , SS.LargeWall_AdobeSP
  , SS.LargeWall_Forcefield
  , SS.LargeWall_Glass
  , SS.LargeWall_MetalSP
  , SS.LargeWall_StoneSP
  , SS.LargeWall_TekSP
  , SS.LargeWall_WoodSP
  , SS.LeashTek
  , SS.LoadoutDummy
  , SS.LoadoutDummySS
  , SS.Mannequin
  , SS.MediumPillar_Adobe
  , SS.MediumPillar_Metal
  , SS.MediumPillar_Stone
  , SS.MediumPillar_Tek
  , SS.MediumPillar_Wood
  , SS.MortarPestlePlus
  , SS.MultiLamp
  , SS.Nanny
  , SS.Noglin_Nullifier
  , SS.PersonalTeleporter
  , SS.PipeFlex_MetalSP
  , SS.PipeFlex_StoneSP
  , SS.PipeHorizontal_Metal
  , SS.PipeHorizontal_Stone
  , SS.PipeIncline_Metal
  , SS.PipeIncline_Stone
  , SS.PipeIntake_Metal
  , SS.PipeIntake_Stone
  , SS.PipeIntersection_Metal
  , SS.PipeIntersection_Stone
  , SS.PipeVertical_Metal
  , SS.PipeVertical_Stone
  , SS.PlanetShield
  , SS.PreservingBinPlus
  , SS.PressurePlateSS
  , SS.Railing_Adobe
  , SS.Railing_Glass
  , SS.Railing_Metal
  , SS.Railing_Stone
  , SS.Railing_Tek
  , SS.Railing_Wood
  , SS.Ramp_AdobeSP
  , SS.Ramp_Glass
  , SS.Ramp_MetalSP
  , SS.Ramp_StoneSP
  , SS.Ramp_TekSP
  , SS.Ramp_WoodSP
  , SS.RepairStation
  , SS.ReplicatorPlus
  , SS.Roof_Adobe
  , SS.Roof_Glass
  , SS.Roof_Greenhouse
  , SS.Roof_Metal
  , SS.Roof_Stone
  , SS.Roof_Tek
  , SS.Roof_TekGlass
  , SS.Roof_Thatch
  , SS.Roof_Wood
  , SS.SPlusCraftingStation
  , SS.SPlusTekLight
  , SS.SheepHerder
  , SS.ShieldPost
  , SS.SlopedWall_Adobe_Left
  , SS.SlopedWall_Adobe_Right
  , SS.SlopedWall_Glass_Left
  , SS.SlopedWall_Glass_Right
  , SS.SlopedWall_Greenhouse_Left
  , SS.SlopedWall_Greenhouse_Right
  , SS.SlopedWall_Metal_Left
  , SS.SlopedWall_Metal_Right
  , SS.SlopedWall_Stone_Left
  , SS.SlopedWall_Stone_Right
  , SS.SlopedWall_Tek_Left
  , SS.SlopedWall_Tek_Right
  , SS.SlopedWall_Thatch_Left
  , SS.SlopedWall_Thatch_Right
  , SS.SlopedWall_Wood_Left
  , SS.SlopedWall_Wood_Right
  , SS.SmallPillar_Adobe
  , SS.SmallPillar_Metal
  , SS.SmallPillar_Stone
  , SS.SmallPillar_Tek
  , SS.SmallPillar_Wood
  , SS.SmithyPlus
  , SS.SpikeWall_Metal
  , SS.SpikeWall_Wood
  , SS.SprayPainterPlus
  , SS.Staircase_Adobe
  , SS.Staircase_Glass
  , SS.Staircase_Metal
  , SS.Staircase_Stone
  , SS.Staircase_Tek
  , SS.Staircase_Wood
  , SS.StandingTorchPlus
  , SS.StorageLarge
  , SS.StorageMetal
  , SS.StorageSmall
  , SS.TaxidermyPlus_Large
  , SS.TaxidermyPlus_Medium
  , SS.TaxidermyPlus_Small
  , SS.TekBedPlus
  , SS.TekBridgePlus
  , SS.TekChemBench
  , SS.TekCookingPot
  , SS.TekCropPlotSS
  , SS.TekCropPlotTriangleSS
  , SS.TekForcefield
  , SS.TekForge
  , SS.TekSensor
  , SS.TekStorage
  , SS.TekStove
  , SS.TekTroughPlus
  , SS.TeleporterPlus
  , SS.TransferTool
  , SS.TransmitterPlus
  , SS.Trapdoor_Adobe
  , SS.Trapdoor_Glass
  , SS.Trapdoor_Metal
  , SS.Trapdoor_Moonpool
  , SS.Trapdoor_Stone
  , SS.Trapdoor_Tek
  , SS.Trapdoor_Wood
  , SS.TreeSapTapPlus
  , SS.TriCeiling_AdobeSP
  , SS.TriCeiling_Glass
  , SS.TriCeiling_GreenhouseSP
  , SS.TriCeiling_MetalSP
  , SS.TriCeiling_StoneSP
  , SS.TriCeiling_TekElevator
  , SS.TriCeiling_TekSP
  , SS.TriCeiling_WoodSP
  , SS.TriFoundation_AdobeSP
  , SS.TriFoundation_Glass
  , SS.TriFoundation_MetalSP
  , SS.TriFoundation_StoneSP
  , SS.TriFoundation_TekSP
  , SS.TriFoundation_WoodSP
  , SS.TriRoof_AdobeSP
  , SS.TriRoof_Glass
  , SS.TriRoof_GreenhouseSP
  , SS.TriRoof_MetalSP
  , SS.TriRoof_StoneSP
  , SS.TriRoof_TekSP
  , SS.TriRoof_WoodSP
  , SS.TrophyBaseSS
  , SS.TrophyWallSS
  , SS.Underwater_Cube
  , SS.Underwater_Cube_Sloped
  , SS.Underwater_Moonpool
  , SS.VaultPlus
  , SS.VesselPlus
  , SS.Vivarium
  , SS.WallTorchPlus
  , SS.Wall_Adobe
  , SS.Wall_Forcefield
  , SS.Wall_Glass
  , SS.Wall_Greenhouse
  , SS.Wall_Metal
  , SS.Wall_Stone
  , SS.Wall_Tek
  , SS.Wall_TekGlass
  , SS.Wall_Thatch
  , SS.Wall_Wood
  , SS.WaterTank_Metal
  , SS.WaterTank_Stone
  , SS.WaterTap_Metal
  , SS.WaterTap_Stone
  , SS.WindTurbinePlus
  , SS.WindowWall_Adobe
  , SS.WindowWall_Metal
  , SS.WindowWall_Stone
  , SS.WindowWall_Tek
  , SS.WindowWall_Wood
  , SS.Window_Adobe
  , SS.Window_Glass
  , SS.Window_Greenhouse
  , SS.Window_Metal
  , SS.Window_Stone
  , SS.Window_Tek
  , SS.Window_Wood
  , SS.Wire_FlexSP
  , SS.Wire_Horizontal
  , SS.Wire_Incline
  , SS.Wire_Intersection
  , SS.Wire_Vertical
  , SS.XLHatchframeSloped_Adobe
  , SS.XLHatchframeSloped_Metal
  , SS.XLHatchframeSloped_Stone
  , SS.XLHatchframeSloped_Tek
  , SS.XLHatchframe_Adobe
  , SS.XLHatchframe_Metal
  , SS.XLHatchframe_Stone
  , SS.XLHatchframe_Tek
  , SS.XLTrapdoor_Adobe
  , SS.XLTrapdoor_Glass
  , SS.XLTrapdoor_Metal
  , SS.XLTrapdoor_Stone
  , SS.XLTrapdoor_Tek
  , SS.XLWall_Adobe
  , SS.XLWall_Forcefield
  , SS.XLWall_Glass
  , SS.XLWall_Metal
  , SS.XLWall_Stone
  , SS.XLWall_Tek
  , SS.XLWall_Wood
  ]

arkOmega :: [AO.Engram]
arkOmega =
  [ AO.SeagullNecklace_ArkOmega
  , AO.PegoNecklace_ArkOmega
  , AO.Thief_ArkOmega
  , AO.ReflectShield
  , AO.PygmyArmor
  , AO.Decoy
  , AO.EggCracker
  , AO.GodEssence
  , AO.OmegaGlider
  , AO.InfinityStone
  , AO.MekSummoner
  , AO.AntiGravityPotion
  , AO.CollectivePotion
  , AO.CraftingPotion
  , AO.EleResPotion
  , AO.EndlessFortitudePotion
  , AO.FortitudePotion
  , AO.GravityPotion
  , AO.AlphaPotion
  , AO.BasicPotion
  , AO.BetaPotion
  , AO.GodPotion
  , AO.OmegaPotion
  , AO.PrimePotion
  , AO.UltimatePotion
  , AO.MagicPotion
  , AO.NightvisionPotion
  , AO.RampagePotion
  , AO.SirenPotion
  , AO.EndlessSpiritualPotion
  , AO.SpiritualPotion
  , AO.AlphaStamina
  , AO.BasicStamina
  , AO.BetaStamina
  , AO.GodStamina
  , AO.OmegaStamina
  , AO.PrimeStamina
  , AO.UltimateStamina
  , AO.StoneskinPotion
  , AO.SuicidePotion
  , AO.TamingPotion
  , AO.WakePotion
  , AO.OmegaRaft
  , AO.AlphaSedatives_ArkOmega
  , AO.AncientSedatives_ArkOmega
  , AO.BetaSedatives_ArkOmega
  , AO.GodlikeSedatives_ArkOmega
  , AO.OmegaSedatives_ArkOmega
  , AO.PrimeSedatives_ArkOmega
  , AO.Sedatives_ArkOmega
  , AO.UltimateSedatives_ArkOmega
  , AO.UnstableSteak
  , AO.UnstableWater
  , AO.BossBeacon
  , AO.ChromaticCannon
  , AO.Craptivator
  , AO.CrossMating
  , AO.CursedStatue
  , AO.DimensionalDumpster
  , AO.DimensionalGrinder
  , AO.DimensionalBox
  , AO.RetrieveTerminal
  , AO.DodoCage
  , AO.DPSDummy
  , AO.EggCollector
  , AO.GodEgg
  , AO.GoldenChest
  , AO.HadesStandingTorch
  , AO.ItemFrag
  , AO.OmegaAltar
  , AO.OmegaBeacon
  , AO.OmegaWorkBench
  , AO.QuantumCompactor
  , AO.RedDwarf
  , AO.RespawnPoint
  , AO.ScrollStorage
  , AO.FruitSeeder
  , AO.SetMannequin
  , AO.SoulCompressor
  , AO.SoulFurnace
  , AO.SoulGrinder
  , AO.SoulMagnet
  , AO.UniqueTotem
  , AO.OmegaUpgradeBench
  , AO.KibbleMachine
  , AO.VoidVacuum
  , AO.Boomstick
  , AO.ClonerStaff
  , AO.CosmicGrenade
  , AO.DischargeBoomerang
  , AO.EtherealSpear
  , AO.Evis
  , AO.Flashbang
  , AO.Fortune
  , AO.GatheringTool
  , AO.GetawayOrb
  , AO.Gravitron
  , AO.HadesTorch
  , AO.AlphaHealingDart
  , AO.BetaHealingDart
  , AO.HealingDart
  , AO.OmegaHealingDart
  , AO.PrimeHealingDart
  , AO.UltimateHealingDart
  , AO.Heartsong
  , AO.Icicle
  , AO.Inversion
  , AO.KnockoutClub
  , AO.Laser
  , AO.Lurkers
  , AO.WeaponMalice
  , AO.CompoundAlphaKnockoutArrow
  , AO.CompoundAncientKnockoutArrow
  , AO.CompoundBetaKnockoutArrow
  , AO.CompoundGodlikeKnockoutArrow
  , AO.CompoundKnockoutArrow
  , AO.CompoundOmegaKnockoutArrow
  , AO.CompoundPrimeKnockoutArrow
  , AO.CompoundUltimateKnockoutArrow
  , AO.WebGun
  , AO.MultiTool
  , AO.Outburst
  , AO.Phantom
  , AO.Plaguefork
  , AO.Ripper
  , AO.Risk
  , AO.Shatter
  , AO.VersatileSlingshot
  , AO.StoneSickle
  , AO.Stormbringer
  , AO.StormGrenade
  , AO.Thunderbolt
  , AO.AlphaArrowKnockout
  , AO.AncientArrowKnockout
  , AO.ArrowKnockout
  , AO.BetaArrowKnockout
  , AO.GodlikeArrowKnockout
  , AO.OmegaArrowKnockout
  , AO.PrimeArrowKnockout
  , AO.UltimateArrowKnockout
  , AO.Translocator
  , AO.UndeadSword
  , AO.Eggsecutioner
  , AO.VacuumGrenade
  , AO.VoidGrenade
  , AO.Wabbajack
  , AO.WakeDart
  , AO.WarpBow
  , AO.WyvernBreath
  ]

arkGenomes :: [AG.Engram]
arkGenomes =
  [ AG.ACModule_01_Enhanced
  , AG.ACModule_02_Elite
  , AG.ACModule_03_Wraith
  , AG.ACModule_04_01_FireStorm
  , AG.ACModule_04_02_SpiritPlague
  , AG.ACModule_04_03_Glacier
  , AG.ACModule_05_ShadowFall
  , AG.ACModule_06_Ethereal
  , AG.ACModule_07_NightVoid
  , AG.ACModule_08_Transcendent
  , AG.AVA
  , AG.FCG
  , AG.CRS
  , AG.CCS
  , AG.DinoGTE
  , AG.DinoGSS
  , AG.DinoGES
  , AG.DC
  , AG.DinoGEMS
  , AG.SH
  , AG.SerumExtractor
  , AG.DinoTSC
  , AG.GKM
  , AG.CorruptNSC
  , AG.DinoGSC
  , AG.DSC
  , AG.DinoESP
  , AG.CorruptCryopod
  , AG.ArrowTranq_Goji
  , AG.HealthDart
  , AG.OxygenDart
  , AG.SlowDart
  , AG.LingenDart
  , AG.TranqDart_Goji
  , AG.GenomeExtractor
  ]

accessoriesPlus :: [AP.Engram]
accessoriesPlus =
  [ AP.Mochila_Avanzado
  , AP.Mochila_Avanzado_Glider_Lite
  , AP.Mochila_Avanzado_Glider_Modern_Lite
  , AP.Portable_BugRepellant
  , AP.Portable_ChitinPaste
  , AP.Portable_Clay
  , AP.Portable_Gunpowder
  , AP.Portable_LAntidote
  , AP.Portable_Narcotic
  , AP.Portable_PreservingSalt
  , AP.Portable_Propellant
  , AP.Portable_Sparkpowder
  , AP.Portable_Stimulant
  , AP.Mochila_Avanzado_Modern
  , AP.Control_TEK_Backpack
  , AP.Mochila_Basica
  , AP.Mochila_Intermedio
  , AP.Advanced_Quiver
  , AP.Basic_Quiver
  , AP.Estandarte
  ]

additionalLights :: [AL.Engram]
additionalLights =
  [ AL.AL_CandleLights
  , AL.AL_ColoredLanterns
  , AL.AL_CraftingStation
  , AL.AL_DiscoBall
  , AL.AL_MetallicBulbLights
  , AL.AL_WoodenBulbLights
  , AL.AL_FireLights
  , AL.AL_RingOfFire
  , AL.AL_FloodLight
  , AL.AL_HandheldCampingLamp
  , AL.AL_HandheldCandelabra
  , AL.AL_HandheldHeartLight
  , AL.AL_HandheldLantern
  , AL.AL_HandheldLantern02
  , AL.AL_HandheldMoonLight
  , AL.AL_HandheldSearchLight
  , AL.AL_HandheldSkullCandleStick
  , AL.AL_SkyLantern
  , AL.AL_HandheldStarLight
  , AL.AL_LightStick_White
  , AL.AL_HandheldTorch
  , AL.AL_Steps
  , AL.AL_GlowBugs
  , AL.AL_ColorChanger
  , AL.AL_PositionController
  , AL.AL_RemoteController
  , AL.AL_SwitchConnector
  , AL.AL_MetalLightSwitch
  , AL.AL_WoodenLightSwitch
  , AL.AL_LightSequencer
  , AL.AL_EmergencyHelmetsClassic
  , AL.AL_Spotlight
  , AL.AL_StringLights
  ]

awesomeSpyglass :: [AS.Engram]
awesomeSpyglass =
  [ AS.AwesomeSpyglass
  ]

awesomeTeleporters :: [AT.Engram]
awesomeTeleporters =
  [ AT.AwesomeTeleporters_Remote
  , AT.AwesomeTeleporters_Teleporter
  , AT.AwesomeTeleporters_DinoTracker
  ]

babyPremiumCare :: [BPC.Engram]
babyPremiumCare =
  [ BPC.CareGiver
  , BPC.CareGiver_SE
  , BPC.CareGiver_Wyvern
  ]

bridges :: [B.Engram]
bridges =
  [ B.JD_Bridge01
  , B.JD_Bridge02
  , B.JD_Bridge_Metal_01
  , B.JD_Bridge_Metal_02
  , B.JD_Bridge_Stone_01
  , B.JD_Bridge_Stone_02
  , B.JD_Bridge_Wood_03
  ]

chetusTools :: [CH.Engram]
chetusTools =
  [ CH.ChetuBola
  , CH.BonusHatchet_TheChetu
  , CH.BonusPick_TheChetu
  , CH.ChetuDinoPickUp
  , CH.BonusSickle
  , CH.ChetuTranqBola
  , CH.Ascender_Chetu
  , CH.SpoilandCrafter_TheChetu
  ]

combatTrainer :: [CT.Engram]
combatTrainer =
  [ CT.RankingPistol
  ]

dinoStorage :: [DS.Engram]
dinoStorage =
  [ DS.SoulGun_DS
  , DS.SoulFinder_DS
  , DS.SoulTraps_DS
  , DS.SoulTerminal_DS
  ]

immersiveTaming :: [IT.Engram]
immersiveTaming =
  [ IT.BaitBalloon
  , IT.HumanScent
  , IT.BaitCooler
  , IT.BaitDispenser
  , IT.BaitStation_Food
  , IT.BaitStation_Kibble
  , IT.BaitStation_Trophy
  , IT.TamingJournal
  , IT.VialStation
  ]

industrialForges :: [IF.Engram]
industrialForges =
  [ IF.IndustrialForge
  , IF.IndustrialForge65
  , IF.IndustrialForge85
  ]

keepInventory :: [KI.Engram]
keepInventory =
  [ KI.FastTravelBed_DIK
  ]

kibbler :: [K.Engram]
kibbler =
  [ K.Kibbler_Balanced
  , K.Kibbler_Easy
  ]

ladysFarming :: [LF.Engram]
ladysFarming =
  [ LF.KitchenSupplies_LWCM
  , LF.PottingShed_LWCM
  , LF.CookingStove_LWCM
  , LF.SmallCookingPot_LWCM
  ]

levelupCounter :: [LVL.Engram]
levelupCounter =
  [ LVL.LevelUp_Counter_AE
  ]

marniHair :: [MH.Engram]
marniHair =
  [ MH.MMHairstylesTable
  ]

pandorasBox :: [PB.Engram]
pandorasBox =
  [ PB.MagicBox
  , PB.PandorasBox
  ]

saddleEmporium :: [SE.Engram]
saddleEmporium =
  [ SE.SaddleSkins_Deer_NivSadEmp
  , SE.Saddle_Deer_NivSadEmp
  , SE.HeavyArmorsSkins_Equus_NivSadEmp
  , SE.HeavyArmors_Equus_NivSadEmp
  , SE.LightArmorskins_Equus_NivSadEmp
  , SE.LightArmors_Equus_NivSadEmp
  , SE.RopeSkins_Equus_NivSadEmp
  , SE.Rope_Equus_NivSadEmp
  , SE.SaddleSkins_Equus_NivSadEmp
  , SE.Saddle_Equus_NivSadEmp
  , SE.SaddleSkins_Paracer_NivSadEmp
  , SE.Saddle_ParacerPlat_NivSadEmp
  , SE.Saddle_ParacerPlat_Wood_NivSadEmp
  , SE.Saddle_Paracer_NivSadEmp
  , SE.SaddleSkins_Para_NivSadEmp
  , SE.Saddle_Para_NivSadEmp
  , SE.SaddleSkins_Ptero_NivSadEmp
  , SE.Saddle_Ptero_NivSadEmp
  , SE.SaddleSkins_Raptor_NivSadEmp
  , SE.Saddle_Raptor_NivSadEmp
  , SE.SaddleSkins_Spino_NivSadEmp
  , SE.Saddle_Spino_NivSadEmp
  , SE.SaddleSkins_Trike_NivSadEmp
  , SE.Saddle_Trike_NivSadEmp
  , SE.Workbench_NivSadEmp
  ]

seedFarm :: [SF.Engram]
seedFarm =
  [ SF.SF_BioTekGiga
  , SF.SF_TheBioLankBoss
  , SF.PotionPoopMaker
  , SF.SF_Seed_Elemental
  , SF.BP_ApprenticeCropPlot
  , SF.BP_AscendantCropPlot
  , SF.BP_JourneymanCropPlot
  , SF.BP_MastercraftCropPlot
  , SF.BP_TekCropPlot
  , SF.UltimateTekPlot
  , SF.MagicCompostBin
  , SF.MagicReverseCrafter
  , SF.SF_Ceiling_Greenhouse
  , SF.SF_Ceiling_Stone
  , SF.SF_Foundation
  , SF.SF_SpoilerBin
  , SF.SF_SuperSmallForge
  , SF.SeedFarm_Table
  ]

shinyDinos :: [SD.Engram]
shinyDinos =
  [ SD.ShinyProd
  , SD.ShinyTracker
  ]

solarPanels :: [SP.Engram]
solarPanels =
  [ SP.RedDwarf_SolarPanel
  ]

upgradeStation :: [US.Engram]
upgradeStation =
  [ US.AugmentStation
  , US.AugmentStation_Base
  , US.AugmentStation_Metal
  , US.BlueprintStation
  , US.BlueprintStation_Base
  , US.BlueprintStation_Metal
  , US.UpgradeStationManual
  , US.UpgradeStation
  , US.UpgradeStation_Base
  , US.UpgradeStation_Metal
  ]

vanilla :: [V.Engram]
vanilla =
  [ V.BasiliskSaddle
  , V.AggroTranqDart
  , V.AlarmTrap
  , V.AmmoBox
  , V.AnvilBench
  , V.ArrowStone
  , V.ArrowTranq
  , V.BloodExtractor
  , V.Bookshelf
  , V.Bow
  , V.BugRepel
  , V.Camera
  , V.Canoe
  , V.Canteen
  , V.ChainSaw
  , V.ChargeBattery
  , V.ChargeLantern
  , V.CherufeSaddle
  , V.ChitinBoots
  , V.ChitinGloves
  , V.ChitinHelmet
  , V.ChitinPants
  , V.ChitinPaste
  , V.ChitinShirt
  , V.Clay
  , V.ClimbingPick
  , V.ClothBoots
  , V.ClothGloves
  , V.ClothHelmet
  , V.ClothPants
  , V.ClothShirt
  , V.ClusterGrenade
  , V.Compass
  , V.CompoundArrow
  , V.CompoundBow
  , V.CropPlot_Large
  , V.Crossbow
  , V.CruiseMissile
  , V.CureLow
  , V.DedicatedStorage
  , V.DesertClothBoots
  , V.DesertClothGloves
  , V.DesertClothGooglesHelmet
  , V.DesertClothPants
  , V.DesertClothShirt
  , V.DinoLeash
  , V.Electronics
  , V.Fishbasket
  , V.FishingNet
  , V.FishingRod
  , V.Flag
  , V.FlagSingle
  , V.FlameArrow
  , V.Flashlight
  , V.FurBoots
  , V.FurGloves
  , V.FurHelmet
  , V.FurPants
  , V.FurShirt
  , V.GPS
  , V.GachaSaddle
  , V.GasBagsSaddle
  , V.GasCollector
  , V.GasMask
  , V.GhillieBoots
  , V.GhillieGloves
  , V.GhillieHelmet
  , V.GhilliePants
  , V.GhillieShirt
  , V.GiantTurtleSaddle
  , V.Glider
  , V.GlowStick
  , V.GrapplingHook
  , V.Gravestone
  , V.Grinder
  , V.Gunpowder
  , V.Handcuffs
  , V.HazardSuit_Boots
  , V.HazardSuit_Chest
  , V.HazardSuit_Gloves
  , V.HazardSuit_Helmet
  , V.HazardSuit_Pants
  , V.HideBoots
  , V.HideGloves
  , V.HideHelmet
  , V.HidePants
  , V.HideShirt
  , V.HideSleepingBag
  , V.HoverSkiff
  , V.IceJumperSaddle
  , V.KarkinoSaddle
  , V.Keypad
  , V.Lamppost
  , V.LamppostOmni
  , V.MagnifyingGlass
  , V.MekBackpack_Shield
  , V.MekSpawner
  , V.MetalBoots
  , V.MetalGloves
  , V.MetalHatchet
  , V.MetalHelmet
  , V.MetalPants
  , V.MetalShield
  , V.MetalShirt
  , V.Milkglider_Saddle
  , V.MinersHelmet
  , V.MiningDrill
  , V.MiracleGro
  , V.Mirror
  , V.Motorboat
  , V.Narcotic
  , V.NightVisionGoggles
  , V.NotePaper
  , V.OilJar
  , V.OilPump
  , V.OwlSaddle
  , V.Paintbrush
  , V.PaintingCanvas
  , V.Parachute
  , V.Polymer
  , V.PortableRopeLadder
  , V.PowerOutlet
  , V.PreservingSalt
  , V.PressurePlate
  , V.Propellant
  , V.Radio
  , V.RavagerSaddle
  , V.RefinedElement
  , V.RefinedShards
  , V.RefinedTranqDart
  , V.RiotBoots
  , V.RiotGloves
  , V.RiotHelmet
  , V.RiotPants
  , V.RiotShield
  , V.RiotShirt
  , V.RocketAmmo
  , V.RocketHommingAmmo
  , V.RollratSaddle
  , V.RopeLadder
  , V.Saddle_Allo
  , V.Saddle_Amarga
  , V.Saddle_Andrewsarchus
  , V.Saddle_Ankylo
  , V.Saddle_Argentavis
  , V.Saddle_Arthro
  , V.Saddle_Baryonyx
  , V.Saddle_Basilosaurus
  , V.Saddle_Beaver
  , V.Saddle_Camelsaurus
  , V.Saddle_Carcha
  , V.Saddle_Carno
  , V.Saddle_Chalico
  , V.Saddle_Daeodon
  , V.Saddle_Deinonychus
  , V.Saddle_Desmodus
  , V.Saddle_Diplodocus
  , V.Saddle_Direbear
  , V.Saddle_Doed
  , V.Saddle_Dolphin
  , V.Saddle_Dunkle
  , V.Saddle_Equus
  , V.Saddle_Galli
  , V.Saddle_Gigant
  , V.Saddle_Hyaenodon
  , V.Saddle_Iguanodon
  , V.Saddle_Kaprosuchus
  , V.Saddle_Mammoth
  , V.Saddle_Manta
  , V.Saddle_Mantis
  , V.Saddle_Megalania
  , V.Saddle_Megalodon
  , V.Saddle_Megalosaurus
  , V.Saddle_Megatherium
  , V.Saddle_Mosa
  , V.Saddle_Mosa_Platform
  , V.Saddle_Moth
  , V.Saddle_Pachy
  , V.Saddle_PachyRhino
  , V.Saddle_Para
  , V.Saddle_Paracer
  , V.Saddle_Paracer_Platform
  , V.Saddle_Pela
  , V.Saddle_Phiomia
  , V.Saddle_Plesia
  , V.Saddle_Plesio_Platform
  , V.Saddle_Procop
  , V.Saddle_Ptero
  , V.Saddle_Quetz
  , V.Saddle_Quetz_Platform
  , V.Saddle_Raptor
  , V.Saddle_Rex
  , V.Saddle_Rhino
  , V.Saddle_RockGolem
  , V.Saddle_Saber
  , V.Saddle_Sarco
  , V.Saddle_Sauro
  , V.Saddle_Sauro_Platform
  , V.Saddle_Scorpion
  , V.Saddle_Spider
  , V.Saddle_SpineyLizard
  , V.Saddle_Spino
  , V.Saddle_Stag
  , V.Saddle_Stego
  , V.Saddle_Tapejara
  , V.Saddle_TerrorBird
  , V.Saddle_Therizino
  , V.Saddle_Thylaco
  , V.Saddle_Titano_Platform
  , V.Saddle_Toad
  , V.Saddle_Trike
  , V.Saddle_Tropeognathus
  , V.Saddle_Turtle
  , V.Saddle_Tuso
  , V.Saddle_Yuty
  , V.Scissors
  , V.ScubaBoots_Flippers
  , V.ScubaHelmet_Goggles
  , V.ScubaPants
  , V.ScubaShirt_SuitWithTank
  , V.ShadowDrakeSaddle
  , V.ShagRug
  , V.Slingshot
  , V.SpaceDolphin_Saddle
  , V.Sparkpowder
  , V.Spear
  , V.SpindlesSaddle
  , V.SprayPainter
  , V.StandingTorch
  , V.Stimulant
  , V.StoneClub
  , V.StoneHatchet
  , V.StonePick
  , V.SubstrateAbsorbent
  , V.Sword
  , V.TekATV
  , V.TekAlarm
  , V.TekBed
  , V.TekBoots
  , V.TekBow
  , V.TekBridge
  , V.TekCanteen
  , V.TekClaws
  , V.TekExosuit
  , V.TekGenerator
  , V.TekGloves
  , V.TekHelmet
  , V.TekHoverSail
  , V.TekLight
  , V.TekMegalodonSaddle
  , V.TekMosaSaddle
  , V.TekPants
  , V.TekPistol
  , V.TekRexSaddle
  , V.TekRifle
  , V.TekRockDrakeSaddle
  , V.TekSecuirtyConsole
  , V.TekShield
  , V.TekShieldArmor
  , V.TekShirt
  , V.TekSniper
  , V.TekSpaceWhaleSaddle
  , V.TekSword
  , V.TekTapejaraSaddle
  , V.Tent
  , V.Toilet
  , V.Torch
  , V.TranqDart
  , V.TranqSpearBolt
  , V.TransGPS
  , V.TransGPSAmmo
  , V.TreePlatformMetal
  , V.TreePlatformWood
  , V.TreeSapTap
  , V.Vessel
  , V.WallTorch
  , V.WarMap
  , V.Wardrums
  , V.WaterJar
  , V.WaterTank
  , V.WaterTankMetal
  , V.WaterWell
  , V.Waterskin
  , V.WeaponC4
  , V.WeaponWhip
  , V.WindTurbine
  , V.WoodShield
  , V.ZiplineAmmo
  , V.ZiplineMotor
  ]

overrides :: Text
overrides = T.intercalate "\n" $
  fmap (\e -> entryL e 1 0) superStructures
  <> fmap (\e -> entryL e 2 0) accessoriesPlus
  <> fmap (\e -> entryL e 3 0) additionalLights
  <> fmap (\e -> entryL e 10 0) arkGenomes
  <> fmap (\e -> entryL e 5 0) arkOmega
  <> fmap (\e -> entryL e 2 0) awesomeSpyglass
  <> fmap (\e -> entryL e 6 0) awesomeTeleporters
  <> fmap (\e -> entryL e 5 0) babyPremiumCare
  <> fmap (\e -> entryL e 4 0) bridges
  <> fmap (\e -> entryL e 5 0) chetusTools
  <> fmap (\e -> entryL e 1 0) combatTrainer
  <> fmap (\e -> entryL e 3 0) dinoStorage
  <> fmap (\e -> entryL e 1 0) immersiveTaming
  <> fmap (\e -> entryL e 11 0) industrialForges
  <> fmap (\e -> entryL e 1 0) keepInventory
  <> fmap (\e -> entryL e 12 0) kibbler
  <> fmap (\e -> entryL e 6 0) ladysFarming
  <> fmap (\e -> entryL e 1 0) levelupCounter
  <> fmap (\e -> entryL e 1 0) marniHair
  <> fmap (\e -> entryL e 13 0) pandorasBox
  <> fmap (\e -> entryL e 6 0) saddleEmporium
  <> fmap (\e -> entryL e 5 0) seedFarm
  <> fmap (\e -> entryL e 3 0) shinyDinos
  <> fmap (\e -> entryL e 14 0) solarPanels
  <> fmap (\e -> entryL e 6 0) upgradeStation
  <> fmap (\e -> entryL e 1 0) vanilla
  <> fmap (`unlock` 2) accessoriesPlus
  <> fmap (`unlock` 3) additionalLights
  <> fmap (`unlock` 10) arkGenomes
  <> fmap (`unlock` 5) arkOmega
  <> fmap (`unlock` 2) awesomeSpyglass
  <> fmap (`unlock` 6) awesomeTeleporters
  <> fmap (`unlock` 5) babyPremiumCare
  <> fmap (`unlock` 4) bridges
  <> fmap (`unlock` 5) chetusTools
  <> fmap (`unlock` 1) combatTrainer
  <> fmap (`unlock` 3) dinoStorage
  <> fmap (`unlock` 1) immersiveTaming
  <> fmap (`unlock` 11) industrialForges
  <> fmap (`unlock` 1) keepInventory
  <> fmap (`unlock` 12) kibbler
  <> fmap (`unlock` 6) ladysFarming
  <> fmap (`unlock` 1) levelupCounter
  <> fmap (`unlock` 1) marniHair
  <> fmap (`unlock` 13) pandorasBox
  <> fmap (`unlock` 6) saddleEmporium
  <> fmap (`unlock` 5) seedFarm
  <> fmap (`unlock` 3) shinyDinos
  <> fmap (`unlock` 14) solarPanels
  <> fmap (`unlock` 1) superStructures
  <> fmap (`unlock` 6) upgradeStation
  <> fmap (`unlock` 1) vanilla
