module Main (main) where

import Level
import Recipe (recipeOverrides)
import Recipes (recipes)
import EngramSetup (overrides)
import qualified Data.Text.IO as Text


main :: IO ()
main = do
  Text.putStrLn (recipeOverrides recipes)
  Text.putStrLn overrides
  Text.putStrLn ""
  Text.putStrLn (generateLevelingConfig 158 58 6.85 35.00 4)
