module Recipes (recipes) where

import Recipe
import qualified Data.Text.IO as Text
import Data.List (foldl')

elemCrystalSeedRecipe :: Recipe
elemCrystalSeedRecipe =
  Recipe ElementalCrystalSeed
    [ Component Crystal 30
    , Component Metal 5
    , Component Obsidian 5
    , Component RareFlower 1
    , Component RareMushroom 1
    ]

apprenticeCrystalSeedRecipe :: Recipe
apprenticeCrystalSeedRecipe =
  Recipe ApprenticeCrystalSeed
    [ Component ElementalCrystal 30
    , Component PlantXSeed 1
    , Component RareFlower 1
    , Component RareMushroom 1
    ]

journeymanCrystalSeedRecipe :: Recipe
journeymanCrystalSeedRecipe =
  Recipe JourneyManCrystalSeed
    [ Component ApprenticeCrystal 30
    , Component PlantXSeed 1
    , Component RareFlower 1
    , Component RareMushroom 1
    ]

mastercraftCrystalSeedRecipe :: Recipe
mastercraftCrystalSeedRecipe =
  Recipe MastercraftCrystalSeed
    [ Component JourneyManCrystal 30
    , Component PlantXSeed 1
    , Component RareFlower 1
    , Component RareMushroom 1
    ]

ascendantCrystalSeedRecipe :: Recipe
ascendantCrystalSeedRecipe =
  Recipe AscendantCrystalSeed
    [ Component MastercraftCrystal 30
    , Component PlantXSeed 1
    , Component RareFlower 1
    , Component RareMushroom 1
    ]

plantXConsumableRecipe :: Recipe
plantXConsumableRecipe =
  Recipe PlantXConsumable
    [ Component ElementalCrystalSeed 1
    , Component MejoberrySeed 1
    ]

dyeSeed :: Recipe
dyeSeed =
  Recipe DyeSeed
    [ Component RedColoring 10
    , Component GreenColoring 10
    , Component BlueColoring 10
    , Component (crystalForTier T2) 2
    , Component RareFlower 1
    , Component RareMushroom 1
    ]

seedRecipe :: Item -> Item -> Tier -> Int -> Recipe
seedRecipe i s t amnt =
  Recipe s
    [ Component i amnt
    , Component (crystalForTier t) 2
    , Component RareFlower 1
    , Component RareMushroom 1
    ]

seedRecipes :: [Recipe]
seedRecipes = foldl' (\acc (s, i, t) -> seedRecipe i s t 50 : acc) [] xs
  where
    xs = [
      (RawMeatSeed, RawMeat, T1),
      (RawPrimeMeatSeed, RawPrimeMeat, T1),
      (RawFishMeatSeed, RawFishMeat, T1),
      (RawPrimeFishMeatSeed, RawPrimeFishMeat, T1),

      -- (DyeSeed, Dye,),
      (FlintSeed, Flint, T2),
      (HideSeed, Hide, T2),
      (SpoiledMeatSeed, SpoiledMeat, T2),
      (ThatchSeed, Thatch, T2),

      (BioToxinSeed, BioToxin, T3),
      (BloodPackSeed, BloodPack, T3),
      (ChitinSeed, Chitin, T3),
      (FiberSeed, Fiber, T3),
      (HoneySeed, GiantBeeHoney, T3),
      (LeechBloodSeed, LeechBlood, T3),
      (OilSeed, Oil, T3),
      (PeltSeed, Pelt, T3),
      (RareFlowerSeed, RareFlower, T3),
      (RareMushroomSeed, RareMushroom, T3),
      (SandSeed, Sand, T3),
      (StoneSeed, Stone, T3),
      (WoodSeed, Wood, T3),

      (AnglerGelSeed, AnglerGel, T4),
      (CementingPasteSeed, CementingPaste, T4),
      (CharcoalSeed, Charcoal, T4),
      (CrystalSeed, Crystal, T4),
      (GasolineSeed, Gasoline, T4),
      (MetalSeed, Metal, T4),
      (NarcoticSeed, Narcotic, T4),
      (ObsidianSeed, Obsidian, T4),
      (SaltSeed, Salt, T4),
      (SapSeed, Sap, T4),
      (SilicaPearlsSeed, SilicaPearls, T4),
      (SilkSeed, Silk, T4),
      (SparkPowderSeed, SparkPowder, T4),
      (StimulantSeed, Stimulant, T4),

      (AmmoniteBileSeed, AmmoniteBile, T5),
      (BlackPearlSeed, BlackPearl, T5),
      (ElectronicsSeed, Electronics, T5),
      (ElementSeed, Element, T5),
      (GemSeed, BlueGem, T5),
      (GunpowderSeed, Gunpowder, T5),
      (KibbleSeed, BasicKibble, T5),
      (MetalIngotSeed, MetalIngot, T5),
      (PolymerSeed, Polymer, T5)
      ]

superSmallForgeRecipe :: Recipe
superSmallForgeRecipe =
  Recipe SuperSmallForge
    [ Component CementingPaste 100
    , Component ApprenticeCrystal 20
    , Component MetalIngot 100
    , Component Polymer 50
    ]

apprenticeCropPlotRecipe :: Recipe
apprenticeCropPlotRecipe =
  Recipe ApprenticeCropPlot
    [ Component LargeCropPlot 1
    , Component ApprenticeCrystal 10
    ]

journeyManCropPlotRecipe :: Recipe
journeyManCropPlotRecipe =
  Recipe JourneyManCropPlot
    [ Component ApprenticeCropPlot 1
    , Component JourneyManCrystal 10
    ]

mastercraftCropPlotRecipe :: Recipe
mastercraftCropPlotRecipe =
  Recipe MastercraftCropPlot
    [ Component JourneyManCropPlot 1
    , Component MastercraftCrystal 10
    ]

ascendantCropPlotRecipe :: Recipe
ascendantCropPlotRecipe =
  Recipe AscendantCropPlot
    [ Component MastercraftCropPlot 1
    , Component AscendantCrystal 10
    ]

tekCropPlotRecipe :: Recipe
tekCropPlotRecipe =
  Recipe TekCropPlot
    [ Component AscendantCropPlot 1
    , Component AscendantCrystal 20
    , Component Electronics 20
    , Component Polymer 20
    ]

ultimateTekCropPlotRecipe :: Recipe
ultimateTekCropPlotRecipe =
  Recipe UltimateTekCropPlot
    [ Component TekCropPlot 1
    , Component Element 50
    , Component AscendantCrystal 50
    , Component BioLankEssence 1
    ]

spawnerRecipes :: [Recipe]
spawnerRecipes =
  [ bioTekGigaSpawnerRecipe
  , bioLankBossSpawnerRecipe
  ]

bioTekGigaSpawnerRecipe :: Recipe
bioTekGigaSpawnerRecipe =
  Recipe BioTekGigaSpawner
    [ Component BlackPearl 25
    , Component Electronics 25
    , Component Element 25
    , Component AscendantCrystal 50
    ]

bioLankBossSpawnerRecipe :: Recipe
bioLankBossSpawnerRecipe =
  Recipe BioLankBossSpawner
    [ Component BlackPearl 25
    , Component Electronics 25
    , Component Element 30
    , Component AscendantCrystal 100
    ]

structureRecipes :: [Recipe]
structureRecipes =
  [ superSmallForgeRecipe
  , apprenticeCropPlotRecipe
  , journeyManCropPlotRecipe
  , mastercraftCropPlotRecipe
  , ascendantCropPlotRecipe
  , tekCropPlotRecipe
  , ultimateTekCropPlotRecipe
  ]

recipes :: [Recipe]
recipes =
  [ elemCrystalSeedRecipe
  , apprenticeCrystalSeedRecipe
  , journeymanCrystalSeedRecipe
  , mastercraftCrystalSeedRecipe
  , ascendantCrystalSeedRecipe
  , plantXConsumableRecipe
  , dyeSeed
  ] <> seedRecipes
  <> structureRecipes
  <> spawnerRecipes
